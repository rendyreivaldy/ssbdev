Version Log

App to Update :
	- RdyFw (main Framework)	(Updated at 26-04-2019)
	- Upil
	- e-Teller	(updated at 26-04-2019)
	- ITPD		(updated at 26-04-2019)

1. Version 1.0
	- add version log file
	- change logic of the error reporting, add new file to redirect whether is development or production
	- add version number in footer, in main menu and login page
	- add new row in parameter, row 'rdyfw_version'
	- fix bug query error because adding the 'rdyfw' object in the query -> application/libraries/Utility.php
	- add new parameterized icon image in browser tab
	- add missing icon like down arrow and friends, jqeuery icons mostly, change some css in -> upil\masterview\assets\libs\css\style.css, must create folder in libs in masterview assets
	- add .htaccess in every new folder I create to avoid hackers accessing our files
	- add parameter to change background color
	- add module API REST

2. version 1.1
	- add unit test :
		# add in application/controller/Unittest.php
		# add in application/view/content_unittest.php

3. version 1.2
	- add background cycler in login
	- add phpunit test
	- sync with btn asuransi
	- add sweetalert
	- main menu yng copyright di bottom
	- sync with btnpocer
	- sync with eteller
	- sync with itpd
	- sync with opac
	- sync with ppdppsyariah
