-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.38-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for ssb
CREATE DATABASE IF NOT EXISTS `ssb` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `ssb`;


-- Dumping structure for procedure ssb.CekKetentuan
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `CekKetentuan`(IN `iduji` varchar(100), IN `kodebank1` varchar(5), IN `nik1` varchar(20), IN `penghasilan1` float, IN `kodeadmwilayah` varchar(10), IN `hargarumah` float, IN `uangmuka` float, IN `sbum1` float, IN `nilaikpr` float, IN `sukubunga1` varchar(10))
BEGIN
	
	SET @hargarumah1 = (SELECT rfpriceindex.harga FROM rfpriceindex WHERE rfpriceindex.kodewilayah = kodeadmwilayah);
	SET @sukubunga = (SELECT rfpolicy.value FROM rfpolicy WHERE rfpolicy.idpolicy = '2');
	SET @penghasilan = (SELECT rfpolicy.VALUE FROM rfpolicy WHERE rfpolicy.idpolicy = '1');
	SET @nilaikprcek = hargarumah - (0.01 * hargarumah) - sbum1;
	SET @kodeadmcek = (SELECT 1 FROM rfpriceindex WHERE kodewilayah = kodeadmwilayah);
	SET @reason = '';
	
	-- cek hit max count per api per bank
	SET @cekhit = 0;
	SET @hitmax = 0;
	SELECT hit, max_hit into @cekhit, @hitmax FROM rflogin WHERE kodebank = kodebank1;
	
	IF (@cekhit >= @hitmax) THEN
		UPDATE resultuji set tanggaluji = NOW(), result_hargarumah = 'N', result_sukubunga = 'N', result_penghasilan= 'N', result_nilaikpr= 'N'  where resultuji.nik = nik1 AND resultuji.kodebank = kodebank1 AND resultuji.id_uji = iduji;
		UPDATE resultuji_hist set tanggaluji = NOW(), result_hargarumah = 'N', result_sukubunga = 'N', result_penghasilan= 'N', result_nilaikpr= 'N'  where resultuji_hist.nik = nik1 AND resultuji_hist.kodebank = kodebank1 AND resultuji_hist.id_uji = iduji;
		select response_code, response_message, @reason as reason from rfresponsecode where response_code = '12';
	ELSE
	
		/* Cek Harga Rumah */	
		IF (hargarumah <= @hargarumah1) THEN
			SET @result_hargarumah = 'Y';
		ELSE
			SET @result_hargarumah = 'N';
	        SET @reason = 'Harga rumah Tidak sesuai permen';
		END IF;
		
		/* Cek Suku Bunga */	
		IF (sukubunga1 = @sukubunga) THEN
			SET @result_sukubunga = 'Y';
		ELSE
			SET @result_sukubunga = 'N';
	        SET @reason = 'Suku Bunga Tidak sesuai permen';
		END IF;
		
		/* Cek Penghasilan */	
		IF (penghasilan1 <= @penghasilan) THEN
			SET @result_penghasilan = 'Y';
		ELSE
			SET @result_penghasilan = 'N';
	        SET @reason = 'Penghasilan Tidak sesuai permen';
		END IF;
		
		/* Cek Nilai KPR */	
		IF (nilaikpr <= @nilaikprcek) THEN
			SET @result_nilai_kpr = 'Y';
		ELSE
			SET @result_nilai_kpr = 'N';
	        SET @reason = 'Nilai KPR Tidak sesuai permen';
		END IF;
	    
		UPDATE resultuji set tanggaluji = NOW(), result_hargarumah = @result_hargarumah, result_sukubunga = @result_sukubunga, result_penghasilan=@result_penghasilan, result_nilaikpr=@result_nilai_kpr  where resultuji.nik = nik1 AND resultuji.kodebank = kodebank1 AND resultuji.id_uji = iduji;
		UPDATE resultuji_hist set tanggaluji = NOW(), result_hargarumah = @result_hargarumah, result_sukubunga = @result_sukubunga, result_penghasilan=@result_penghasilan, result_nilaikpr=@result_nilai_kpr  where resultuji_hist.nik = nik1 AND resultuji_hist.kodebank = kodebank1 AND resultuji_hist.id_uji = iduji;
		
		-- cek kalo misalnya kodeadmwilayah nya ga terdaftar di sistem SSB
	   IF (@kodeadmcek IS NULL) THEN
			SET @reason = 'Kode Wilayah tidak terdaftar';
	        select response_code, response_message, @reason as reason from rfresponsecode where response_code = '10';
		ELSE
			IF(@result_hargarumah = 'N' OR @result_sukubunga = 'N' OR @result_penghasilan = 'N' OR @result_nilai_kpr = 'N' OR @reason != '') then
				select response_code, response_message, @reason as reason from rfresponsecode where response_code = '04';
			ELSE
				if exists (select 1 from resultuji where resultuji.nik = nik1 and resultuji.kodebank <> kodebank1 and datediff(NOW(),resultuji.tanggaluji) <= 30) then
					
					/*Sedang diajukan/Sudah diuji oleh Akun Bank Lain, Tunggu lewat 30 hari */	
					select response_code, response_message, @reason as reason from rfresponsecode where response_code = '03';
				else 
					select response_code, response_message, @reason as reason from rfresponsecode where response_code = '00';
				end if;
			END IF;
		END IF;
		
		UPDATE rflogin SET hit = hit + 1 WHERE kodebank = kodebank1;
	
	END IF;
	
	
	
	
	
	
	
    
	
	END//
DELIMITER ;


-- Dumping structure for procedure ssb.CekLogin
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `CekLogin`(IN `kodebank1` VARCHAR(5), IN `pin1` VARCHAR(100))
BEGIN
	IF EXISTS (SELECT 1 FROM rflogin WHERE rflogin.kodebank = kodebank1) THEN
		IF EXISTS (SELECT 1 FROM rflogin WHERE rflogin.pin = pin1) THEN
			SET @tanggal = (SELECT rflogin.tanggal FROM rflogin WHERE rflogin.kodebank = kodebank1 AND rflogin.pin = pin1);
			IF (SELECT TO_DAYS(NOW())) != (SELECT TO_DAYS(@tanggal)) then
				update rflogin set rflogin.loginid = randomlogin(), rflogin.tanggal = NOW() where rflogin.kodebank = kodebank1 and rflogin.pin = pin1;
			ELSE
				update rflogin set rflogin.tanggal = NOW() where rflogin.kodebank = kodebank1 and rflogin.pin = pin1;
            END IF;
			SELECT '00' as `response_code`, rflogin.loginid from rflogin WHERE rflogin.kodebank = kodebank1 AND rflogin.pin = pin1;
		ELSE
			SELECT response_code, response_message FROM rfresponsecode WHERE response_code = '02';
		END IF;
	ELSE
		SELECT response_code, response_message FROM rfresponsecode WHERE response_code = '01';
	END IF;
	
	-- hapus seluruh data_uji dan result uji setelah 3 bulan 
	DELETE FROM data_uji WHERE created_at <= NOW() - INTERVAL 3 MONTH;
	DELETE FROM resultuji WHERE tanggaluji <= NOW() - INTERVAL 3 MONTH;
END//
DELIMITER ;


-- Dumping structure for procedure ssb.CekLoginId
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `CekLoginId`(IN `kodebank1` VARCHAR(5), IN `loginid1` VARCHAR(100))
BEGIN
	IF EXISTS (SELECT 1 FROM rflogin WHERE rflogin.kodebank = kodebank1 AND rflogin.loginid = loginid1) THEN
		SELECT '00' as `response_code`;
	ELSE
		SELECT response_code, response_message FROM rfresponsecode WHERE response_code = '02';
	END IF;
END//
DELIMITER ;


-- Dumping structure for procedure ssb.CekPengembang
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `CekPengembang`(iduji VARCHAR(100),kodebank1 VARCHAR(5), nik1 VARCHAR(20),npwp_pengembang1 varchar(16))
BEGIN
	IF EXISTS (SELECT 1 FROM sireng WHERE sireng.npwp_pengembang = npwp_pengembang1) THEN
		UPDATE resultuji SET tanggaluji = NOW(), result_pengembang= 'Y'  WHERE resultuji.nik = nik1 AND resultuji.kodebank = kodebank1 AND resultuji.id_uji = iduji;
        UPDATE resultuji_hist SET tanggaluji = NOW(), result_pengembang= 'Y'  WHERE resultuji_hist.nik = nik1 AND resultuji_hist.kodebank = kodebank1 AND resultuji_hist.id_uji = iduji;
		-- SELECT '00';	
		select response_code, response_message from rfresponsecode where response_code = '00';
	ELSE	
		UPDATE resultuji SET tanggaluji = NOW(), result_pengembang= 'N'  WHERE resultuji.nik = nik1 AND resultuji.kodebank = kodebank1 AND resultuji.id_uji = iduji;
        UPDATE resultuji_hist SET tanggaluji = NOW(), result_pengembang= 'N'  WHERE resultuji_hist.nik = nik1 AND resultuji_hist.kodebank = kodebank1 AND resultuji_hist.id_uji = iduji;
		-- SELECT '07';
		select response_code, response_message from rfresponsecode where response_code = '07';
	END IF;
    END//
DELIMITER ;


-- Dumping structure for procedure ssb.CekSubsidi
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `CekSubsidi`(iduji VARCHAR(100),kodebank1 VARCHAR(5), nik1 VARCHAR(20))
BEGIN
	SET @result_subsidi = '';
	SET @response_code = '';
	IF EXISTS (SELECT 1 FROM subsidi WHERE subsidi.nik = nik1) THEN
		/*Sudah Pernah Mendapat Subsidi*/	
		-- UPDATE resultuji SET tanggaluji = NOW(), result_subsidi = 'N'  WHERE resultuji.nik = nik1 AND resultuji.kodebank = kodebank1 AND resultuji.id_uji = iduji;
        -- UPDATE resultuji_hist SET tanggaluji = NOW(), result_subsidi = 'N'  WHERE resultuji_hist.nik = nik1 AND resultuji_hist.kodebank = kodebank1 AND resultuji_hist.id_uji = iduji;
		-- SELECT '06' ;
		-- select response_code, response_message from rfresponsecode where response_code = '06';
        SET @result_subsidi = 'N';
        SET @response_code = '06';
	ELSE
		-- UPDATE resultuji SET tanggaluji = NOW(), result_subsidi = 'Y'  WHERE resultuji.nik = nik1 AND resultuji.kodebank = kodebank1 AND resultuji.id_uji = iduji;	
        -- UPDATE resultuji_hist SET tanggaluji = NOW(), result_subsidi = 'Y'  WHERE resultuji_hist.nik = nik1 AND resultuji_hist.kodebank = kodebank1 AND resultuji_hist.id_uji = iduji;	
		-- SELECT '00';
        -- select response_code, response_message from rfresponsecode where response_code = '00';
        SET @result_subsidi = 'Y';
        SET @response_code = '00';
	end if ;
    
    UPDATE resultuji SET tanggaluji = NOW(), result_subsidi = @result_subsidi  WHERE resultuji.nik = nik1 AND resultuji.kodebank = kodebank1 AND resultuji.id_uji = iduji;
	UPDATE resultuji_hist SET tanggaluji = NOW(), result_subsidi = @result_subsidi  WHERE resultuji_hist.nik = nik1 AND resultuji_hist.kodebank = kodebank1 AND resultuji_hist.id_uji = iduji;
    select response_code, response_message from rfresponsecode where response_code = @response_code;
		
    END//
DELIMITER ;


-- Dumping structure for table ssb.data_uji
CREATE TABLE IF NOT EXISTS `data_uji` (
  `nama_pemohon` varchar(999) DEFAULT NULL,
  `pekerjaan` varchar(200) DEFAULT NULL,
  `jenis_kelamin` varchar(200) DEFAULT NULL,
  `nik` varchar(20) DEFAULT NULL,
  `npwp_pemohon` varchar(20) DEFAULT NULL,
  `penghasilan` float DEFAULT NULL,
  `nama_pasangan` varchar(200) DEFAULT NULL,
  `nik_pasangan` varchar(20) DEFAULT NULL,
  `no_sp3k` varchar(200) DEFAULT NULL,
  `harga_rumah` float DEFAULT NULL,
  `uang_muka` float DEFAULT NULL,
  `sbum` float DEFAULT NULL,
  `nilai_kpr` float DEFAULT NULL,
  `suku_bunga` varchar(10) DEFAULT NULL,
  `tenor_bulan` varchar(10) DEFAULT NULL,
  `angsuran` float DEFAULT NULL,
  `nilai_subsidi` float DEFAULT NULL,
  `nama_pengembang` varchar(200) DEFAULT NULL,
  `npwp_pengembang` varchar(20) DEFAULT NULL,
  `nama_perumahan` varchar(200) DEFAULT NULL,
  `alamat_agunan` varchar(200) DEFAULT NULL,
  `kota_agunan` varchar(200) DEFAULT NULL,
  `provinsi` varchar(200) DEFAULT NULL,
  `kode_adm_wilayah` varchar(20) DEFAULT NULL,
  `kode_pos_agunan` varchar(200) DEFAULT NULL,
  `luas_tanah` varchar(200) DEFAULT NULL,
  `luas_bangunan` varchar(200) DEFAULT NULL,
  `id_uji` varchar(100) NOT NULL,
  `kodebank` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_uji`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table ssb.data_uji: ~0 rows (approximately)
/*!40000 ALTER TABLE `data_uji` DISABLE KEYS */;
/*!40000 ALTER TABLE `data_uji` ENABLE KEYS */;


-- Dumping structure for table ssb.data_uji_hist
CREATE TABLE IF NOT EXISTS `data_uji_hist` (
  `nama_pemohon` varchar(999) DEFAULT NULL,
  `pekerjaan` varchar(200) DEFAULT NULL,
  `jenis_kelamin` varchar(200) DEFAULT NULL,
  `nik` varchar(20) DEFAULT NULL,
  `npwp_pemohon` varchar(20) DEFAULT NULL,
  `penghasilan` float DEFAULT NULL,
  `nama_pasangan` varchar(200) DEFAULT NULL,
  `nik_pasangan` varchar(20) DEFAULT NULL,
  `no_sp3k` varchar(200) DEFAULT NULL,
  `harga_rumah` float DEFAULT NULL,
  `uang_muka` float DEFAULT NULL,
  `sbum` float DEFAULT NULL,
  `nilai_kpr` float DEFAULT NULL,
  `suku_bunga` varchar(10) DEFAULT NULL,
  `tenor_bulan` varchar(10) DEFAULT NULL,
  `angsuran` float DEFAULT NULL,
  `nilai_subsidi` float DEFAULT NULL,
  `nama_pengembang` varchar(200) DEFAULT NULL,
  `npwp_pengembang` varchar(20) DEFAULT NULL,
  `nama_perumahan` varchar(200) DEFAULT NULL,
  `alamat_agunan` varchar(200) DEFAULT NULL,
  `kota_agunan` varchar(200) DEFAULT NULL,
  `provinsi` varchar(200) DEFAULT NULL,
  `kode_adm_wilayah` varchar(20) DEFAULT NULL,
  `kode_pos_agunan` varchar(200) DEFAULT NULL,
  `luas_tanah` varchar(200) DEFAULT NULL,
  `luas_bangunan` varchar(200) DEFAULT NULL,
  `id_uji` varchar(100) NOT NULL,
  `kodebank` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_uji`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table ssb.data_uji_hist: ~0 rows (approximately)
/*!40000 ALTER TABLE `data_uji_hist` DISABLE KEYS */;
/*!40000 ALTER TABLE `data_uji_hist` ENABLE KEYS */;


-- Dumping structure for procedure ssb.InsertUji
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `InsertUji`(IN `nama_pemohon1` varchar(999), IN `pekerjaan1` varchar(200), IN `jenis_kelamin1` varchar(200), IN `nik1` varchar(20), IN `npwp_pemohon1` varchar(20), IN `penghasilan1` float, IN `nama_pasangan1` varchar(200), IN `nik_pasangan1` varchar(20), IN `no_sp3k1` varchar(200), IN `harga_rumah1` float, IN `uang_muka1` float, IN `sbum1` float, IN `nilai_kpr1` float, IN `suku_bunga1` varchar(10), IN `tenor_bulan1` varchar(10), IN `angsuran1` float, IN `nilai_subsidi1` float, IN `nama_pengembang1` varchar(200), IN `npwp_pengembang1` varchar(20), IN `nama_perumahan1` varchar(200), IN `alamat_agunan1` varchar(200), IN `kota_agunan1` varchar(20), IN `provinsi1` varchar(20), IN `kode_adm_wilayah1` varchar(20), IN `kode_pos_agunan1` varchar(200), IN `luas_tanah1` varchar(10), IN `luas_bangunan1` varchar(10), IN `kodebank1` varchar(10)
)
BEGIN
	
	-- data pasangan gaboleh kedaftar di sistem, jadi suami istri cuma
	-- boleh 1 kali mengajukan
	set @cekpasangan = 0;
	SELECT 
	COUNT(a.nama_pemohon) as total
	INTO @cekpasangan
	FROM data_uji a
	WHERE a.nik = nik_pasangan1;
	
	-- untuk nik, NPWP dan kode bank yang sama apabila resultuji nya Y semua, paksa batal uji dulu
	-- kalo data uji yang result ujinya ada yang N 1 biji, flow kayak biasa, apus insert lagi
	SET @totalsukses = 0;
	SELECT
	COUNT(a.nama_pemohon) as total
	INTO 
	@totalsukses
	FROM data_uji a
	LEFT JOIN resultuji b on a.id_uji = b.id_uji
	WHERE b.result_dukcapil = 'Y'
	AND b.result_hargarumah = 'Y'
	AND b.result_penghasilan = 'Y'
	AND b.result_pengembang = 'Y'
	AND b.result_nilaikpr = 'Y'
	AND b.result_sukubunga = 'Y'
	AND b.result_subsidi = 'Y'
	AND (a.nik = nik1 or a.npwp_pemohon = npwp_pemohon1)
	AND a.kodebank = kodebank1;
	
	IF(@totalsukses > 0) THEN
		select response_code, response_message from rfresponsecode where response_code = '13';
	ELSEIF (@cekpasangan > 0) THEN
		select response_code, response_message from rfresponsecode where response_code = '15';
	ELSE
	
	
		DELETE FROM data_uji WHERE nik = nik1 and kodebank = kodebank1;
		DELETE FROM resultuji WHERE nik = nik1 and kodebank = kodebank1;
		
		SET @id_uji1 = UUID();
	    
		INSERT INTO data_uji
	    (
			nama_pemohon,
			pekerjaan,
			jenis_kelamin,
			nik,
			npwp_pemohon,
			penghasilan ,
			nama_pasangan,
			nik_pasangan,
			no_sp3k,
			harga_rumah ,
			uang_muka ,
			sbum ,
			nilai_kpr ,
			suku_bunga,
			tenor_bulan,
			angsuran ,
			nilai_subsidi ,
			nama_pengembang,
			npwp_pengembang,
			nama_perumahan,
			alamat_agunan,
			kota_agunan,
			provinsi,
			kode_adm_wilayah,
			kode_pos_agunan,
			luas_tanah,
			luas_bangunan,
			id_uji,
	        kodebank,
	        created_at
	    ) VALUES (
			nama_pemohon1,
			pekerjaan1,
			jenis_kelamin1,
			nik1,
			npwp_pemohon1,
			penghasilan1 ,
			nama_pasangan1,
			nik_pasangan1,
			no_sp3k1,
			harga_rumah1 ,
			uang_muka1 ,
			sbum1 ,
			nilai_kpr1 ,
			suku_bunga1,
			tenor_bulan1,
			angsuran1 ,
			nilai_subsidi1 ,
			nama_pengembang1,
			npwp_pengembang1,
			nama_perumahan1,
			alamat_agunan1,
			kota_agunan1,
			provinsi1,
			kode_adm_wilayah1,
			kode_pos_agunan1,
			luas_tanah1,
			luas_bangunan1,
			@id_uji1,
	        kodebank1,
	        NOW()
	    );
	    
	    INSERT INTO resultuji 
	    (
			id_uji,
	        kodebank,
	        nik,
	        tanggaluji
		) values (
			@id_uji1, 
			kodebank1, 
			nik1, 
			NOW()
		);
	    
		INSERT INTO data_uji_hist SELECT * FROM data_uji WHERE id_uji = @id_uji1;
		INSERT INTO resultuji_hist SELECT * FROM resultuji WHERE id_uji = @id_uji1;
		select response_code, response_message, @id_uji1 as id_uji from rfresponsecode where response_code = '00';
	
	END IF;
	
END//
DELIMITER ;


-- Dumping structure for table ssb.log
CREATE TABLE IF NOT EXISTS `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(25) DEFAULT NULL,
  `datetimelog` datetime DEFAULT NULL,
  `action` text,
  `keterangan` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8616 DEFAULT CHARSET=utf8;


-- Dumping structure for table ssb.menu
CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` tinytext,
  `icon` varchar(75) DEFAULT NULL,
  `id_parent` int(11) DEFAULT NULL,
  `url` text,
  `user_role` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- Dumping data for table ssb.menu: ~13 rows (approximately)
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` (`id`, `name`, `icon`, `id_parent`, `url`, `user_role`, `active`) VALUES
	(1, 'Admin', 'fas fa-user', 0, '', 1, 1),
	(2, 'User', '', 1, '/User', 1, 1),
	(5, 'Menu', '', 1, '/Menu', 1, 1),
	(7, 'User Guide', '', 10, '.user_guide', 1, 1),
	(8, 'Template', '', 10, '.masterview', 1, 1),
	(9, 'Parameter', '', 1, '/Parameter', 1, 1),
	(10, 'Master', 'fas fa-smile', 0, '', 1, 1),
	(11, 'User Role', '', 1, '/UserRole', 1, 1),
	(13, 'Log', '', 1, '/Log', 1, 1),
	(14, 'Unit Test', '', 1, '/Unittest', 1, 1),
	(15, 'SSB', 'far fa-lightbulb', 0, '', 1, 1),
	(16, 'Data Uji', '', 15, 'Ssb/', 1, 1),
	(17, 'SSB', 'far fa-lightbulb', 0, '', 2, 1),
	(18, 'Data Uji', '', 17, 'Ssb/', 2, 1);
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;


-- Dumping structure for table ssb.param
CREATE TABLE IF NOT EXISTS `param` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` tinytext,
  `value` text,
  `keterangan` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

-- Dumping data for table ssb.param: ~28 rows (approximately)
/*!40000 ALTER TABLE `param` DISABLE KEYS */;
INSERT INTO `param` (`id`, `name`, `value`, `keterangan`) VALUES
	(1, 'enable_registration', '0', 'flag untuk enable disable registration via user directly. enable = 1, disable = 0'),
	(2, 'block_multiple_false_login', '0', 'kalo misalkan beberapa kali salah login, user dikunci'),
	(3, 'login_trial_max', '2', 'berapa kali user bisa maksimal login sampe dia ngunci. biar pas berarti n-1. jadi kalo mau max 3 kali, berarti n = 2'),
	(5, 'apps_name', 'SSB', 'Nama Aplikasi untuk di munculin di mana mana'),
	(7, 'controller_after_login', 'User/', 'INI KAYAKNYA GAJADI GW PAKE DEH, KARENA JADINYA MAU PER USER ROLE DEFAULT LANDING PAGE NYA. controller yang akan di hit setelah login berahasil. semi default controller after login'),
	(9, 'login_image', '/files/images/pupera.png', 'image yang ada di halaman login, image size 155x35 png file'),
	(10, 'login_ldap', '0', 'enable disable login pake LDAP'),
	(11, 'login_color', '#efeff6', 'warna halaman login buat perbedaan dikit kalo misalnya nanti mau buat beberapa aplikasi'),
	(12, 'welcome_text', 'SSB SatKer KEMENPUPERA', 'kata-kata yang nanti akan dimunculin di login page buat pembeda kalo misalnya mau dipake di beberapa aplikasi'),
	(13, 'rdyfw_version', 'Version 1.0', 'version dari aplikasinya'),
	(14, 'tab_icon', '/files/images/pupera.png', 'icon yang ada di browser tab, biar pas 150x150 kali yah'),
	(15, 'background_color', '#cff0cc8c', 'warna background supaya membedain sama aplikasi lain'),
	(16, 'images_background_login', '["pupera2.jpg"]', 'array images yang mau di jadiin background'),
	(17, 'background_cycle_flag', 'false', 'status enable apa disable background image'),
	(18, 'background_cycle_interval', '3', 'interval background cycle ganti gambar'),
	(19, 'dukcapil_cek_url', 'http://10.255.0.65/ws_dukcapil_65/Service.asmx/getNikData2', 'URL API mas fitrah untuk cek dukcapil'),
	(20, 'dukcapil_progname', 'DGB', 'parameter yang mesti dikirim untuk API cek dukcapil'),
	(21, 'dukcapil_hashcode', '', 'parameter yang mesti dikirim untuk API cek dukcapil. tapi katanya dikosongin aja'),
	(22, 'dukcapil_user', NULL, 'user buat dukcapilnya. ternyata pake yak ?'),
	(23, 'dukcapil_password', NULL, 'password buat dukcapil'),
	(24, 'dukcapil_ip_address', NULL, 'IP address buat nembak dukcapil'),
	(25, 'dukcapil_url_direct', 'http://172.16.160.128:8000/dukcapil/get_json/BTN/CALL_NIK', 'URL direct hit ke dukcapil'),
	(26, 'dukcapil_mock_response', '0', '1 -> enabled fake response, 0 -> disable fake response. hit dukcapil apa mau fake response, pake quota soalnya hit dukcapilnya.'),
	(27, 'dukcapil_direct_hit', '0', ' 0 -> api mas fitrah, 1 -> direct hit. hit dukcapil via API mas fitrah apa langsung'),
	(28, 'ssb_ceksubsidi', 'http://ssb.ppdpp.id/Api/getdebitur_flpp/', 'url API untuk cek debitur ke ssb, formatnya pake nik after last slash'),
	(29, 'ssb_cekpupr', 'http://ssb.ppdpp.id/Api/getdata_pupr/', 'url API untuk cek pupr ? ini apaan yak ?'),
	(30, 'ssb_cekpengembang', 'http://ssb.ppdpp.id/Api/getdata_pengembang/', 'url API untuk cek pengembang ke ssb, formatnya pake npwp pengembang after last slash'),
	(31, 'curl_proxy', '1', 'buat segala hit hit keluar yang butuh proxy curl'),
	(32, 'manual_tgl_uji', '1', '0 -> disable, 1 -> enable, set tanggal ujia ga by system, tapi manual di passing dari parameter');
/*!40000 ALTER TABLE `param` ENABLE KEYS */;


-- Dumping structure for function ssb.randomlogin
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `randomlogin`() RETURNS varchar(128) CHARSET utf8mb4
BEGIN
SET @chars = 'aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPqQrRsStTuUvVwWxXyYzZ1234567890';
SET @charLen = LENGTH(@chars);
SET @randomPassword = '';
WHILE LENGTH(@randomPassword) < 20
    DO
    SET @randomPassword = CONCAT(@randomPassword, SUBSTRING(@chars,CEILING(RAND() * @charLen),1));
END WHILE;
RETURN @randomPassword ;
    END//
DELIMITER ;


-- Dumping structure for table ssb.resultuji
CREATE TABLE IF NOT EXISTS `resultuji` (
  `id_uji` varchar(100) NOT NULL,
  `kodebank` varchar(5) DEFAULT NULL,
  `nik` varchar(20) DEFAULT NULL,
  `tanggaluji` datetime DEFAULT NULL,
  `result_dukcapil` varchar(10) DEFAULT NULL,
  `result_hargarumah` varchar(10) DEFAULT NULL,
  `result_penghasilan` varchar(10) DEFAULT NULL,
  `result_pengembang` varchar(10) DEFAULT NULL,
  `result_nilaikpr` varchar(10) DEFAULT NULL,
  `result_sukubunga` varchar(10) DEFAULT NULL,
  `result_subsidi` varchar(10) DEFAULT NULL,
  `id_hasil_uji` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_hasil_uji`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table ssb.resultuji: ~0 rows (approximately)
/*!40000 ALTER TABLE `resultuji` DISABLE KEYS */;
/*!40000 ALTER TABLE `resultuji` ENABLE KEYS */;


-- Dumping structure for table ssb.resultuji_hist
CREATE TABLE IF NOT EXISTS `resultuji_hist` (
  `id_uji` varchar(100) NOT NULL,
  `kodebank` varchar(5) DEFAULT NULL,
  `nik` varchar(20) DEFAULT NULL,
  `tanggaluji` datetime DEFAULT NULL,
  `result_dukcapil` varchar(10) DEFAULT NULL,
  `result_hargarumah` varchar(10) DEFAULT NULL,
  `result_penghasilan` varchar(10) DEFAULT NULL,
  `result_pengembang` varchar(10) DEFAULT NULL,
  `result_nilaikpr` varchar(10) DEFAULT NULL,
  `result_sukubunga` varchar(10) DEFAULT NULL,
  `result_subsidi` varchar(10) DEFAULT NULL,
  `id_hasil_uji` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_hasil_uji`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table ssb.resultuji_hist: ~0 rows (approximately)
/*!40000 ALTER TABLE `resultuji_hist` DISABLE KEYS */;
/*!40000 ALTER TABLE `resultuji_hist` ENABLE KEYS */;


-- Dumping structure for table ssb.rflogin
CREATE TABLE IF NOT EXISTS `rflogin` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `kodebank` varchar(5) DEFAULT NULL,
  `pin` varchar(100) DEFAULT NULL,
  `loginid` varchar(999) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `max_hit` int(11) DEFAULT NULL,
  `hit` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table ssb.rflogin: ~2 rows (approximately)
/*!40000 ALTER TABLE `rflogin` DISABLE KEYS */;
INSERT INTO `rflogin` (`id`, `kodebank`, `pin`, `loginid`, `tanggal`, `max_hit`, `hit`) VALUES
	(1, '200', '123', 'ytWyWjIr7bKugAqQfgP8', '2020-06-02 08:26:26', 6000, 18),
	(2, '201', '123', '7qzqeeIaF59F7GhpymvGa', '2020-05-12 10:31:17', 5000, 0);
/*!40000 ALTER TABLE `rflogin` ENABLE KEYS */;


-- Dumping structure for table ssb.rfpolicy
CREATE TABLE IF NOT EXISTS `rfpolicy` (
  `idpolicy` varchar(3) NOT NULL,
  `policy` varchar(200) DEFAULT NULL,
  `value` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`idpolicy`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table ssb.rfpolicy: ~2 rows (approximately)
/*!40000 ALTER TABLE `rfpolicy` DISABLE KEYS */;
INSERT INTO `rfpolicy` (`idpolicy`, `policy`, `value`) VALUES
	('1', 'penghasilan', '8000000'),
	('2', 'sukubunga', '0.05');
/*!40000 ALTER TABLE `rfpolicy` ENABLE KEYS */;


-- Dumping structure for table ssb.rfpriceindex
CREATE TABLE IF NOT EXISTS `rfpriceindex` (
  `kodewilayah` varchar(190) NOT NULL,
  `harga` float DEFAULT NULL,
  PRIMARY KEY (`kodewilayah`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table ssb.rfpriceindex: ~0 rows (approximately)
/*!40000 ALTER TABLE `rfpriceindex` DISABLE KEYS */;
INSERT INTO `rfpriceindex` (`kodewilayah`, `harga`) VALUES
	('327510', 150000000);
/*!40000 ALTER TABLE `rfpriceindex` ENABLE KEYS */;


-- Dumping structure for table ssb.rfresponsecode
CREATE TABLE IF NOT EXISTS `rfresponsecode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `response_code` varchar(50) DEFAULT NULL,
  `response_message` tinytext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- Dumping data for table ssb.rfresponsecode: ~18 rows (approximately)
/*!40000 ALTER TABLE `rfresponsecode` DISABLE KEYS */;
INSERT INTO `rfresponsecode` (`id`, `response_code`, `response_message`) VALUES
	(1, '00', 'Sukses'),
	(2, '01', 'Kode Bank Tidak Terdaftar'),
	(3, '02', 'Login ID Tidak Ditemukan'),
	(4, '03', 'NIK Sudah diujikan Bank Lain'),
	(5, '04', 'Pengajuan Tidak sesuai Ketentuan Permen'),
	(6, '05', 'NIK Tidak Terdaftar di Dukcapil'),
	(7, '06', 'NIK Pernah memperoleh Subsidi'),
	(8, '07', 'Data Pengembang Tidak Terdaftar di SIRENG'),
	(9, '08', 'Parameter tidak sesuai. Cek kembali dokumen teknis'),
	(10, '09', 'Nama tidak sesuai dengan data Dukcapil'),
	(11, '10', 'Kode Wilayah tidak valid'),
	(12, '11', 'Tidak bisa menghubungi server'),
	(13, '12', 'Quota total akses API telah mencapai maksimal'),
	(14, '13', 'Data dengan NIK atau NPWP tersebut sudah ada di sistem dan telah lolos Uji Dini. Silahkan Batal Uji terlebih dahulu untuk Uji Dini ulang'),
	(15, '14', 'Pemohon sudah menikah namun NIK Pasangan kosong'),
	(16, '15', 'Pasangan pernah memperoleh subsidi'),
	(17, '16', 'Jenis Kelamin tidak sesuai dengan data Dukcapil'),
	(18, '17', 'No Kartu Keluarga Pemohon dan Pasangan tidak sesuai'),
	(19, '18', 'NIK tidak ditemukan di sistem');
/*!40000 ALTER TABLE `rfresponsecode` ENABLE KEYS */;


-- Dumping structure for table ssb.sireng
CREATE TABLE IF NOT EXISTS `sireng` (
  `id` bigint(20) NOT NULL,
  `id_pengembang` varchar(20) NOT NULL,
  `nama_pengembang` varchar(999) DEFAULT NULL,
  `npwp_pengembang` varchar(16) DEFAULT NULL,
  `alamat_pengembang` varchar(999) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table ssb.sireng: ~0 rows (approximately)
/*!40000 ALTER TABLE `sireng` DISABLE KEYS */;
INSERT INTO `sireng` (`id`, `id_pengembang`, `nama_pengembang`, `npwp_pengembang`, `alamat_pengembang`) VALUES
	(0, 'a', 'majusuka', '123456789098889', 'jalan zamrud no 1');
/*!40000 ALTER TABLE `sireng` ENABLE KEYS */;


-- Dumping structure for table ssb.subsidi
CREATE TABLE IF NOT EXISTS `subsidi` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nik` varchar(20) NOT NULL,
  `is_subsidi` bit(1) DEFAULT NULL,
  `subsidi_type` varchar(100) DEFAULT NULL,
  `subsidi_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table ssb.subsidi: ~0 rows (approximately)
/*!40000 ALTER TABLE `subsidi` DISABLE KEYS */;
/*!40000 ALTER TABLE `subsidi` ENABLE KEYS */;


-- Dumping structure for table ssb.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` text,
  `password` text,
  `id_role` int(11) DEFAULT NULL,
  `nama` text,
  `is_blocked` int(11) DEFAULT '0',
  `login_trial` int(1) DEFAULT NULL,
  `last_login_date` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `islogin` int(11) DEFAULT NULL,
  `lastactiontime` datetime DEFAULT NULL,
  `session` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- Dumping data for table ssb.user: ~3 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `username`, `password`, `id_role`, `nama`, `is_blocked`, `login_trial`, `last_login_date`, `created_by`, `updated_by`, `islogin`, `lastactiontime`, `session`) VALUES
	(1, 'rdy', '4dff4ea340f0a823f15d3f4f01ab62eae0e5da579ccb851f8db9dfe84c58b2b37b89903a740e1ee172da793a6e79d560e5f7f9bd058a12a280433ed6fa46510a', 1, 'Rendy Reivaldy', 0, 0, '2020-06-02 04:32:41', 0, NULL, 1, '2019-03-21 11:53:05', NULL),
	(12, 'Rendy13864', '4dff4ea340f0a823f15d3f4f01ab62eae0e5da579ccb851f8db9dfe84c58b2b37b89903a740e1ee172da793a6e79d560e5f7f9bd058a12a280433ed6fa46510a', 1, 'Rendy Reivaldy', 0, 0, '2019-03-21 09:33:33', 1, NULL, 0, '2019-03-21 11:53:05', NULL),
	(13, 'ssb', '75902ba2733cf1dd104b37f5744f161487ef19529b6accc0e2d8c7849be01001d16d7320a1ea536183d9ee0c6864fe9ccff5b1f4967ec1de342485aab420e426', 2, 'SSB', 0, 0, '2020-06-02 10:15:39', 1, NULL, 1, NULL, NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;


-- Dumping structure for table ssb.user_role
CREATE TABLE IF NOT EXISTS `user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` tinytext,
  `keterangan` text,
  `landing_page` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table ssb.user_role: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` (`id`, `role`, `keterangan`, `landing_page`) VALUES
	(1, 'Master', 'Master Super Hyper Ultra Mega Giga Admin', 'User'),
	(2, 'ssb', 'User SSB', 'Ssb/');
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
