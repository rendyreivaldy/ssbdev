TO-DO
---------------------------------------------------------------------
1. Login
	a. V registration
	b. V login form validity
	c. V login security
		- V multiple login blocked
		- V encrypted password send
		- V login LDAP
	d. V Logout
2. V config db menu
3. V user
4. V menu
	a. V left menu list
	b. V prohibited menu when someone playing with the URL
5. V Logging
	a. V login integration with logging, updating lastactiontime
	b. V logging for every action and the data
	c. V log show
6. V user role



HOMEWORK

---------------------------------------------------------------------



TEST SCRIPT

---------------------------------------------------------------------
LOGIN PAGE
1. login username kosong
2. login password kosong
3. login salah password x kali
4. login gada user terdaftar
5. registrasi username kosong
6. registrasi password kosong
7. registrasi nama kosong
8. registrasi pastikan password kosong
9. registrasi password dan pastikan password beda
10. normal login
11. normal logout
12. login LDAP

----------------------------------------------------------------------
USER PAGE
1. Filter data
2. create nama kosong
3. create username kosong
4. create password kosong
5. create password konfirmasi kosong
6. edit nama kosong
7. edit username kosong
8. edit password kosong
9. edit password konfirmasi kosong
10. create javascript password dengan konfirmasi password beda
10. change password javascript password dengan konfirmasi password beda
11. change password old password salah
12. delete user

----------------------------------------------------------------------
MENU PAGE
1. filter data
2. create menu nama kosong
3. create menu url kosong
4. edit menu nama kosong
5. edit menu url kosong
6. delete menu

----------------------------------------------------------------------
PARAMETER PAGE
1. filter data
2. create parameter name kosong
3. create parameter value kosong
4. edit parameter name kosong
5. edit parameter value kosong
6. hapus parameter

----------------------------------------------------------------------
USER ROLE PAGE
1. filter data
2. create user role role kosong
3. edit user role role kosong
4. hapus user role

