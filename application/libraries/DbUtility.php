<?php defined('BASEPATH') OR exit('No direct script access allowed');

class DbUtility
{

    public function createNewRow($table, $data, $db)
    {
        return $db->insert($table, $data);
    }

    public function retrievetableall($table, $db)
    {
        $query = $db->get($table);
        return $query->result();
    }

    public function retrievedataspecific($table, $wherecondition, $wherecolumn, $db)
    {
        $db->where($wherecolumn, $wherecondition);
        $query = $db->get($table);
        return $query->row();
    }

    public function retrievedataspecificresult($table, $wherecondition, $wherecolumn, $db)
    {
        $db->where($wherecolumn, $wherecondition);
        $query = $db->get($table);
        return $query->result();
    }

    public function updaterow($table, $data, $conditioncolumn, $condition, $db)
    {
        $db->where($conditioncolumn, $condition);
        return $db->update($table, $data);
    }

    public function deleterow($table, $columnCondition, $condition, $db)
    {
        $db->where($columnCondition, $condition);
        return $db->delete($table);
    }

    public function retrievedataspecificcustom($table, $queryselectstring, $wherestring, $db)
    {
        // $db->select('(SELECT SUM(payments.amount) FROM payments WHERE payments.invoice_id=4) AS amount_paid', FALSE);
        $db->select($queryselectstring, FALSE);
        $db->where($wherestring);
        $query = $db->get($table);
        return $query->result();
    }

    public function retrievedataspecificverycustom($wherestring, $db)
    {
        $query = $db->query($wherestring);
        return $query->result();
    }

    public function runcustomquery($wherestring, $db)
    {
        $query = $db->query($wherestring);
    }



}