<?php

	/**
	* 
	*/
	class ConnectionUtility 
	{
		
		function __construct()
		{
			# code...
		}

		public function middlewareConnectionPost($postData, $url) {

			// The data to send to the API
			/*$postData = array(
			    'kind' => 'blogger#post',
			    'title' => 'A new post',
			    'content' => 'With <b>exciting</b> content...'
			);*/

			// Setup cURL
			$ch = curl_init($url);
			curl_setopt_array($ch, array(
			    CURLOPT_POST => TRUE,
			    CURLOPT_RETURNTRANSFER => TRUE,
			    CURLOPT_HTTPHEADER => array(
			        'Content-Type: application/json',
			        'id: BTNTBTN',
			        'key: BtNTT3sting'
			    ),
			    CURLOPT_POSTFIELDS => json_encode($postData)
			));

			// Send the request
			$response = curl_exec($ch);

			// Check for errors
			if($response === FALSE){
			    die(curl_error($ch));
			}

			// Decode the response
			return json_decode($response, TRUE);

		}
	}

?>