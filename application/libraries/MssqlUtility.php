<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MssqlUtility
{

    public static function connectOdbc($dbName){
        $instance =& get_instance();
        $instance->load->model('ModelParam');

        $server = $instance->ModelParam->getParameter('sqlsrv_host');
        $user = $instance->ModelParam->getParameter('sqlsrv_username');
        $password = $instance->ModelParam->getParameter('sqlsrv_password');

        $conn = odbc_connect("Driver={SQL Server};Server=".$server.";Database=".$dbName.";", $user, $password);
        if ($conn){
            return $conn;
        }else{
            die(odbc_errormsg());
            return false;
        }
        exit;
    }

    public static function odbcExec($query, $conn){
        $result = odbc_exec($conn, $query);
        if ($result == 0)
        {
            echo ("odbc failed.");
            return false;
        }
        else
        {
            $finalArray = array();
            while($row = odbc_fetch_array($result)) {
                array_push($finalArray, $row);
            }
            return $finalArray;
        }
    }

    public static function odbcExecWithoutFetch($query, $conn){
        $result = odbc_exec($conn, $query);
    }

    public static function closePDO($conn) 
    { 
        if ($conn) 
            odbc_close($conn); 
    }

}
