<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Loggings
{

    public static function logExit($data){
        print_r($data);
        exit;
    }

    public static function varDump($data){
    	var_dump($data);
    }

    public static function logDatabase($username, $action, $keterangan, $object){
    	// LOGGING DATA
		$object->load->model('LogModel');
		$object->LogModel->logData($username, $action, substr($keterangan, 0, 60000));
    }
}
