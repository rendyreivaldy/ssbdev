<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Utility
{

	public static function getProjectRootFilePath()
	{
        require_once '.'.DIRECTORY_SEPARATOR.'environment.php';

		$pathInPieces = explode(DIRECTORY_SEPARATOR , __DIR__);
        if(IS_WINDOWS == 1){
            return $pathInPieces[0].DIRECTORY_SEPARATOR.
                $pathInPieces[1].DIRECTORY_SEPARATOR.
                $pathInPieces[2].DIRECTORY_SEPARATOR.
                $pathInPieces[3].DIRECTORY_SEPARATOR;
        } else {
            return $pathInPieces[0].DIRECTORY_SEPARATOR.
                $pathInPieces[1].DIRECTORY_SEPARATOR.
                $pathInPieces[2].DIRECTORY_SEPARATOR.
                $pathInPieces[3].DIRECTORY_SEPARATOR
                .$pathInPieces[4].DIRECTORY_SEPARATOR       // kalo windows ini di hide nih
                ;
        }
	}

    public static function inputClean($data){
        return $this->security->xss_clean($data);
    }

    public static function everyControllerConstruct($object){
        if($object->session->userdata('status') != "login") redirect('/Login', 'refresh');
        if(!$object->ModelUser->checkMenuRole(get_class($object), $object)) show_error('user role anda tidak memiliki akses ke menu ini', 403, $heading = 'destination : ' . get_class($object));
        $object->session->set_userdata('previous_page', uri_string());
    }

    public static function getNameAndRole($object){
        $roleDesc = "";
        $query = $object->db->query(
            "SELECT a.nama, b.role FROM user a
            LEFT JOIN user_role b ON a.id_role = b.id
            WHERE a.id = " . $object->session->userdata('id')
            , FALSE
        );
        $dataFinal = $query->result_array();
        foreach ($dataFinal as $key) {
            $roleDesc = $key['role'];
        }

        return array(
            "nama" => $object->session->userdata('nama'),
            "role" => $roleDesc
        );
    }

    public static function getMenuByRole($object){
        $finalArray = array();
        $query = $object->db->query(
            "SELECT
            *
            FROM menu 
            WHERE user_role = " . $object->session->userdata('role') . 
            " AND id_parent = 0 AND active = 1"
            , FALSE
        );
        $dataFinal = $query->result_array();
        foreach ($dataFinal as $key) {
            $queryTwo = $object->db->query(
                "SELECT
                name,
                url
                FROM menu 
                WHERE id_parent = " . $key['id'] . 
                " AND active = 1
                AND user_role = " . $object->session->userdata('role')
                , FALSE
            );
            $dataFinalTwo = $queryTwo->result_array();
            array_push($finalArray, array(
                'parentmenu' => array(
                    "name" => $key['name']
                    , "icon" => $key['icon']
                    ),
                'childmenu' => $dataFinalTwo,
                )
            );
        }
        return $finalArray;
    }

    public static function formatNumberRaw($data) {
        return number_format($data, 2, ".", ",");
    }

    public static function formatNumberWithBackCommaNumbers($data){
        $dataSplitted = explode('.', $data);
        return number_format($dataSplitted[0], 0, ".", ",") . "." . $dataSplitted[1];
    }

    public static function getAppsName(){
        $pathInPieces = explode(DIRECTORY_SEPARATOR , FCPATH);
        return $pathInPieces[count($pathInPieces)-2];
    }

    public static function getAppsNameParam($object){
        return $object->ModelParam->getParameter('apps_name');
    }

    public static function getSessionData($object, $key){
        $object->load->library('session');
        return $object->session->userdata($key);
    }

    public static function getParameterData($key){
        $ci = new CI_Controller();
        $ci =& get_instance();
        $ci->load->model('ModelParam');
        return $ci->ModelParam->getParameter($key);
    }

    public static function getArrayFromFile($filename)
    {
    	$configFilePath = Utility::getProjectRootFilePath() . "files/configs/";
        $string = file_get_contents($configFilePath . $filename . ".txt");
        return json_decode($string, true);
    }

    public static function getTextFromFile($filename)
    {
        $configFilePath = Utility::getProjectRootFilePath() . "files/text/";
        $string = file_get_contents($configFilePath . $filename . ".txt");
        return json_decode($string, true);
    }

    public static function putTextToFile($filename, $content)
    {
        $configFilePath = Utility::getProjectRootFilePath() . "files/text/";
        $string = file_put_contents($configFilePath . $filename . ".txt", $content);
        return json_decode($string, true);
    }

    public static function getCurrentDateTimeWithFormat($format)
    {
    	date_default_timezone_set("Asia/Jakarta");
		return date($format);  // "Y-m-d H:i:s" -> normal mySQL
    }

    public static function changeDateFormat($originalDate, $newDateFormatString){
        return  date("d-m-Y", strtotime($originalDate));
    }

    public static function getPath(){
        return fileutility::getArrayFromFile('general')['URLHEAD'] . $_SERVER['SERVER_NAME'] 
        . fileutility::getArrayFromFile('general')['PROJECTFOLDERNAME'] . "/";
    }

    public static function loginLDAP($username,$password){
        
        //konek ke LDAP
        $ldap = ldap_connect("btn.co.id");
        if($ldap)
        {
            $userldap = $username . "@btn.co.id";

            $bind = @ldap_bind($ldap, $userldap, $password);
            if ($bind) {
                return true;
            } else {
                return false;
            }
        }else{
           return false;
        }
    }

    public static function decryptPassword($password){
        $key = pack("H*", "1a1b3d4f5a3b5c9d9a8b6a6b6e6a5e7b");
        $iv =  pack("H*", "5a9b9d9b9e9f9a9b9f9e9a9d5c4b5e6b");
        $encrypted = base64_decode($password);
        $shown = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $encrypted, MCRYPT_MODE_CBC, $iv);
        //$shown = trim($shown);
        $password = preg_replace('/[\x00-\x1f]/', '', $shown);
        return $password;
    }

    public static function CryptoJSAesDecrypt($jsonString){
        $passphrase = "1a1b3d4f5a3b5c9d9a8b6a6b6e6a5e7b";
        $jsondata = json_decode($jsonString, true);
        try {
            $salt = hex2bin($jsondata["salt"]);
            $iv  = hex2bin($jsondata["iv"]);          
        } catch(Exception $e) { return null; }
        $ciphertext = base64_decode($jsondata["ciphertext"]);
        $iterations = 999; //same as js encrypting 
        $key = hash_pbkdf2("sha512", $passphrase, $salt, $iterations, 64);
        $decrypted= openssl_decrypt($ciphertext , 'aes-256-cbc', hex2bin($key), OPENSSL_RAW_DATA, $iv);
        return $decrypted;
    }

    public static function encryptString($object, $data) {
        $object->load->library('encrypt');
        return $object->encrypt->encode($data);
    }
    
    public static function decryptString($object, $data){
        $object->load->library('encrypt');
        return $object->encrypt->decode($data);
    }

    public static function getSoapWs($url, $params, $function){
        $client = new SoapClient($url);
        $response = $client->__soapCall($function, array($params));
        return $response;
    }

    public static function initCurl($url) {

        ini_set('max_execution_time', 300);     // in seconds
    
        // require_once("stringconfig.php");
        // create curl resource
        $ch = curl_init();
        // set options
        curl_setopt($ch, CURLOPT_URL, $url);

        // if(getParameterData('curl_proxy') == "1") {
            $proxy = '172.16.10.10:8080';
            curl_setopt($ch, CURLOPT_PROXY, $proxy);
        // }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // read more about HTTPS http://stackoverflow.com/questions/31162706/how-to-scrape-a-ssl-or-https-url/31164409#31164409
        curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

        // $output contains the output string
        $output = curl_exec($ch);
        // close curl resource to free up system resources
        curl_close($ch); 
        return $output;
    }

    public static function convertBase64ToImage($ktp, $base64_string, $type ){
        // $dateFileName = date('Ymd') . $milliseconds = round(microtime(true) * 1000);
        $dateFileName = date('Ymd');
        $partEasyPath = $dateFileName . "_" . $type . "_" . $ktp . ".jpg";
        $output_file = FCPATH . 'files'. DIRECTORY_SEPARATOR .'ppdpp' . DIRECTORY_SEPARATOR . $partEasyPath;
        $decoded = base64_decode($base64_string);
        file_put_contents($output_file, $decoded);
        $easyPath = base_url('files/ppdpp/') . $partEasyPath;
        return $easyPath; 
    }

    public static function getClientIPAddress() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        // return $ipaddress;

        return '10.14.1.66';
    }

    public static function initCurlPost($paramArray, $url) {

        ini_set('max_execution_time', 300);     // in seconds

        $parameter = "";
        foreach ($paramArray as $key => $value) {
            if($parameter == "")
                $parameter = $key . "=" . $value;
            else
                $parameter = $parameter . "&" . $key . "=" . $value;
        }
    
        // require_once("stringconfig.php");
        // create curl resource
        $ch = curl_init();
        // set options
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $parameter);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // $output contains the output string
        $output = curl_exec($ch);
        // close curl resource to free up system resources
        curl_close($ch); 
        return $output;
    }

    public static function postBodyRawCurl($jsonData, $url){
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $jsonData);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(   
            'Content-Type: application/json',
            'test: token'
            ));
        $output = curl_exec($curl);
        return $output;
    }

    public static function postBodyRawCurlCustomHeader($jsonData, $url, $header){
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $output = curl_exec($curl);
        return $output;
    }

    public static function convertTime($source, $targetFormat){
        // $source = '2012-07-31';
        $date = new DateTime($source);
        return $date->format($targetFormat); // 31.07.2012
        // echo $date->format('d.m.Y'); // 31.07.2012
        // echo $date->format('d-m-Y'); // 31-07-2012
    }

    public static function curlSendFile($fileName, $url){
        if (function_exists('curl_file_create')) { // php 5.5+
            $cFile = curl_file_create($fileName);
        } else { // 
            $cFile = '@' . realpath($fileName);
        }
        $post = array(/*'extra_info' => '123456',*/'file_contents'=> $cFile);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        $result = curl_exec ($ch);
        curl_close ($ch);
        return $result;
    }

    public static function sftpSendFile($filePath, $params){
        $localFile='/files/myfile.zip';
        $remoteFile='/filesDir/myfile.zip';
        $host = "sftp.example.com";
        $port = 22;
        $user = "sftp-user";
        $pass = "abc123456789";

        $result = array();

        $ftp_server = $params['ftpServer'];
        $ftp_port = $params['ftpPort'];
        $ftp_user_name = $params['ftpUserName'];
        $ftp_user_pass =  $params['ftpUserPass'];
        $file = $filePath; //tobe uploaded
        $remote_file = $params['remote_file'];
        $fileName = basename($filePath);

        $connection = ssh2_connect($ftp_server, $ftp_port);
        ssh2_auth_password($connection, $ftp_user_name, $ftp_user_pass);
        $sftp = ssh2_sftp($connection);

        $stream = fopen("ssh2.sftp://$sftp/" . $remote_file . '/' . $fileName, 'w');
        $file = file_get_contents($file);
        fwrite($stream, $file);
        fclose($stream);

        $result['status'] = true;
        if(file_exists($file))
            unlink($file);

        return $result;
    }

    public function cUrlSftp($filePath, $params){
        $ftp_server = $params['ftpServer'];
        $ftp_port = $params['ftpPort'];
        $ftp_user_name = $params['ftpUserName'];
        $ftp_user_pass =  $params['ftpUserPass'];
        $file = $filePath; //tobe uploaded
        $remote_file = $params['remote_file'];
        $fileName = basename($filePath);

        $result = array();

        $ch = curl_init('sftp://' . $ftp_server . ':' . $ftp_port . "/" . $remote_file . '/' . $fileName);
        $fh = fopen($file, 'r');
        if ($fh) {
            curl_setopt($ch, CURLOPT_USERPWD, $ftp_user_name . ':' . $ftp_user_pass);
            curl_setopt($ch, CURLOPT_UPLOAD, true);
            curl_setopt($ch, CURLOPT_PROTOCOLS, CURLPROTO_SFTP);
            curl_setopt($ch, CURLOPT_INFILE, $fh);
            curl_setopt($ch, CURLOPT_INFILESIZE, filesize($file));
            curl_setopt($ch, CURLOPT_VERBOSE, true);
         
            $verbose = fopen('php://temp', 'w+');
            curl_setopt($ch, CURLOPT_STDERR, $verbose);
         
            $response = curl_exec($ch);
            $error = curl_error($ch);
            curl_close($ch);
            fclose($fh);
         
            if ($response) {
                $result['status'] = true;
                if(file_exists($file))
                    unlink($file); 
            } else {
                rewind($verbose);
                $verboseLog = stream_get_contents($verbose);
                $result['status'] = false;
                $result['message'] =  "There was a problem while uploading file : " . $verboseLog;
            }
            return $result;
        }
    }

    public static function ftpSendFile($filePath, $params){
        $result = array();

        $ftp_server = $params['ftpServer'];
        $ftp_port = $params['ftpPort'];
        $ftp_user_name = $params['ftpUserName'];
        $ftp_user_pass =  $params['ftpUserPass'];
        $file = $filePath; //tobe uploaded
        $remote_file = $params['remote_file'];
        $fileName = basename($filePath);

        // set up basic connection, first -> normal, second -> SFTP
        // $conn_id = ftp_connect($ftp_server, $ftp_port);
        $conn_id = ftp_ssl_connect($ftp_server, $ftp_port);

        // login with username and password
        $login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);

        // upload a file
        ftp_pasv($conn_id, true);
        if (ftp_put($conn_id, $remote_file . '/' . $fileName, $file, FTP_BINARY)) {
            $result['status'] = true;
        } else {
            $result['status'] = false;
            $result['message'] =  "There was a problem while uploading file";
        }
        // close the connection
        ftp_close($conn_id);

        if(file_exists($file)) 
            unlink($file);

        return $result;
    }

    public static function sftpPHPSecLibSend($filePath, $params){
        
        // ini_set('MAX_EXECUTION_TIME', '-1');
        ini_set('max_execution_time', 0); 
        
        $result = array();

        $ftp_server = $params['ftpServer'];
        $ftp_port = $params['ftpPort'];
        $ftp_user_name = $params['ftpUserName'];
        $ftp_user_pass =  $params['ftpUserPass'];
        $file = $filePath; //tobe uploaded
        $remote_file = $params['remote_file'];
        $fileName = basename($filePath);

        set_include_path(get_include_path() . PATH_SEPARATOR . APPPATH . 'third_party/phpseclib');
        include_once(APPPATH.'third_party/phpseclib/Net/SFTP.php');


        $sftp = new Net_SFTP($ftp_server, $ftp_port);
        if (!$sftp->login($ftp_user_name, $ftp_user_pass)) {
            $result['status'] = false;
            $result['message'] =  "Login sftp Failed";
        }

        // puts a three-byte file named filename.remote on the SFTP server
        // parameter kedua file asal, parameter pertama target file
        $data = file_get_contents($file);
        $sftp->put($remote_file . '/' . $fileName, $data);
        // puts an x-byte file named filename.remote on the SFTP server,
        // where x is the size of filename.local
        // $sftp->put('filename.remote', 'filename.local', NET_SFTP_LOCAL_FILE);

        // if(file_exists($file)) 
        //     unlink($file);

        $result['status'] = true;
        return $result;
    }

    public static function deleteFileZip($file){
        if(file_exists($file)) 
            unlink($file);
    }

    public static function exportToCSV($filename, $arrayData){

        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: application/vnd.ms-excel;");
        header("Pragma: no-cache");
        header("Expires: 0");

        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header('Content-Type: application/octet-stream');
        header("Content-Transfer-Encoding: binary");

        $out = fopen("php://output", 'w');
        foreach ($arrayData as $data)
        {
            fputcsv($out, $data,"\t");
        }
        fclose($out);
    }

    public function generateRandomNumber(){
        return rand(100000,999999);
    }

    public function parseXML($data){
        $xml = simplexml_load_string($data) or die("Error: Cannot create object");
        return json_decode(json_encode($xml), true);
    }

}
