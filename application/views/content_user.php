<script src="/<?php echo $appname; ?>/files/assets/js/jquery.dataTables.min.js"></script>
<script src="/<?php echo $appname; ?>/files/assets/js/table.js"></script>
<link href="/<?php echo $appname; ?>/files/assets/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="/<?php echo $appname; ?>/files/assets/css/table.css" rel="stylesheet">

<div class="container-fluid dashboard-content">
    <!-- ============================================================== -->
    <!-- pageheader -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="page-header">
                <h2 class="pageheader-title">Manajemen User</h2>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- end pageheader -->
    <!-- ============================================================== -->
    
    <div class="row card">
        <div class="card-body">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="row">
                    <div class="panel panel-primary filterable">
                        <div class="panel-heading">
                            <h3 class="panel-title"></h3>
                            <div class="pull-right">
                            </div>
                        </div>
                        <table id="example" class="display" style="width: 100%;">
                            <thead>
                                <tr class="filters">
                                    <th>Username</th>
                                    <th>Role</th>
                                    <th>Nama</th>
                                    <th>Blocked</th>
                                    <th>Last Login Date</th>
                                    <!-- <th>Created By</th> -->
                                    <!-- <th>Updated By</th> -->
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    foreach ($dataUserAll as $key) {
                                        echo '<tr>';
                                        echo '<td>' . $key['username'] . '</td>';
                                        echo '<td>' . $key['id_role'] . '</td>';
                                        echo '<td>' . $key['nama'] . '</td>';
                                        echo '<td>' . $key['is_blocked'] . '</td>';
                                        echo '<td>' . $key['last_login_date'] . '</td>';
                                        // echo '<td>' . $key['created_by'] . '</td>';
                                        // echo '<td>' . $key['updated_by'] . '</td>';
                                        echo '<td><a class="btn btn-sm btn-outline-light" href="' . site_url('User/editUser/' . $key['id']) . '"><i class="far fa-edit"></i></a>';
                                        echo '<a href="#" onClick="deleteOnClick(' . $key['id'] . ')" data-toggle="modal" data-target="#exampleModal" class="btn btn-sm btn-outline-light" ><i class="far fa-trash-alt"></i></a></td>';
                                        echo '</tr>';
                                    }
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Username</th>
                                    <th>Role</th>
                                    <th>Nama</th>
                                    <th>Blocked</th>
                                    <th>Last Login Date</th>
                                    <!-- <th>Created By</th> -->
                                    <!-- <th>Updated By</th> -->
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="text" id="hiddenid" style="display: none;">

    <!-- DATA MODAL DIALOG POP UP -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Hapus Data</h5>
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </a>
                </div>
                <div class="modal-body">
                    <p>Apakah Anda yakin ingin menghapus data ini ?</p>
                </div>
                <div class="modal-footer">
                    <a href="#" onClick="deleteConfirmationOnClick()" class="btn btn-primary">Hapus</a>
                    <a href="#" class="btn btn-secondary" data-dismiss="modal">Batal</a>
                </div>
            </div>
        </div>
    </div>
    <!-- DATA MODAL DIALOG POP UP -->

    <div class="row card">
        <div class="card-body">
            <a href="<?php echo site_url('User/createUser'); ?>" class="btn btn-primary btn-block">Buat User Baru</a>
        </div>
    </div>
</div>

<script type="text/javascript">
    
    var hiddenId = document.getElementById("hiddenid");

    function deleteOnClick(id){
        hiddenid.value = id;
    }

    function deleteConfirmationOnClick(){
        window.location.href = "<?php echo site_url('User/deleteUser/') ?>" + hiddenid.value;
    }

</script>
