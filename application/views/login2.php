<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login V1</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="/<?php echo $appname; ?>/masterview/assets/login/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/<?php echo $appname; ?>/masterview/assets/login/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/<?php echo $appname; ?>/masterview/assets/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/<?php echo $appname; ?>/masterview/assets/login/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="/<?php echo $appname; ?>/masterview/assets/login/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/<?php echo $appname; ?>/masterview/assets/login/vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/<?php echo $appname; ?>/masterview/assets/login/css/util.css">
	<link rel="stylesheet" type="text/css" href="/<?php echo $appname; ?>/masterview/assets/login/css/main.css">
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-pic js-tilt" data-tilt>
					<img src="/<?php echo $appname; ?>/masterview/assets/login/images/img-01.png" alt="IMG">
				</div>

				<form class="login100-form validate-form">
					<span class="login100-form-title">
						Member Login
					</span>

					<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
						<input class="input100" type="text" name="email" placeholder="Email">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Password is required">
						<input class="input100" type="password" name="pass" placeholder="Password">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>
					
					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							Masuk
						</button>
					</div>

					<div class="text-center p-t-136">
						<a class="txt2" href="#">
							Buat Akun Baru
							<i class="fa fa-long-arrow-right m-l-5" aria-hidden="true" data-toggle="modal" data-target="#exampleModal"></i>
						</a>
					</div>
				    <!-- MODAL CREATE AN ACCOUNT -->
				    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				        <div class="modal-dialog" role="document">
				            <div class="modal-content">
				                <div class="modal-header">
				                    <h5 class="modal-title" id="exampleModalLabel">Create an Account</h5>
				                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
				                        <span aria-hidden="true">&times;</span>
				                    </a>
				                </div>
				                <div class="modal-body">
				                    <div class="card-body">
				                        <form>
				                            <div class="form-group">
				                                <input class="form-control form-control-lg" id="username_create" type="text" placeholder="Username" autocomplete="off">
				                            </div>
				                            <div class="form-group">
				                                <input class="form-control form-control-lg" id="password_create" type="password" placeholder="Password">
				                            </div>
				                            <div class="form-group">
				                                <input class="form-control form-control-lg" id="password_confirm_create" type="password" placeholder="Confirm Password">
				                            </div>
				                            <button type="submit" class="btn btn-primary btn-lg btn-block">Register</button>
				                        </form>
				                    </div>
				                </div>
				                <div class="modal-footer">
				                    <a href="#" class="btn btn-secondary" data-dismiss="modal">Close</a>
				                </div>
				            </div>
				        </div>
				    </div>
				    <!-- MODAL CREATE AN ACCOUNT END -->
				</form>
			</div>
		</div>
	</div>
	
	

	
<!--===============================================================================================-->	
	<script src="/<?php echo $appname; ?>/masterview/assets/login/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="/<?php echo $appname; ?>/masterview/assets/login/vendor/bootstrap/js/popper.js"></script>
	<script src="/<?php echo $appname; ?>/masterview/assets/login/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="/<?php echo $appname; ?>/masterview/assets/login/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="/<?php echo $appname; ?>/masterview/assets/login/vendor/tilt/tilt.jquery.min.js"></script>
	<script >
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>
<!--===============================================================================================-->
	<script src="/<?php echo $appname; ?>/masterview/assets/login/js/main.js"></script>

</body>
</html>