<div class="container-fluid dashboard-content">
    <div class="row">
        <div class="col-xl-10">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header" id="top">
                        <h2 class="pageheader-title">Detail Data User</h2>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- basic form  -->
            <!-- ============================================================== -->

            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <div class="card-body">
                            <form id="validationform" data-parsley-validate="" novalidate="">
                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label text-sm-right">Nama</label>
                                    <div class="col-12 col-sm-8 col-lg-6">
                                        <h2><?php echo $nama; ?></h2>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label text-sm-right">Username</label>
                                    <div class="col-12 col-sm-8 col-lg-6">
                                        <h2><?php echo $username; ?></h2>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label text-sm-right">Role</label>
                                    <div class="col-12 col-sm-8 col-lg-6">
                                        <h2><?php echo $role; ?></h2>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label text-sm-right">Is Blocked</label>
                                    <div class="col-12 col-sm-8 col-lg-6">
                                        <h2><?php echo $isblocked; ?></h2>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label text-sm-right">Last Login Date</label>
                                    <div class="col-12 col-sm-8 col-lg-6">
                                        <h2><?php echo $lastlogindate; ?></h2>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label text-sm-right">Created By</label>
                                    <div class="col-12 col-sm-8 col-lg-6">
                                        <h2><?php echo $createdby; ?></h2>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label text-sm-right">Updated By</label>
                                    <div class="col-12 col-sm-8 col-lg-6">
                                        <h2><?php echo $updatedby; ?></h2>
                                    </div>
                                </div>
                            </form>
                            <a href="<?php echo site_url('User/'); ?>" class="btn btn-secondary">Kembali</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end basic form  -->
            <!-- ============================================================== -->
        </div>
    </div>
</div>