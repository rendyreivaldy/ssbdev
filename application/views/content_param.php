<script src="/<?php echo $appname; ?>/files/assets/js/jquery.dataTables.min.js"></script>
<script src="/<?php echo $appname; ?>/files/assets/js/table.js"></script>
<link href="/<?php echo $appname; ?>/files/assets/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="/<?php echo $appname; ?>/files/assets/css/table.css" rel="stylesheet">

<div class="container-fluid dashboard-content">
    <!-- ============================================================== -->
    <!-- pageheader -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="page-header">
                <h2 class="pageheader-title">Manajemen Parameter</h2>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- end pageheader -->
    <!-- ============================================================== -->
    
    <div class="row card">
        <div class="card-body">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="row">
                    <div class="panel panel-primary filterable">
                        <div class="panel-heading">
                            <h3 class="panel-title"></h3>
                            <div class="pull-right">
                            </div>
                        </div>
                        <table id="example" class="display" style="width: 100%;">
                            <thead>
                                <tr class="filters">
                                    <th>Name</th>
                                    <th>Value</th>
                                    <th>Keterangan</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    foreach ($dataParamAll as $key) {
                                        echo '<tr>';
                                        echo '<td>' . $key['name'] . '</td>';
                                        echo '<td>' . $key['value'] . '</td>';
                                        echo '<td>' . $key['keterangan'] . '</td>';
                                        echo '<td><a class="btn btn-sm btn-outline-light" href="' . site_url('Parameter/editParam/' . $key['id']) . '"><i class="far fa-edit"></i></a>';
                                        echo '<a href="#" onClick="deleteOnClick(' . $key['id'] . ')" data-toggle="modal" data-target="#exampleModal" class="btn btn-sm btn-outline-light" ><i class="far fa-trash-alt"></i></a></td>';
                                        echo '</tr>';
                                    }
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Name</th>
                                    <th>Value</th>
                                    <th>Keterangan</th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="text" id="hiddenid" style="display: none;">

    <!-- DATA MODAL DIALOG POP UP -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Hapus Data</h5>
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </a>
                </div>
                <div class="modal-body">
                    <p>Apakah Anda yakin ingin menghapus data ini ?</p>
                </div>
                <div class="modal-footer">
                    <a href="#" onClick="deleteConfirmationOnClick()" class="btn btn-primary">Hapus</a>
                    <a href="#" class="btn btn-secondary" data-dismiss="modal">Batal</a>
                </div>
            </div>
        </div>
    </div>
    <!-- DATA MODAL DIALOG POP UP -->

    <div class="row card">
        <div class="card-body">
            <a href="<?php echo site_url('Parameter/createParam'); ?>" class="btn btn-primary btn-block">Buat Parameter Baru</a>
        </div>
    </div>
</div>

<script type="text/javascript">
    
    var hiddenId = document.getElementById("hiddenid");

    function deleteOnClick(id){
        hiddenid.value = id;
    }

    function deleteConfirmationOnClick(){
        window.location.href = "<?php echo site_url('Parameter/deleteParam/') ?>" + hiddenid.value;
    }

</script>
