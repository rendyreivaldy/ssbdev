<script src="/<?php echo $appname; ?>/files/assets/js/jquery.dataTables.min.js"></script>
<script src="/<?php echo $appname; ?>/files/assets/js/table.js"></script>
<link href="/<?php echo $appname; ?>/files/assets/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="/<?php echo $appname; ?>/files/assets/css/table.css" rel="stylesheet">

<div class="container-fluid dashboard-content">
    <!-- ============================================================== -->
    <!-- pageheader -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="page-header">
                <h2 class="pageheader-title">Data SSB</h2>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- end pageheader -->
    <!-- ============================================================== -->
    
    <div class="row card">
        <div class="card-body">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="row">
                    <div class="panel panel-primary filterable">
                        <div class="panel-heading">
                            <h3 class="panel-title"></h3>
                            <div class="pull-right">
                            </div>
                        </div>
                        <hr>
                        <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th width="43%">Nama Pemohon</th>
                                    <th width="35%">NIK</th>
                                    <th width="15%">Kode Bank</th>
                                    <th width="7%"></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <div class="float-right mt-2">
                            <i>Total Data : <span id="totalData"></span></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="text" id="hiddenid" style="display: none;">

</div>

<script type="text/javascript">
    
    var hiddenId = document.getElementById("hiddenid");

    function deleteOnClick(id){
        hiddenid.value = id;
    }

    function deleteConfirmationOnClick(){
        window.location.href = "<?php echo site_url('Menu/deleteMenu/') ?>" + hiddenid.value;
    }


    $(document).ready(function() {
     
        //datatables
        table = $('#table').DataTable({ 
     
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.
     
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('DataUjiList/')?>",
                "type": "POST",
                "data": function ( data ) {
                    
                }
            },
     
            //Set column definition initialisation properties.
            "columnDefs": [
                { 
                    "targets": [ 3 ], //first column / numbering column
                    "orderable": false, //set not orderable
                },
            ],
            "columns": [
                { "data": null,
                    "render": function ( data, type, full, meta ) { 
                        return data[2];
                    }
                },
                { "data": null,
                    "render": function ( data, type, full, meta ) { 
                        return data[3];
                    }
                }, 
                { "data": null,
                    "render": function ( data, type, full, meta ) { 
                        return data[4];
                    }
                },
                { "data": null, 
                    "render" : function ( data, type, row, meta ) {
                        return '<a class="btn btn-sm btn-outline-light" href="<?php echo site_url('Ssb/detail/'); ?>' + data[1] + '"><i class="fas fa-search"></i></a>';
                    }
                }
                
            ],
            initComplete: function() {
                $('#totalData').html(table.page.info().recordsTotal);
            }
     
        });
     
        $('#btn-filter').click(function(){ //button filter event click
            table.ajax.reload();  //just reload table
        });
        $('#btn-reset').click(function(){ //button reset event click
            $('#form-filter')[0].reset();
            table.ajax.reload();  //just reload table
        });
    });

</script>
