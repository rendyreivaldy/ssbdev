<div class="container-fluid dashboard-content">
    <div class="row">
        <div class="col-xl-10">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header" id="top">
                        <h2 class="pageheader-title">Detail Data User</h2>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- basic form  -->
            <!-- ============================================================== -->

            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="post" action="<?php echo site_url('User/submitCreateUser'); ?>">
                                <div class="alert alert-danger" role="alert" style="display: <?php echo $notificationDisplay; ?>">
                                    <?php echo $notificationMessage; ?>
                                </div>
                                <div class="form-group">
                                    <label for="nama" class="col-form-label">Nama</label>
                                    <input id="nama" name="nama" type="text" class="form-control" value="<?php echo $nama; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="username" class="col-form-label">Username</label>
                                    <input id="username" name="username" type="text" class="form-control" value="<?php echo $username; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input id="password" name="password" type="password" class="form-control" onkeyup="passwordValidation()">
                                </div>
                                <div class="form-group">
                                    <label for="passwordkonfirm">Konfirmasi Password</label>
                                    <input id="passwordkonfirm" name="passwordkonfirm" type="password"  class="form-control" onkeyup="passwordValidation()">
                                </div>
                                <div class="alert alert-danger" id="warning" role="alert">
                                    Password dan Konfirmasi Password Belum Diisi
                                </div>
                                <div class="form-group">
                                    <label for="input-select">Role</label>
                                    <select class="form-control" id="role" name="role">
                                        <?php 
                                            foreach ($roleddl as $key) {
                                                if($key['id'] == $role){
                                                    echo '<option value="' . $key['id'] . '" selected>' . $key['role'] . '</option>';
                                                } else {
                                                    echo '<option value="' . $key['id'] . '">' . $key['role'] . '</option>';
                                                }
                                            } 
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" id="isblocked" name="isblocked" class="custom-control-input" <?php echo $isblocked ? 'checked=""' : ''; ?>><span class="custom-control-label">Block User ?</span>
                                    </label>
                                </div>
                                <button id="btnsave" type="submit" name="submit" value="login" class="btn btn-primary">Simpan</button>
                                <a href="<?php echo site_url('User/'); ?>" class="btn btn-danger">Kembali</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end basic form  -->
            <!-- ============================================================== -->
        </div>
    </div>
</div>

<script type="text/javascript">  
    var passwordField = document.getElementById("password");
    var passwordConfirmField = document.getElementById("passwordkonfirm");
    var username = document.getElementById("username");
    var name = document.getElementById("name");
    var btnSave = document.getElementById("btnsave");
    var warning = document.getElementById("warning");

    // make sure the password is correctly exact. if not, disable the registration button

    function passwordValidation(){
        if(passwordField.value != passwordConfirmField.value){
            warning.innerHTML = "Password Tidak Sama";
            warning.className = "alert alert-danger";
            btnSave.disabled = true;
        } else if (passwordField.value == "" && passwordConfirmField.value == ""){
            warning.className = "alert alert-danger";
            warning.innerHTML = "Password dan Konfirmasi Password Belum Diisi";
            btnSave.disabled = false;
        } else {
            warning.className = "alert alert-success";
            warning.innerHTML = "Password Sama";
            btnSave.disabled = false;
        }
    }

</script>