<link rel="stylesheet" href="<?php echo base_url(); ?>files/assets/css/jquery-ui.css">
<script src="<?php echo base_url(); ?>files/assets/js/jquery-1.12.4.js"></script>
<script src="<?php echo base_url(); ?>files/assets/js/jquery-ui.js"></script>
<style type="text/css">
    .ui-datepicker-calendar {
        display: block;
    }
</style>

<div class="container-fluid dashboard-content">
    <div class="row">
        <div class="col-xl-10">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header" id="top">
                        <h2 class="pageheader-title">2301 - Penarikan Tunai Dari Tabungan</h2>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- basic form  -->
            <!-- ============================================================== -->

            <form method="post" action="<?php echo site_url('Menu/submitCreateMenu'); ?>">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="alert alert-danger" role="alert" style="display: <?php echo $notificationDisplay; ?>">
                                    <?php echo $notificationMessage; ?>
                                </div>
                                <div class="form-group">
                                    <label for="tglefektif" class="col-form-label">Tanggal Efektif</label>
                                    <input class="form-control" type="text" id="tglefektif" value="<?php echo ''; ?>">
                                </div>
                                <div class="form-row">
                                    <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 mb-2">
                                        <label for="nilaidibayar">Nilai Dibayar</label>
                                        <input id="nilaidibayar" name="nilaidibayar" type="text" class="form-control" value="<?php echo ''; ?>">
                                    </div>
                                    <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 mb-2">
                                    </div>
                                    <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 mb-2">
                                        <label for="matauang">Mata Uang</label>
                                        <select class="form-control" id="matauang" name="matauang">
                                            <?php 
                                                foreach ($idmatauangddl as $key) {
                                                    if($key['id'] == $matauang){
                                                        echo '<option value="' . $key['id'] . '" selected>' . $key['name'] . '</option>';
                                                    } else {
                                                        echo '<option value="' . $key['id'] . '">' . $key['name'] . '</option>';
                                                    }
                                                } 
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="biaya" class="col-form-label">Biaya</label>
                                    <input id="biaya" name="biaya" type="text" class="form-control" value="<?php echo ''; ?>">
                                </div>
                                <div class="form-row">
                                    <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 mb-2">
                                        <label for="rekeningdebit">Rekening Debit</label>
                                        <input id="rekeningdebit" name="rekeningdebit" type="text" class="form-control" value="<?php echo ''; ?>">
                                    </div>
                                    <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 mb-2">
                                    </div>
                                    <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 mb-2">
                                        <label for="rekeningdebit"></label>
                                        <input id="rekdebitmatauang" name="rekdebitmatauang" type="text" class="form-control" value="<?php echo ''; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="nama" class="col-form-label">Nama</label>
                                    <input id="nama" name="nama" type="text" class="form-control" value="<?php echo ''; ?>">
                                </div>
                                <div class="form-row">
                                    <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 mb-2">
                                        <label for="noseributab" class="col-form-label">No Seri Butab</label>
                                        <input id="noseributab" name="noseributab" type="text" class="form-control" value="<?php echo ''; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="catatan" class="col-form-label">Catatan</label>
                                    <input id="catatan" name="catatan" type="text" class="form-control" value="<?php echo ''; ?>">
                                </div>
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" <?php //checked="" ?> class="custom-control-input"><span class="custom-control-label">Request Nasabah</span>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="nilaidebit" class="col-form-label">Nilai Debit</label>
                                    <input class="form-control" type="text" id="nilaidebit" value="<?php echo ''; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="saldobutab" class="col-form-label">Saldo Buku Tabungan</label>
                                    <input id="saldobutab" name="saldobutab" type="text" class="form-control" value="<?php echo ''; ?>">
                                </div>
                                <div class="form-row">
                                    <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 mb-2">
                                        <label for="nobaristbutab" class="col-form-label">No Baris Butab</label>
                                        <input id="nobaristbutab" name="nobaristbutab" type="text" class="form-control" value="<?php echo ''; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="namano" class="col-form-label">Nama Penyetor/No HP</label>
                                    <input id="namano" name="namano" type="text" class="form-control" value="<?php echo ''; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="noktp" class="col-form-label">No KTP</label>
                                    <input id="noktp" name="noktp" type="text" class="form-control" value="<?php echo ''; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="saldobutab" class="col-form-label">Sumber Dana</label>
                                    <input id="saldobutab" name="saldobutab" type="text" class="form-control" value="<?php echo ''; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="tujuantransaksi" class="col-form-label">Tujuan Transaksi</label>
                                    <input id="tujuantransaksi" name="tujuantransaksi" type="text" class="form-control" value="<?php echo ''; ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <button id="btnsave" type="submit" name="submit" value="login" class="btn btn-primary">Simpan</button>
                        <a href="<?php echo site_url('Menu/'); ?>" class="btn btn-danger">Kembali</a>
                    </div>
                </div>
            </form>
            <!-- ============================================================== -->
            <!-- end basic form  -->
            <!-- ============================================================== -->
        </div>
    </div>
</div>

<script type="text/javascript">
    

    $( function() {
        $( "#tglefektif" ).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd-mm-yy',
            // showButtonPanel: true,
            // onClose: function(dateText, inst) { 
            //     var date = new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay);
            //     var monthReal = date.getMonth() + 1;
            //     if(monthReal < 10){
            //         monthReal = "0" + monthReal;
            //     }
            //     var day = date.getDate();
            //     if(day < 10){
            //         day = "0" + day;
            //     }
            //     var today = new Date();
            //     var time = date.getFullYear() + "-" + monthReal + "-" + day + " " + today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
            //     $('#tglefektif').val(time);
            // }
        })
    });

</script>