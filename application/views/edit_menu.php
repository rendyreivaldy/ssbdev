<div class="container-fluid dashboard-content">
    <div class="row">
        <div class="col-xl-10">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header" id="top">
                        <h2 class="pageheader-title">Detail Data Menu</h2>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- basic form  -->
            <!-- ============================================================== -->

            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="post" action="<?php echo site_url('Menu/submitEditMenu/' . $id); ?>">
                                <div class="alert alert-danger" role="alert" style="display: <?php echo $notificationDisplay; ?>">
                                    <?php echo $notificationMessage; ?>
                                </div>
                                <div class="form-group">
                                    <label for="nama" class="col-form-label">Nama</label>
                                    <input id="nama" name="nama" type="text" class="form-control" value="<?php echo $nama; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="icon" class="col-form-label">Icon Name</label>
                                    <input id="icon" name="icon" type="text" class="form-control" value="<?php echo $icon; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="input-select">Parent</label>
                                    <br><i><small>Untuk menu parent tidak perlu mengisi data ini</small></i>
                                    <select class="form-control" id="idparent" name="idparent">
                                        <?php 
                                            foreach ($idparentddl as $key) {
                                                if($key['id'] == $idparent){
                                                    echo '<option value="' . $key['id'] . '" selected>' . $key['name'] . '</option>';
                                                } else {
                                                    echo '<option value="' . $key['id'] . '">' . $key['name'] . '</option>';
                                                }
                                            } 
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="url" class="col-form-label">URL</label>
                                    <br><i><small>Untuk menu parent tidak perlu mengisi data ini</small></i>
                                    <input id="url" name="url" type="text" class="form-control" value="<?php echo $url; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="input-select">User Role</label>
                                    <select class="form-control" id="role" name="role">
                                        <?php 
                                            foreach ($roleddl as $key) {
                                                if($key['id'] == $role){
                                                    echo '<option value="' . $key['id'] . '" selected>' . $key['role'] . '</option>';
                                                } else {
                                                    echo '<option value="' . $key['id'] . '">' . $key['role'] . '</option>';
                                                }
                                            } 
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" id="active" name="active" class="custom-control-input" <?php echo $active ? 'checked=""' : ''; ?>><span class="custom-control-label">Active</span>
                                    </label>
                                </div>
                                <button id="btnsave" type="submit" name="submit" value="login" class="btn btn-primary">Simpan</button>
                                <a href="<?php echo site_url('Menu/'); ?>" class="btn btn-danger">Kembali</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end basic form  -->
            <!-- ============================================================== -->
        </div>
    </div>
</div>
