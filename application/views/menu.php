<div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav flex-column">
        <li class="nav-divider">
            Menu
        </li>
        <?php

            $counter = 1;
            foreach ($leftmenu as $key) {
                echo '<li class="nav-item">';
                echo '<a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-' . $counter . '" aria-controls="submenu-' . $counter . '"><i class="' . $key['parentmenu']['icon'] . '"></i>' . $key['parentmenu']['name'] . '</a>';
                echo '<div id="submenu-' . $counter . '" class="collapse submenu" style="">';
                echo '<ul class="nav flex-column">';
                foreach ($key['childmenu'] as $keyTwo) {
                    echo '<li class="nav-item">';
                    $url = strpos( $keyTwo['url'], '.' ) !== false ? base_url(str_replace('.', '', $keyTwo['url'])) : site_url($keyTwo['url']);
                    echo '<a class="nav-link" href="' . $url . '">' . $keyTwo['name'] . '</a>';
                    echo '</li>';
                }
                echo '</ul></div></li>';
                $counter++;
            }
        ?>
    </ul>
</div>