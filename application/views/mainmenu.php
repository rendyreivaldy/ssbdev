<?php 
    $ci = new CI_Model();
    $ci =& get_instance();
    $ci->load->model('ModelParam');
?>

<!doctype html>
<html lang="en">

<head>
    <link rel="icon" href="/<?php echo $appname . $ci->ModelParam->getParameter('tab_icon'); ?>">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo $mainappname; ?></title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/<?php echo $appname; ?>/masterview/assets/vendor/bootstrap/css/bootstrap.min.css">
    <link href="/<?php echo $appname; ?>/masterview/assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="/<?php echo $appname; ?>/masterview/assets/libs/css/style.css">
    <link rel="stylesheet" href="/<?php echo $appname; ?>/masterview/assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
    <style type="text/css">
        .footer{
            position: absolute;
            bottom: 0;
        }
    </style>
    
    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
    <script src="/<?php echo $appname; ?>/masterview/assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <script src="/<?php echo $appname; ?>/masterview/assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
    <script src="/<?php echo $appname; ?>/masterview/assets/vendor/slimscroll/jquery.slimscroll.js"></script>
    <script src="/<?php echo $appname; ?>/masterview/assets/libs/js/main-js.js"></script>
</head>

<body style="background-color: <?php echo $ci->ModelParam->getParameter('background_color'); ?>;">
    <!-- ============================================================== -->
    <!-- main wrapper -->
    <!-- ============================================================== -->
    <div class="dashboard-main-wrapper">
        <!-- ============================================================== -->
        <!-- navbar -->
        <!-- ============================================================== -->
       <div class="dashboard-header">
            <nav class="navbar navbar-expand-lg bg-white fixed-top">
                <a class="navbar-brand" href="#"><?php echo $mainappname; ?></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto navbar-right-top">
                        <li class="nav-item dropdown nav-user">
                            <a class="nav-link nav-user-img" href="#" id="navbarDropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="/<?php echo $appname; ?>/files/images/male.png" alt="" class="user-avatar-md rounded-circle"></a>
                            <?php  echo $menuright; ?>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- ============================================================== -->
        <!-- end navbar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- left sidebar -->
        <!-- ============================================================== -->
        <div class="nav-left-sidebar sidebar-dark">
            <div class="menu-list">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="d-xl-none d-lg-none" href="#">Dashboard</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <?php echo $menuleft; ?>
                </nav>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end left sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            <?php echo $menucontent; ?>
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <div class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            Copyright © 2018 Concept by Colorlib modified by rdy-byteworks. All rights reserved
                        </div>
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">    
                            <?php 
                                echo $ci->ModelParam->getParameter('rdyfw_version');
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- end main wrapper -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- end main wrapper -->
</body>
 
</html>
