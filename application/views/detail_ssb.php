<div class="container-fluid dashboard-content">
    <div class="row">
        <div class="col-xl-12">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            
            <div class="row mb-3 ml-2">
                <a href="<?= site_url('Ssb/'); ?>" class="btn btn-xs btn-rounded btn-danger"><i class="fas fa-arrow-left"></i> Kembali</a>
            </div>
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header" id="top">
                        <h2 class="pageheader-title">Detail Data <?= $nama_pemohon; ?></h2>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- basic form  -->
            <!-- ============================================================== -->

            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-6">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="card">
                                <h5 class="card-header">Pemohon</h5>
                                <div class="card-body">
                                    <form>
                                        <div class="form-group">
                                            <label class="col-form-label">Nama</label>
                                            <input value="<?= $nama_pemohon; ?>" type="text" class="form-control" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-form-label">Jenis Kelamin</label>
                                            <input value="<?= $jenis_kelamin; ?>" type="text" class="form-control" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-form-label">NIK</label>
                                            <input value="<?= $nik; ?>" type="text" class="form-control" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-form-label">NPWP</label>
                                            <input value="<?= $npwp_pemohon; ?>" type="text" class="form-control" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-form-label">Pekerjaan</label>
                                            <input value="<?= $pekerjaan; ?>" type="text" class="form-control" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-form-label">Penghasilan</label>
                                            <input value="<?= number_format($penghasilan,0); ?>" type="text" class="form-control" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-form-label">Pasangan</label>
                                            <input value="<?= $nama_pasangan; ?>" type="text" class="form-control" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-form-label">NIK Pasangan</label>
                                            <input value="<?= $nik_pasangan; ?>" type="text" class="form-control" disabled>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="card">
                                <h5 class="card-header">Data Kredit</h5>
                                <div class="card-body">
                                    <form>
                                        <div class="form-group">
                                            <label class="col-form-label">No. SP3K</label>
                                            <input value="<?= $no_sp3k; ?>" type="text" class="form-control" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-form-label">Harga Rumah</label>
                                            <input value="<?= number_format($harga_rumah,0); ?>" type="text" class="form-control" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-form-label">Uang Muka</label>
                                            <input value="<?= number_format($uang_muka,0); ?>" type="text" class="form-control" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-form-label">SBUM</label>
                                            <input value="<?= $sbum; ?>" type="text" class="form-control" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-form-label">Nilai KPR</label>
                                            <input value="<?= number_format($nilai_kpr,0); ?>" type="text" class="form-control" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-form-label">Suku Bunga</label>
                                            <input value="<?= $suku_bunga; ?>" type="text" class="form-control" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-form-label">Tenor (Bulan)</label>
                                            <input value="<?= $tenor_bulan; ?>" type="text" class="form-control" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-form-label">Angsuran</label>
                                            <input value="<?= number_format($angsuran,0); ?>" type="text" class="form-control" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-form-label">Nilai Subsidi</label>
                                            <input value="<?= number_format($nilai_subsidi,0); ?>" type="text" class="form-control" disabled>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-6">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="card">
                                <h5 class="card-header">Agunan</h5>
                                <div class="card-body">
                                    <form>
                                        <div class="form-group">
                                            <label class="col-form-label">Alamat</label>
                                            <input value="<?= $alamat_agunan; ?>" type="text" class="form-control" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-form-label">Kota</label>
                                            <input value="<?= $kota_agunan; ?>" type="text" class="form-control" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-form-label">Provinsi</label>
                                            <input value="<?= $provinsi; ?>" type="text" class="form-control" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-form-label">Kode Administrasi Wilayah</label>
                                            <input value="<?= $kode_adm_wilayah; ?>" type="text" class="form-control" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-form-label">Kode Pos</label>
                                            <input value="<?= $kode_pos_agunan; ?>" type="text" class="form-control" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-form-label">Luas Tanah</label>
                                            <input value="<?= $luas_tanah; ?>" type="text" class="form-control" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-form-label">Luas Bangunan</label>
                                            <input value="<?= $luas_bangunan; ?>" type="text" class="form-control" disabled>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="card">
                                <h5 class="card-header">Pengembang</h5>
                                <div class="card-body">
                                    <form>
                                        <div class="form-group">
                                            <label class="col-form-label">Nama Pengembang</label>
                                            <input value="<?= $nama_pengembang; ?>" type="text" class="form-control" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-form-label">NPWP Pengembang</label>
                                            <input value="<?= $npwp_pengembang; ?>" type="text" class="form-control" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-form-label">Nama Perumahan</label>
                                            <input value="<?= $nama_perumahan; ?>" type="text" class="form-control" disabled>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="card">
                                <h5 class="card-header">Hasil Uji</h5>
                                <div class="card-body">
                                    <form>
                                        <div class="form-group">
                                            <label class="col-form-label">Tangal Uji</label>
                                            <input value="<?= $tanggaluji; ?>" type="text" class="form-control" disabled>
                                        </div>
                                    </form>
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th scope="col">Jenis Uji</th>
                                                <th scope="col" >Hasil Uji</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Uji Dukcapil</td>
                                                <td>
                                                    <?= $result_dukcapil == "Y" ? 
                                                        "<span class=\"badge badge-success\">$result_dukcapil</span>" : 
                                                        "<span class=\"badge badge-danger\">$result_dukcapil</span>"; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Uji Harga Rumah</td>
                                                <td>
                                                    <?= $result_hargarumah == "Y" ? 
                                                        "<span class=\"badge badge-success\">$result_hargarumah</span>" : 
                                                        "<span class=\"badge badge-danger\">$result_hargarumah</span>"; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Uji Penghasilan</td>
                                                <td>
                                                    <?= $result_penghasilan == "Y" ? 
                                                        "<span class=\"badge badge-success\">$result_penghasilan</span>" : 
                                                        "<span class=\"badge badge-danger\">$result_penghasilan</span>"; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Uji Pengembang</td>
                                                <td>
                                                    <?= $result_pengembang == "Y" ? 
                                                        "<span class=\"badge badge-success\">$result_pengembang</span>" : 
                                                        "<span class=\"badge badge-danger\">$result_pengembang</span>"; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Uji Nilai KPR</td>
                                                <td>
                                                    <?= $result_nilaikpr == "Y" ? 
                                                        "<span class=\"badge badge-success\">$result_nilaikpr</span>" : 
                                                        "<span class=\"badge badge-danger\">$result_nilaikpr</span>"; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Uji Suku Bunga</td>
                                                <td>
                                                    <?= $result_sukubunga == "Y" ? 
                                                        "<span class=\"badge badge-success\">$result_sukubunga</span>" : 
                                                        "<span class=\"badge badge-danger\">$result_sukubunga</span>"; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Uji Subsidi</td>
                                                <td>
                                                    <?= $result_subsidi == "Y" ? 
                                                        "<span class=\"badge badge-success\">$result_subsidi</span>" : 
                                                        "<span class=\"badge badge-danger\">$result_subsidi</span>"; ?>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- ============================================================== -->
            <!-- end basic form  -->
            <!-- ============================================================== -->
        </div>
    </div>
</div>