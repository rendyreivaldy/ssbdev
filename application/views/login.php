<?php 
    $ci = new CI_Model();
    $ci =& get_instance();
    $ci->load->model('ModelParam');
?>

<!doctype html>
<html lang="en">
 
<head>
    <link rel="icon" href="/<?php echo $appname . $ci->ModelParam->getParameter('tab_icon'); ?>">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo $mainappname; ?></title>
    <!-- Bootstrap CSS -->
    
    <link rel="stylesheet" href="/<?php echo $appname; ?>/masterview/assets/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/<?php echo $appname; ?>/masterview/assets/vendor/fonts/circular-std/style.css">
    <link rel="stylesheet" href="/<?php echo $appname; ?>/masterview/assets/libs/css/style.css">
    <link rel="stylesheet" href="/<?php echo $appname; ?>/masterview/assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
    <link rel="stylesheet" href="/<?php echo $appname . '/files/assets/css'; ?>/backgroundcycle.css">
    <style>
        html,
        body {
            height: 100%;
        }

        body {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-align: center;
            align-items: center;
            padding-top: 40px;
            padding-bottom: 40px;
            background-color: <?php echo $logincolor; ?>;
        }
    </style>
</head>

<body style="background-color: <?php echo $ci->ModelParam->getParameter('background_color'); ?>;">
    
    <!-- BACKGROUND CYCLER -->

    <div id="background_cycler" >

        <?php 

            $images = json_decode($images, true);
            $counter = 0;
            foreach ($images as $key) {
                if($counter == 0)
                    echo '<img class="active" src="' .  base_url('files/images') . '/' . $key . '" alt=""/>';
                else
                    echo '<img src="' .  base_url('files/images') . '/' . $key . '" alt="" />';
                $counter++;
            }

        ?>
    </div>

    <!-- BACKGROUND CYLCER END -->

    <!-- ============================================================== -->
    <!-- login page  -->
    <!-- ============================================================== -->
    <div class="splash-container">
        <div class="card ">
            <span class="text-center" style="margin-top: 10px;"><?php echo $ci->ModelParam->getParameter('rdyfw_version'); ?></span>
            <div class="card-header text-center"><a href=""><img class="logo-img" 
                src="/<?php echo $appname . $loginimage; ?>" 
                alt="logo"></a><span class="splash-description"><?php echo $welcometext; ?></span></div>
            <div class="card-body">
                <form method="post" action="<?php echo site_url('Login/actionLogin'); ?>">
                    <div class="form-group">
                        <input class="form-control form-control-lg" name="username" id="username" type="text" placeholder="Username" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <input class="form-control form-control-lg" name="password" id="password" type="password" placeholder="Password" >
                    </div> 
                    <div class="form-group">
                        <input class="form-control form-control-lg" name="passwordhide" id="passwordhide" type="password" style="display : none;">
                    </div>
                    <div class="alert alert-danger" id="loginstatus" style="display : <?php echo $loginmessagedisplay ?>;" role="alert">
                        <?php echo $loginmessage ?>
                    </div>
                    <button type="submit" name="submit" value="login" class="btn btn-primary btn-lg btn-block" onclick="beforeSubmit()">Masuk</button>
                </form>
            </div>
            <?php if($enableregistration == "1"){ ?>
                <div class="card-footer bg-white p-0  ">
                    <div class="card-footer-item card-footer-item-bordered">
                        <a href="#" class="footer-link" data-toggle="modal" data-target="#exampleModal">Registrasi Akun Baru</a>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>

    <!-- MODAL CREATE AN ACCOUNT -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Buat Akun Baru</h5>
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                </div>
                <div class="modal-body">
                    <div class="card-body">
                        <form method="post" action="<?php echo site_url('Login/registration'); ?>">
                            <div class="form-group">
                                <input class="form-control form-control-lg" required="" id="name" name="nama_create" type="text" placeholder="Nama">
                            </div>
                            <div class="form-group">
                                <input class="form-control form-control-lg" required="" id="username" name="username_create" type="text" placeholder="Username" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <input class="form-control form-control-lg" required="" id="password_create" name="password_create" type="password" placeholder="Password" onkeyup="passwordValidation()">
                            </div>
                            <div class="form-group">
                                <input class="form-control form-control-lg" required="" data-parsley-equalto="#password_create" id="password_confirm_create" name="password_confirm_create" type="password" placeholder="Pastikan Password"  onkeyup="passwordValidation()">
                            </div>
                            <div class="alert alert-danger" id="warning" role="alert">
                                Semua Data-Data Tidak Boleh Kosong
                            </div>
                            <button type="submit" name="submit" id="reg_submit_btn" value="registration" class="btn btn-primary btn-lg btn-block">Registrasi</button>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="#" id="reg_btn_close" class="btn btn-secondary" data-dismiss="modal">Tutup</a>
                </div>
            </div>
        </div>
    </div>
    <!-- MODAL CREATE AN ACCOUNT END -->
  
    <!-- ============================================================== -->
    <!-- end login page  -->
    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
    <script src="/<?php echo $appname; ?>/masterview/assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <script src="/<?php echo $appname; ?>/files/assets/js/crypto-js.js"></script>
    <script src="/<?php echo $appname; ?>/masterview/assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>

    <script type="text/javascript">
        
        var warning = document.getElementById("warning");
        var btnReg = document.getElementById("reg_submit_btn");
        var passwordField = document.getElementById("password_create");
        var passwordConfirmField = document.getElementById("password_confirm_create");
        var btnRegClose = document.getElementById("reg_btn_close");
        var username = document.getElementById("username");
        var name = document.getElementById("name");

        // make sure the password is correctly exact. if not, disable the registration button
        
        function passwordValidation(){
            if(passwordField.value != passwordConfirmField.value){
                warning.innerHTML = "Password Tidak Sama";
                warning.className = "alert alert-danger";
                btnReg.disabled = true;
            } else if (passwordField.value == "" && passwordConfirmField.value == ""){
                warning.className = "alert alert-danger";
                warning.innerHTML = "Password dan Konfirmasi Password Belum Diisi";
                btnReg.disabled = false;
            } else {
                warning.className = "alert alert-success";
                warning.innerHTML = "Password Sama";
                btnReg.disabled = false;
            }
        }

        // when the dialg is closed, remove the data from the fields
        btnRegClose.addEventListener("click", function() {
            name.value = "";
            username.value = "";
            passwordField.value = "";
            passwordConfirmField.value = "";
        });


    </script>
    <script type="text/javascript">
        //hide the background while the images load, ready to fade in later
        $('#background_cycler').hide();
        var isCycling = <?php echo $bgCycleFlag; ?>;
        var cycleInterval = <?php echo $bgInterval; ?>;
    </script>

    <script src="/<?php echo $appname . '/files/assets/js'; ?>/backgroundcycle.js"></script>

</body>
 
</html>
