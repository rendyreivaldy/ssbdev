<div class="container-fluid dashboard-content">
    <div class="row">
        <div class="col-xl-10">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header" id="top">
                        <h2 class="pageheader-title">Detail Data Parameter</h2>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- basic form  -->
            <!-- ============================================================== -->

            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="post" action="<?php echo site_url('Parameter/submitEditParam/' . $id); ?>">
                                <div class="alert alert-danger" role="alert" style="display: <?php echo $notificationDisplay; ?>">
                                    <?php echo $notificationMessage; ?>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-form-label">Name</label>
                                    <input id="name" name="name" type="text" class="form-control" value="<?php echo $name; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="value" class="col-form-label">Value</label>
                                    <input id="value" name="value" type="text" class="form-control" value="<?php echo $value; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="keterangan" class="col-form-label">Keterangan</label>
                                    <textarea class="form-control" name="keterangan" id="keterangan" rows="3"  value="<?php echo $keterangan; ?>"><?php echo $keterangan; ?></textarea>
                                </div>
                                <button id="btnsave" type="submit" name="submit" value="login" class="btn btn-primary">Simpan</button>
                                <a href="<?php echo site_url('Parameter/'); ?>" class="btn btn-danger">Kembali</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end basic form  -->
            <!-- ============================================================== -->
        </div>
    </div>
</div>
