<div class="container-fluid dashboard-content">
    <div class="row">
        <div class="col-xl-10">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header" id="top">
                        <h2 class="pageheader-title">Detail Data User</h2>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- basic form  -->
            <!-- ============================================================== -->

            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="post" action="<?php echo site_url('User/submitEditUser/' . $id); ?>">
                                <div class="alert alert-danger" role="alert" style="display: <?php echo $notificationDisplay; ?>">
                                    <?php echo $notificationMessage; ?>
                                </div>
                                <div class="form-group">
                                    <label for="nama" class="col-form-label">Nama</label>
                                    <input id="nama" name="nama" type="text" class="form-control" value="<?php echo $nama; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="username" class="col-form-label">Username</label>
                                    <input id="username" name="username" type="text" class="form-control" value="<?php echo $username; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="input-select">Role</label>
                                    <select class="form-control" id="role" name="role">
                                        <?php 
                                            foreach ($roleddl as $key) {
                                                if($key['id'] == $role){
                                                    echo '<option value="' . $key['id'] . '" selected>' . $key['role'] . '</option>';
                                                } else {
                                                    echo '<option value="' . $key['id'] . '">' . $key['role'] . '</option>';
                                                }
                                            } 
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" id="isblocked" name="isblocked" class="custom-control-input" <?php echo $isblocked ? 'checked=""' : ''; ?>><span class="custom-control-label">Block User ?</span>
                                    </label>
                                </div>
                                <button id="btnsave" type="submit" name="submit" value="login" class="btn btn-primary">Simpan</button>
                                <a href="<?php echo site_url('User/changePassword/' . $id); ?>" class="btn btn-warning">Ubah Password</a>
                                <a href="<?php echo site_url('User/'); ?>" class="btn btn-danger">Kembali</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end basic form  -->
            <!-- ============================================================== -->
        </div>
    </div>
</div>
