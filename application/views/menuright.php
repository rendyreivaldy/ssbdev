<div class="dropdown-menu dropdown-menu-right nav-user-dropdown" aria-labelledby="navbarDropdownMenuLink2">
    <div class="nav-user-info">
        <h5 class="mb-0 text-white nav-user-name">
        <?php echo $rightname['nama']; ?></h5>
        <span class="status"></span><span class="ml-2"><?php echo $rightname['role']; ?></span>
    </div>
    <a class="dropdown-item" href="<?php echo site_url('Login/logout'); ?>"><i class="fas fa-power-off mr-2"></i>Logout</a>
</div>