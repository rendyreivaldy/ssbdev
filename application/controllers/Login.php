<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct()
    {
            parent::__construct();
			$this->load->model('ModelUser');
			$this->load->model('ModelParam');
	        $this->load->helper('form');
			if($this->router->method != 'logout' && $this->session->userdata('status') == "login") redirect('/User', 'refresh');
    }

    private function loadPage($parameter){
		$parameter['appname'] = $this->utility->getAppsName();
		$parameter['mainappname'] = $this->utility->getAppsNameParam($this);
		$parameter['loginimage'] = $this->ModelParam->getParameter('login_image');
		$parameter['enableregistration'] = $this->ModelParam->getParameter('enable_registration');
		$parameter['logincolor'] = $this->ModelParam->getParameter('login_color');
		$parameter['welcometext'] = $this->ModelParam->getParameter('welcome_text');
		$parameter['images'] = $this->ModelParam->getParameter('images_background_login');
		$parameter['bgCycleFlag'] = $this->ModelParam->getParameter('background_cycle_flag');
		$parameter['bgInterval'] = $this->ModelParam->getParameter('background_cycle_interval');
		$this->load->view('login', $parameter);
    }

	public function index()
	{
		// setting up parameter
		$parameter['loginmessagedisplay'] = 'none';	// deafultly hide login error message
		$parameter['loginmessage'] = '';

		$this->loadPage($parameter);
	}

	public function actionLogin(){
		$dataArray['username'] = $this->security->xss_clean($this->input->post('username'));
		$dataArray['password'] = $this->security->xss_clean($this->input->post('passwordhide'));
		$dataArray['password'] = $this->utility->CryptoJSAesDecrypt($dataArray['password']);

		//////////////////////////////////////////////////////////////////////////////////////
		///////////////////////////////// UNIT TESTING ///////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////////

		// $this->ModelUser->startUnitTestingLogin($this->unit);

		//////////////////////////////////////////////////////////////////////////////////////
		///////////////////////////////// UNIT TESTING ///////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////////

		$result = $this->ModelUser->checkLogin($dataArray);
		if($result['status']){
			$this->ModelUser->successLogin($this->session);

			/*
				ini kalo misalnya mau pake config dari db yang parameter2 itu landing page defaultnya
				tapi kalo dari DB kenanya ke semua role user. jadi gw buat per role gitu deh
			 */
			// redirect($this->ModelParam->getParameter('controller_after_login'), 'refresh');

			redirect($result['landingpage'][0]['landing_page'], 'refresh');
			
		} else {
			$parameter['loginmessagedisplay'] = 'block';	// show error message
			$parameter['loginmessage'] = $result['message'];
		}
		$this->loadPage($parameter);
	}

	public function registration(){
		if ($parameter['enableregistration'] == "1"){
			$dataArray['username'] = $this->security->xss_clean($this->input->post('username_create'));
			$dataArray['password'] = $this->security->xss_clean($this->input->post('password_create'));
			$dataArray['passwordconfirm'] = $this->security->xss_clean($this->input->post('password_confirm_create'));
			$dataArray['nama'] = $this->security->xss_clean($this->input->post('nama_create'));
			$this->ModelUser->registration($dataArray);
			$parameter['loginmessagedisplay'] = 'block';	// show error message
			$parameter['loginmessage'] = 'User berhasil terdaftar';
		}
		$this->loadPage($parameter);
	}

	public function logout(){
		$this->ModelUser->logOut($this->session);
		redirect('Login/', 'refresh');
	}
}
