<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Log extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
		$this->load->model('ModelUser');
		$this->load->model('ModelParam');
		$this->load->model('ModelLog');
		$this->utility->everyControllerConstruct($this);
    }

	public function index()
	{
		// LOGGING DATA
		$this->loggings->logDatabase($this->session->userdata('id'), 'View', 'Log Index page', $this);

		$subparameter['dataLogAll'] = $this->ModelLog->getLogData();
		$subparameter['rightname'] = $this->utility->getNameAndRole($this);
		$subparameter['leftmenu'] = $this->utility->getMenuByRole($this);
		$subparameter['appname'] = $this->utility->getAppsName();

		$parameter['appname'] = $subparameter['appname'];
		$parameter['mainappname'] = $this->utility->getAppsNameParam($this);
		$parameter['menuleft'] = $this->load->view('menu', $subparameter, TRUE);
		$parameter['menuright'] = $this->load->view('menuright', $subparameter, TRUE);
		$parameter['menucontent'] = $this->load->view('content_log', $subparameter, TRUE);
		$this->load->view('mainmenu', $parameter);
	}
}
