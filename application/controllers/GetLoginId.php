<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class GetLoginId extends REST_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('ModelApi');
    }

    public function index_post()
    {
        $parameter = $this->ModelApi->getParameterRawBody(); 
        $arrayToCheck = array(
            "kodebank", "pin"
        );
        $resultCekParameter = $this->ModelApi->cekParameter($parameter, $arrayToCheck);

        if($resultCekParameter['responseCode'] != "00"){
            $message = $this->ModelApi->getResponseMessage('08');
            $message['response_message'] .= " (" . $resultCekParameter['responseMessage'] . ")";
        } else {
            $kodeBank = $parameter['kodebank'];
            $pin = $parameter['pin'];

            $result = $this->ModelApi->cekLogin($kodeBank, $pin);
            if($result['response_code'] == '00'){
                $loginId = $result['loginid'];
                $message = [
                    'kodebank' => $kodeBank,
                    'loginid' => $loginId,
                    'responsecode' => $result['response_code']
                ];
            } else {
                $message = [
                    'responsecode' => $result['response_code'],
                    'responsemessage' => $result['response_message']
                ];
            }
        }
        $message = $this->ModelApi->changeResponseMessageFormat($message);
        $kodeBank = isset($parameter['kodebank']) ? $parameter['kodebank'] : '';

        // LOGGING
        $logData = array(
            "parameter" => $parameter,
            "response" => $message
        );
        $this->loggings->logDatabase($kodeBank, 'API Get Login ID', json_encode($logData), $this);
        $this->set_response($message, REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code
    }
}   

