<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Parameter extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
		$this->load->model('ModelUser');
		$this->load->model('ModelParam');
		$this->utility->everyControllerConstruct($this);
    }

	public function index()
	{
		// LOGGING DATA
		$this->loggings->logDatabase($this->session->userdata('id'), 'View', 'Parameter Index page', $this);

		$subparameter['dataParamAll'] = $this->ModelParam->getParamData();
		$subparameter['rightname'] = $this->utility->getNameAndRole($this);
		$subparameter['leftmenu'] = $this->utility->getMenuByRole($this);
		$subparameter['appname'] = $this->utility->getAppsName();

		$parameter['appname'] = $subparameter['appname'];
		$parameter['mainappname'] = $this->utility->getAppsNameParam($this);
		$parameter['menuleft'] = $this->load->view('menu', $subparameter, TRUE);
		$parameter['menuright'] = $this->load->view('menuright', $subparameter, TRUE);
		$parameter['menucontent'] = $this->load->view('content_param', $subparameter, TRUE);
		$this->load->view('mainmenu', $parameter);
	}

	public function createParam(){
		// LOGGING DATA
		$this->loggings->logDatabase($this->session->userdata('id'), 'View', 'Parameter Create page', $this);

		$subparameter = array();
		$subparameter['notificationDisplay'] = "none";
		$subparameter['notificationMessage'] = "";
		$subparameter['id'] = "";
		$subparameter['name'] = "";
		$subparameter['value'] = "";
		$subparameter['keterangan'] = "";
		$subparameter['rightname'] = $this->utility->getNameAndRole($this);
		$subparameter['leftmenu'] = $this->utility->getMenuByRole($this);

		$parameter['appname'] = $this->utility->getAppsName();
		$parameter['mainappname'] = $this->utility->getAppsNameParam($this);
		$parameter['menuleft'] = $this->load->view('menu', $subparameter, TRUE);
		$parameter['menuright'] = $this->load->view('menuright', $subparameter, TRUE);
		$parameter['menucontent'] = $this->load->view('create_param', $subparameter, TRUE);

		$this->load->view('mainmenu', $parameter);
	}

	public function editParam($id){
		// LOGGING DATA
		$this->loggings->logDatabase($this->session->userdata('id'), 'View', 'Parameter Edit page', $this);

		$dataGet = $this->ModelParam->getDataByIdForEdit($id);

		$subparameter = array();
		$subparameter['notificationDisplay'] = "none";
		$subparameter['notificationMessage'] = "";
		$subparameter['id'] = $dataGet[0]['id'];
		$subparameter['name'] = $dataGet[0]['name'];
		$subparameter['value'] = $dataGet[0]['value'];
		$subparameter['keterangan'] = $dataGet[0]['keterangan'];
		$subparameter['rightname'] = $this->utility->getNameAndRole($this);
		$subparameter['leftmenu'] = $this->utility->getMenuByRole($this);

		$parameter['appname'] = $this->utility->getAppsName();
		$parameter['mainappname'] = $this->utility->getAppsNameParam($this);
		$parameter['menuleft'] = $this->load->view('menu', $subparameter, TRUE);
		$parameter['menuright'] = $this->load->view('menuright', $subparameter, TRUE);
		$parameter['menucontent'] = $this->load->view('edit_param', $subparameter, TRUE);

		$this->load->view('mainmenu', $parameter);
	}

	public function submitCreateParam(){
		$subparameter = array();

		$subparameter['name'] = $this->security->xss_clean($this->input->post('name'));
		$subparameter['value'] = $this->security->xss_clean($this->input->post('value'));
		$subparameter['keterangan'] = $this->security->xss_clean($this->input->post('keterangan'));
		$subparameter['notificationDisplay'] = "none";
		$subparameter['notificationMessage'] = "";

		if($subparameter['name'] == ''){
			$subparameter['notificationDisplay'] = "block";
			$subparameter['notificationMessage'] = "Nama Tidak Boleh Kosong";
		} else if ($subparameter['value'] == '') {
			$subparameter['notificationDisplay'] = "block";
			$subparameter['notificationMessage'] = "Value Tidak Boleh Kosong";
		} else {

			// LOGGING DATA
			$this->loggings->logDatabase($this->session->userdata('id'), 'Submit', 'Submit create parameter success : ' . json_encode($subparameter), $this);

			$this->ModelParam->createParam($subparameter);
			redirect('Parameter/', 'refresh');
		}

		// LOGGING DATA
		$this->loggings->logDatabase($this->session->userdata('id'), 'Submit', 'Submit create parameter error : ' . json_encode($subparameter), $this);

		$subparameter['rightname'] = $this->utility->getNameAndRole($this);
		$subparameter['leftmenu'] = $this->utility->getMenuByRole($this);

		$parameter['appname'] = $this->utility->getAppsName();
		$parameter['mainappname'] = $this->utility->getAppsNameParam($this);
		$parameter['menuleft'] = $this->load->view('menu', $subparameter, TRUE);
		$parameter['menuright'] = $this->load->view('menuright', $subparameter, TRUE);
		$parameter['menucontent'] = $this->load->view('create_param', $subparameter, TRUE);

		$this->load->view('mainmenu', $parameter);
	}

	public function submitEditParam($id){

		$subparameter = array();

		$subparameter['name'] = $this->security->xss_clean($this->input->post('name'));
		$subparameter['value'] = $this->security->xss_clean($this->input->post('value'));
		$subparameter['keterangan'] = $this->security->xss_clean($this->input->post('keterangan'));
		$subparameter['id'] = $id;
		$subparameter['notificationDisplay'] = "none";
		$subparameter['notificationMessage'] = "";

		if($subparameter['name'] == ''){
			$subparameter['notificationDisplay'] = "block";
			$subparameter['notificationMessage'] = "Nama Tidak Boleh Kosong";
		} else if ($subparameter['value'] == '') {
			$subparameter['notificationDisplay'] = "block";
			$subparameter['notificationMessage'] = "Value Tidak Boleh Kosong";
		} else {

			// LOGGING DATA
			$this->loggings->logDatabase($this->session->userdata('id'), 'Submit', 'Submit edit parameter success : ' . json_encode($subparameter), $this);

			$this->ModelParam->updateDataParam($subparameter);
			redirect('Parameter/', 'refresh');
		}

		// LOGGING DATA
		$this->loggings->logDatabase($this->session->userdata('id'), 'Submit', 'Submit edit parameter error : ' . json_encode($subparameter), $this);

		$subparameter['rightname'] = $this->utility->getNameAndRole($this);
		$subparameter['leftmenu'] = $this->utility->getMenuByRole($this);

		$parameter['appname'] = $this->utility->getAppsName();
		$parameter['mainappname'] = $this->utility->getAppsNameParam($this);
		$parameter['menuleft'] = $this->load->view('menu', $subparameter, TRUE);
		$parameter['menuright'] = $this->load->view('menuright', $subparameter, TRUE);
		$parameter['menucontent'] = $this->load->view('edit_param', $subparameter, TRUE);

		$this->load->view('mainmenu', $parameter);
	}

	public function deleteParam($id){
		// LOGGING DATA
		$deletedData = $this->ModelParam->getDataByIdForEdit($id);
		$this->loggings->logDatabase($this->session->userdata('id'), 'Delete', 'Deleted parameter  : ' . json_encode($deletedData), $this);

		$this->ModelParam->deleteParam($id);
		redirect('Parameter/', 'refresh');
	}
}
