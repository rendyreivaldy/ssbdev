<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ssb extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
		$this->load->model('ModelUser');
		$this->load->model('ModelParam');
		$this->load->model('ModelSsb');
		$this->utility->everyControllerConstruct($this);
    }

	public function index()
	{
		// LOGGING DATA
		$this->loggings->logDatabase($this->session->userdata('id'), 'View', 'Ssb Index page', $this);

		$subparameter['rightname'] = $this->utility->getNameAndRole($this);
		$subparameter['leftmenu'] = $this->utility->getMenuByRole($this);
		$subparameter['appname'] = $this->utility->getAppsName();

		$parameter['appname'] = $subparameter['appname'];
		$parameter['mainappname'] = $this->utility->getAppsNameParam($this);
		$parameter['menuleft'] = $this->load->view('menu', $subparameter, TRUE);
		$parameter['menuright'] = $this->load->view('menuright', $subparameter, TRUE);
		$parameter['menucontent'] = $this->load->view('content_ssb', $subparameter, TRUE);
		$this->load->view('mainmenu', $parameter);
	}

	public function detail($idUji){
		// LOGGING DATA
		$this->loggings->logDatabase($this->session->userdata('id'), 'View', 'Ssb Detail page ' . $idUji, $this);

		$dataUji = $this->ModelSsb->getDataByIdForEdit($idUji, 'data_uji');
		$resultUji = $this->ModelSsb->getDataByIdForEdit($idUji, 'resultuji');

		$subparameter = array();
		$subparameter['notificationDisplay'] = "none";
		$subparameter['notificationMessage'] = "";
		$subparameter['idUji'] = $idUji;
		$subparameter['nama_pemohon'] = $dataUji[0]['nama_pemohon'];
		$subparameter['pekerjaan'] = $dataUji[0]['pekerjaan'];
		$subparameter['jenis_kelamin'] = $dataUji[0]['jenis_kelamin'];
		$subparameter['nik'] = $dataUji[0]['nik'];
		$subparameter['npwp_pemohon'] = $dataUji[0]['npwp_pemohon'];
		$subparameter['penghasilan'] = $dataUji[0]['penghasilan'];

		$subparameter['nama_pasangan'] = $dataUji[0]['nama_pasangan'];
		$subparameter['nik_pasangan'] = $dataUji[0]['nik_pasangan'];
		$subparameter['no_sp3k'] = $dataUji[0]['no_sp3k'];
		$subparameter['harga_rumah'] = $dataUji[0]['harga_rumah'];
		$subparameter['uang_muka'] = $dataUji[0]['uang_muka'];

		$subparameter['sbum'] = $dataUji[0]['sbum'];
		$subparameter['nilai_kpr'] = $dataUji[0]['nilai_kpr'];
		$subparameter['suku_bunga'] = $dataUji[0]['suku_bunga'];
		$subparameter['tenor_bulan'] = $dataUji[0]['tenor_bulan'];
		$subparameter['angsuran'] = $dataUji[0]['angsuran'];

		$subparameter['nilai_subsidi'] = $dataUji[0]['nilai_subsidi'];
		$subparameter['nama_pengembang'] = $dataUji[0]['nama_pengembang'];
		$subparameter['npwp_pengembang'] = $dataUji[0]['npwp_pengembang'];
		$subparameter['nama_perumahan'] = $dataUji[0]['nama_perumahan'];
		$subparameter['alamat_agunan'] = $dataUji[0]['alamat_agunan'];

		$subparameter['kota_agunan'] = $dataUji[0]['kota_agunan'];
		$subparameter['provinsi'] = $dataUji[0]['provinsi'];
		$subparameter['kode_adm_wilayah'] = $dataUji[0]['kode_adm_wilayah'];
		$subparameter['kode_pos_agunan'] = $dataUji[0]['kode_pos_agunan'];
		$subparameter['luas_tanah'] = $dataUji[0]['luas_tanah'];

		$subparameter['luas_bangunan'] = $dataUji[0]['luas_bangunan'];
		$subparameter['kodebank'] = $dataUji[0]['kodebank'];
		$subparameter['created_at'] = $dataUji[0]['created_at'];

		$subparameter['result_dukcapil'] = $resultUji[0]['result_dukcapil'];
		$subparameter['result_hargarumah'] = $resultUji[0]['result_hargarumah'];
		$subparameter['result_penghasilan'] = $resultUji[0]['result_penghasilan'];
		$subparameter['result_pengembang'] = $resultUji[0]['result_pengembang'];
		$subparameter['result_nilaikpr'] = $resultUji[0]['result_nilaikpr'];

		$subparameter['result_sukubunga'] = $resultUji[0]['result_sukubunga'];
		$subparameter['result_subsidi'] = $resultUji[0]['result_subsidi'];
		$subparameter['tanggaluji'] = $resultUji[0]['tanggaluji'];

		$subparameter['roleddl'] = $this->ModelSsb->getRoleForDropDown();
		$subparameter['idparentddl'] = $this->ModelSsb->getIdParentForDropDown();
		$subparameter['rightname'] = $this->utility->getNameAndRole($this);
		$subparameter['leftmenu'] = $this->utility->getMenuByRole($this);

		$parameter['appname'] = $this->utility->getAppsName();
		$parameter['mainappname'] = $this->utility->getAppsNameParam($this);
		$parameter['menuleft'] = $this->load->view('menu', $subparameter, TRUE);
		$parameter['menuright'] = $this->load->view('menuright', $subparameter, TRUE);
		$parameter['menucontent'] = $this->load->view('detail_ssb', $subparameter, TRUE);

		$this->load->view('mainmenu', $parameter);
	}
}
