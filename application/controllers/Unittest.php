<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
	test : 
	V + login
	V - login wrong pin
	V - login wrong kode bank

	V + uji Dini
	V - uji dini wrong kodebank
	V - uji dini wrong loginid
	- uji dini harga rumah diatas 150.000.000
	- uji dini suku bunga diatas 0.05
	- uji dini penghasilan diatas 8.000.000
	- uji dini nilai kpr > harga rumah - 1% hargarumah - sbum
	- uji dini udah ada di subsidi
	- uji dini npwp pengembang tidak terdaftar
	- uji dini dukcapil pemohon
	- uji dini dukcapil pasangan
	- uji dini kode wilayah tidak terdaftar di sistem

	manual test API cek subsidi, FLPP, dan SIRENG
	1. http://ssb.ppdpp.id/Api/getdebitur_flpp/1271060901940001
	2. http://ssb.ppdpp.id/Api/getdata_pupr/6171023010780006
	3. http://ssb.ppdpp.id/Api/getdata_pengembang/840699714304000


	review mas ridwan
		- cek dukcapil pemohon sudah menikah tapi pasangan kosong
		- cek dukcapil pemohon belum menikah nama pasangan kosong di bolehin
		- cek dukcapil nama pasangan sebagai nama pemohon, suami istri gaboleh ambil 2 kali
		- cek dukcapil beda jenis kelamin antara yang di passing sama data dukcapil
		- cek npwp sudah terdaftar gabisa
		- cek dukcapil no KK suami sama istri harus sama

		- buat parameter ambil tanggal uji dari data yang dipassing, atau ambil data created date
*/

class Unittest extends CI_Controller {

	public $loginId;
	public $parameterDataALot;
	public $dukcapilTargetHitApi ;

    public function __construct()
    {
        parent::__construct();
		$this->load->model('ModelUser');
		$this->load->model('ModelParam');
		$this->load->model('ModelUnitTest');
		$this->load->model('ModelApi');
		$this->load->model('ModelUjiDini');
		$this->load->library("unit_test");
		$this->utility->everyControllerConstruct($this);

		$this->parameterDataALot = array(
			"nama_pemohon" => "KADEK WIDIADA",
			"pekerjaan" => "wirausaha",
			"jenis_kelamin" => "L",
			"nik" => "5108060507800005",
			"npwp_pemohon" => "920607538902000",
			"penghasilan" => "6500000",
			"nama_pasangan" => "KETUT WIDIANI",
			"nik_pasangan" => "5108064505780008",
			"no_sp3k" => "1234567890",
			"harga_rumah" => "124000000",
			"uang_muka" => "1240000",
			"sbum" => "0",
			"nilai_kpr" => "122760000",
			"suku_bunga" => "0.05",
			"tenor_bulan" => "240",
			"angsuran" => "980000",
			"nilai_subsidi" => "122760000",
			"nama_pengembang" => "PT. KHAZANAH BABEL LESTARI",
			"npwp_pengembang" => "840699714304000",
			"nama_perumahan" => "perumahan contoh subsidi",
			"alamat_agunan" => "jalan zamrud no 1",
			"kota_agunan" => "bekasi",
			"provinsi" => "jawa barat",
			"kode_adm_wilayah" => "327510",
			"kode_pos_agunan" => "17421",
			"luas_tanah" => "100",
			"luas_bangunan" => "36",
			"kodebank"  => "200",
			"loginid"  => $this->loginId,
			"pin" => "123"
		);

		$this->dukcapilTargetHitApi = $this->ModelParam->getParameter('dukcapil_direct_hit');
    }

	public function index()
	{
		// NO TIME LIMIT BEIBEHHH !!!!
		set_time_limit(0);

		$subparameter = array();
		$subparameter['rightname'] = $this->utility->getNameAndRole($this);
		$subparameter['leftmenu'] = $this->utility->getMenuByRole($this);

		$startTime = microtime(true);
		$this->runAllTests();
		$time_elapsed_secs = microtime(true) - $startTime;

		$subparameter['data'] = $this->unit->report();

		$subparameter['errorCount'] = substr_count($subparameter['data'],"<span style=\"color: #C00;\">Failed</span>");
		$subparameter['UnitTestCount'] = substr_count($subparameter['data'],"<th style=\"text-align: left; border-bottom:1px solid #CCC;\">Test Name</th>");
		$subparameter['title'] = $subparameter['errorCount'] > 0 ? "Unit Test (".$subparameter['errorCount']." Error Found)" : "Unit Test (".$subparameter['UnitTestCount']." Testen sind gut !)";
		$subparameter['title'] .= " Done in " . number_format($time_elapsed_secs, 2, '.', ''). " s";

		$parameter['appname'] = $this->utility->getAppsName();
		$parameter['mainappname'] = $this->utility->getAppsNameParam($this);
		$parameter['menuleft'] = $this->load->view('menu', $subparameter, TRUE);
		$parameter['menuright'] = $this->load->view('menuright', $subparameter, TRUE);
		$parameter['menucontent'] = $this->load->view('content_unittest', $subparameter, TRUE);

		$this->load->view('mainmenu', $parameter);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////

	public function runAllTests(){

		//////////// Functions - Unit Tests ///////////

		// $this->unitTestJenisKelaminCheck();
		// $this->unitTestGetJenisKelaminPasangan();
		// $this->unitTestCekParameter();
		// $this->unitTestCekLogin();
		// $this->unitTestCekLoginId();
		// $this->unitTestGetResponseMessage();
		// $this->unitTestCekUjiDiniParameter();
		// $this->unitTestCekLoginIdApi();
		// $this->unitTestChangeResponseMessageFormat();
		// $this->unitTestExecuteInsertUji();
		// $this->unitTestExecuteCekKetentuan();
		// $this->unitTestExecuteCekDukcapil();
		// $this->unitTestExecuteCekSubsidiHitAPI();
		// $this->unitTestExecuteCekPengembangHitAPI();
		// $this->unitTestGetFinalResponseCode();
		// $this->unitTestGetFinalResult();
		// $this->unitTestUjiDiniStartFlow();

		/////////////////// APIs /////////////////////
		
		$this->unitTestGetLoginIdAPI();
		$this->unitTestUjiDiniAPIHitCountMax();
		$this->unitTestUjiDiniAPI();
		$this->unitTestUjiDiniAPINIKSudahDiujiPadaBankYangSamaDenganStatusLolos();
		$this->unitTestUjiDiniAPIDeleteDataLebihDari3Bulan();
		$this->unitTestBatalUjiAPI();

		////////////////// NEGATIVE //////////////////

		$this->unitTestGetLoginIdAPIWrongKodebank();
		$this->unitTestGetLoginIdAPIWrongPin();
		$this->unitTestUjiDiniAPIWrongKodeBank();
		$this->unitTestUjiDiniAPIWrongLoginId();
		$this->unitTestUjiDiniAPIHargaRumahDiatasSeratusLimaPuluhJuta();
		$this->unitTestUjiDiniAPISukuBungaDiatasNolKomaLima();
		$this->unitTestUjiDiniAPIPenghasilanDiatasDelapanJuta();
		$this->unitTestUjiDiniAPINilaiKPRDiatasStandard();
		$this->unitTestUjiDiniAPISudahSubsidiHitAPISSB();				// inih belon belom nemu datanya
		$this->unitTestUjiDiniAPINPWPPengembangTidakTerdaftar();
		$this->unitTestUjiDiniAPIWrongKTPPemohon();
		$this->unitTestUjiDiniAPIWrongLKTPPasangan();
		$this->unitTestUjiDiniAPIKodeWilayahTidakTerdaftar();

		$this->unitTestUjiDiniAPIPemohonSudahMenikahNIKPasanganKosong();
		$this->unitTestUjiDiniAPIPemohonBelumMenikahNIKPasanganKosong();
		$this->unitTestUjiDiniAPISuamiIstriTidakBolehDuaKali();
		$this->unitTestUjiDiniAPICekDukcapilJenisKelaminBeda();
		$this->unitTestUjiDiniAPICekNPWPExist();
		$this->unitTestUjiDiniAPICekDukcapilNoKKSuamiIstriHarusSama();
		$this->unitTestUjiDiniAPITanggalUjiManual();
		$this->unitTestBatalUjiAPIDataNIKTidakAda();

		$this->cleanUpTableForTesting($this->parameterDataALot['nik']);

	}

	//////////////////////////////////////////////////////////
	///////////////// GETLOGINID /////////////////////////////
	//////////////////////////////////////////////////////////

	public function cleanUpTableForTesting($nik){
		$kodeBank = $this->parameterDataALot['kodebank'];
		$nik = $nik;
		$this->db->query("DELETE FROM data_uji WHERE kodebank = '$kodeBank' AND nik = '$nik'", FALSE);
		$this->db->query("DELETE FROM resultuji WHERE kodebank = '$kodeBank' AND nik = '$nik'", FALSE);
		$this->db->query("UPDATE rflogin SET hit = 0 WHERE kodebank = '$kodeBank'", FALSE);
	}

	public function unitTestCekParameter(){

		$arrayToCheck = array(
			"nama_pemohon",
			"pekerjaan",
			"jenis_kelamin",
			"nik",
			"npwp_pemohon",
			"penghasilan",
			"nama_pasangan",
			"nik_pasangan",
			"no_sp3k",
			"harga_rumah",
			"uang_muka",
			"sbum"
		);
		$result = $this->ModelApi->cekParameter($this->parameterDataALot, $arrayToCheck);
		$expectedResult = array(
			"responseCode" => "00"
		);
		$testName = "Function - Cek Parameter";

		$notes = $this->ModelUnitTest->formatNotes($result, $expectedResult);
		$this->unit->run($result['responseCode'], $expectedResult['responseCode'], $testName, $notes);
	}

	public function unitTestJenisKelaminCheck(){
		$dataResultDukcapilJenisKelamin = "Laki-Laki";
		$passedJenisKelamin = "L";
		$result = $this->ModelUjiDini->jenisKelaminCheck($dataResultDukcapilJenisKelamin, $passedJenisKelamin);
		$expectedResult = true;
		$testName = "Function - cek jenis kelamin";

		$notes = $this->ModelUnitTest->formatNotes($result, $expectedResult);
		$this->unit->run($result, $expectedResult, $testName, $notes);
	}

	public function unitTestGetJenisKelaminPasangan(){
		$jenisKelaminPemohon = "L";
		$result = $this->ModelUjiDini->getJenisKelaminPasangan($jenisKelaminPemohon);
		$expectedResult = "P";
		$testName = "Function - get jenis kelamin pasangan";

		$notes = $this->ModelUnitTest->formatNotes($result, $expectedResult);
		$this->unit->run($result, $expectedResult, $testName, $notes);
	}

	public function unitTestCekLogin(){

		$kodeBank = $this->parameterDataALot['kodebank'];
		$pin = $this->parameterDataALot['pin'];
		$result = $this->ModelApi->cekLogin($kodeBank, $pin);
		$expectedResult = array(
			"response_code" => "00"
		);
		$testName = "Function - Cek Login ";

		$this->loginId = $result['loginid'];
		$this->parameterDataALot['loginid'] = $result['loginid'];

		$notes = $this->ModelUnitTest->formatNotes($result, $expectedResult);
		$this->unit->run($result['response_code'], $expectedResult['response_code'], $testName, $notes);
	}

	public function unitTestCekLoginId(){

		$kodeBank = $this->parameterDataALot['kodebank'];
		$result = $this->ModelApi->cekLoginId($kodeBank, $this->loginId);
		$expectedResult = array(
			"response_code" => "00"
		);
		$testName = "Function - Cek Login ID";

		$notes = $this->ModelUnitTest->formatNotes($result, $expectedResult);
		$this->unit->run($result, $expectedResult, $testName, $notes);
	}

	public function unitTestGetResponseMessage(){

		$responseCode = '00';
		$result = $this->ModelApi->getResponseMessage($responseCode);
		$expectedResult = array(
			"response_code" => "00",
			"response_message" => "Sukses"
		);
		$testName = "Function - Login Get Response Code";

		$notes = $this->ModelUnitTest->formatNotes($result, $expectedResult);
		$this->unit->run($result, $expectedResult, $testName, $notes);
	}

	public function unitTestGetLoginIdAPI(){

		// delete the existing data after function testing
		$kodeBank = $this->parameterDataALot['kodebank'];
		$nik = $this->parameterDataALot['nik'];
		$this->db->query("DELETE FROM data_uji WHERE kodebank = '$kodeBank' AND nik = '$nik'", FALSE);
		$this->db->query("DELETE FROM resultuji WHERE kodebank = '$kodeBank' AND nik = '$nik'", FALSE);

		$jsonData = json_encode(
			array(
				"kodebank" => $this->parameterDataALot['kodebank'],
				"pin" => $this->parameterDataALot['pin']
			)
		);
		$url = "http://localhost/ssbdev/GetLoginId";
		$result = $this->utility->postBodyRawCurl($jsonData, $url);
		$result = json_decode($result, true);
		$this->loginId = $result['loginid'];
		$this->parameterDataALot['loginid'] = $result['loginid'];

		$expectedResult = array(
			"response_code" => "00"
		);
		$testName = "API - GetLoginId";

		$notes = $this->ModelUnitTest->formatNotes($result, $expectedResult);
		$this->unit->run($result["responsecode"], $expectedResult["response_code"], $testName, $notes);
	}

	//////////////////////////////////////////////////////////
	//////////////////// UJIDINI /////////////////////////////
	//////////////////////////////////////////////////////////

	public function unitTestCekUjiDiniParameter(){
		

		$parameter = $this->parameterDataALot;

		$result = $this->ModelUjiDini->cekParameter($parameter);
		$expectedResult = true;
		$testName = "Function - Uji Dini Cek Parameter";

		$notes = $this->ModelUnitTest->formatNotes($result, $expectedResult);
		$this->unit->run($result, $expectedResult, $testName, $notes);
	}

	public function unitTestCekLoginIdApi(){
		$parameter = $this->parameterDataALot;

		$result = $this->ModelApi->cekLoginIdApi($parameter);
		$expectedResult = array(
			"responsecode" => "00"
		);
		$testName = "Function - Cek Login ID API";

		$notes = $this->ModelUnitTest->formatNotes($result, $expectedResult);
		$this->unit->run($result["responsecode"], $expectedResult["responsecode"], $testName, $notes);
	}

	public function unitTestChangeResponseMessageFormat(){

		$parameter = array(
			"response_code" => "00",
			"response_message" => "test"
		);

		$result = $this->ModelApi->changeResponseMessageFormat($parameter);
		$expectedResult = array(
			"responsecode" => "00",
			"responsemessage" => "test"
		);
		$testName = "Function - Change format response message no underscore allowed";

		$notes = $this->ModelUnitTest->formatNotes($result, $expectedResult);
		$this->unit->run($result["responsecode"], $expectedResult["responsecode"], $testName, $notes);
	}

	public function unitTestExecuteInsertUji(){
		

		$parameter = $this->parameterDataALot;

		$result = $this->ModelUjiDini->executeInsertUji($parameter);
		$expectedResult = array(
			"response_code" => '00'
		);
		$testName = "Function - Uji Dini Execute Insert Uji";
		$this->parameterDataALot['id_uji'] = $result['id_uji'];
		$notes = $this->ModelUnitTest->formatNotes($result, $expectedResult);
		$this->unit->run($result["response_code"], $expectedResult["response_code"], $testName, $notes);
	}

	public function unitTestExecuteCekKetentuan(){
		

		$parameter = $this->parameterDataALot;

		$result = $this->ModelUjiDini->executeCekKetentuan($parameter);
		$expectedResult = array(
			"response_code" => '00'
		);
		$testName = "Function - Uji Dini Execute Cek Ketentuan";

		$notes = $this->ModelUnitTest->formatNotes($result, $expectedResult);
		$this->unit->run($result["response_code"], $expectedResult["response_code"], $testName, $notes);
	}

	public function unitTestExecuteCekDukcapil(){
		$parameter = $this->parameterDataALot;
		$result = $this->ModelUjiDini->executeCekDukcapil($parameter['nik'], $parameter['nama_pemohon'], $parameter['kodebank'], $this->dukcapilTargetHitApi, $parameter['id_uji'], $parameter['jenis_kelamin']);

		$expectedResult = array(
			"response_code" => '00'
		);
		$testName = "Function - Uji Dini Execute Cek Dukcapil";

		$notes = $this->ModelUnitTest->formatNotes($result, $expectedResult);
		$this->unit->run($result["response_code"], $expectedResult["response_code"], $testName, $notes);
	}

	public function unitTestExecuteCekSubsidi(){
		

		$parameter = $this->parameterDataALot;

		$result = $this->ModelUjiDini->executeCekSubsidi($parameter);
		$expectedResult = array(
			"response_code" => '00'
		);
		$testName = "Function - Uji Dini Execute Cek Subsidi";

		$notes = $this->ModelUnitTest->formatNotes($result, $expectedResult);
		$this->unit->run($result["response_code"], $expectedResult["response_code"], $testName, $notes);
	}

	public function unitTestExecuteCekSubsidiHitAPI(){
		

		$parameter = $this->parameterDataALot;

		$result = $this->ModelUjiDini->executeCekSubsidiHitAPI($parameter);
		$expectedResult = array(
			"response_code" => '00'
		);
		$testName = "Function - Uji Dini Execute Cek Subsidi Hit API SSB";

		$notes = $this->ModelUnitTest->formatNotes($result, $expectedResult);
		$this->unit->run($result["response_code"], $expectedResult["response_code"], $testName, $notes);
	}

	public function unitTestExecuteCekPengembangHitAPI(){
		

		$parameter = $this->parameterDataALot;

		$result = $this->ModelUjiDini->executeCekPengembangHitAPI($parameter);
		$expectedResult = array(
			"response_code" => '00'
		);
		$testName = "Function - Uji Dini Execute Cek Pengembang Hit API SSB";

		$notes = $this->ModelUnitTest->formatNotes($result, $expectedResult);
		$this->unit->run($result["response_code"], $expectedResult["response_code"], $testName, $notes);
	}

	public function unitTestExecuteCekPengembang(){
		

		$parameter = $this->parameterDataALot;

		$result = $this->ModelUjiDini->executeCekPengembang($parameter);
		$expectedResult = array(
			"response_code" => '00'
		);
		$testName = "Function - Uji Dini Execute Cek Pengembang";

		$notes = $this->ModelUnitTest->formatNotes($result, $expectedResult);
		$this->unit->run($result["response_code"], $expectedResult["response_code"], $testName, $notes);
	}

	public function unitTestGetFinalResponseCode(){
		

        $resultSukses = $this->ModelApi->getResponseMessage('00');
        $this->parameterDataALot['responseMessage'] = $resultSukses['response_message'];
        $this->parameterDataALot['responseCode'] = $resultSukses['response_code'];

		$parameter = array(
			"responseCode" => "00",
			"responseMessage" => "sukses"
		);
		
		$result = array(
			"response_code" => "00",
			"response_message" => "sukses"
		);

		$result = $this->ModelUjiDini->getResponseCodeAndMessage($parameter, $result);
		$expectedResult = array(
			"response_code" => '00'
		);
		$testName = "Function - Uji Dini Get Final Response Code";

		$notes = $this->ModelUnitTest->formatNotes($result, $expectedResult);
		$this->unit->run($result["responseCode"], $expectedResult["response_code"], $testName, $notes);
	}

	public function unitTestGetFinalResult(){
		

		$parameter = $this->parameterDataALot;

		$result = $this->ModelUjiDini->getFinalResult($parameter);
		$expectedResult = array(
			"response_code" => '00'
		);
		$testName = "Function - Uji Dini Get Final Result";

		$notes = $this->ModelUnitTest->formatNotes($result, $expectedResult);
		$this->unit->run($result["responsecode"], $expectedResult["response_code"], $testName, $notes);
	}

	public function unitTestUjiDiniStartFlow(){
		

		$parameter = $this->parameterDataALot;

		$result = $this->ModelUjiDini->ujiDiniStartFlow($parameter);
		$expectedResult = array(
			"response_code" => '00'
		);
		$testName = "Function - Uji Dini Start Flow";

		$notes = $this->ModelUnitTest->formatNotes($result, $expectedResult);
		$this->unit->run($result["responsecode"], $expectedResult["response_code"], $testName, $notes);
	}

	public function unitTestUjiDiniAPI(){
		$jsonData = json_encode(
			$this->parameterDataALot
		);
		$url = "http://localhost/ssbdev/UjiDini";
		$result = $this->utility->postBodyRawCurl($jsonData, $url);
		// print_r($result);exit;
		$result = json_decode($result, true);

		$expectedResult = array(
			"response_code" => "00"
		);
		$testName = "API - UjiDini";

		$notes = $this->ModelUnitTest->formatNotes($result, $expectedResult);
		$this->unit->run($result["responsecode"], $expectedResult["response_code"], $testName, $notes);
	}

	public function unitTestBatalUjiAPI(){
		$parameter = array(
			"kodebank" => $this->parameterDataALot['kodebank'],
			"loginid" =>  $this->parameterDataALot['loginid'],
			"nik" =>  $this->parameterDataALot['nik']
		);

		$jsonData = json_encode(
			$parameter
		);
		$url = "http://localhost/ssbdev/BatalUji";
		$result = $this->utility->postBodyRawCurl($jsonData, $url);
		$result = json_decode($result, true);

		$expectedResult = array(
			"response_code" => "00"
		);

		$testName = "API - Batal Uji";

		$notes = $this->ModelUnitTest->formatNotes($result, $expectedResult);
		$this->unit->run($result['responsecode'], $expectedResult["response_code"], $testName, $notes);
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////// NEGATIVE TEST ///////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public function unitTestGetLoginIdAPIWrongKodebank(){
		$jsonData = json_encode(
			array(
				"kodebank" => "202",
				"pin" => $this->parameterDataALot['pin']
			)
		);
		$url = "http://localhost/ssbdev/GetLoginId";
		$result = $this->utility->postBodyRawCurl($jsonData, $url);
		$result = json_decode($result, true);

		$expectedResult = array(
			"response_code" => "01"
		);
		$testName = "API - GetLoginId Wrong Kode Bank";

		$notes = $this->ModelUnitTest->formatNotes($result, $expectedResult);
		$this->unit->run($result["responsecode"], $expectedResult["response_code"], $testName, $notes);
	}

	public function unitTestGetLoginIdAPIWrongPin(){
		$jsonData = json_encode(
			array(
				"kodebank" => $this->parameterDataALot['kodebank'],
				"pin" => "124"
			)
		);
		$url = "http://localhost/ssbdev/GetLoginId";
		$result = $this->utility->postBodyRawCurl($jsonData, $url);
		$result = json_decode($result, true);

		$expectedResult = array(
			"response_code" => "02"
		);
		$testName = "API - GetLoginId Wrong PIN";

		$notes = $this->ModelUnitTest->formatNotes($result, $expectedResult);
		$this->unit->run($result["responsecode"], $expectedResult["response_code"], $testName, $notes);
	}

	public function unitTestUjiDiniAPIWrongKodeBank(){
		$wrongParameter = $this->parameterDataALot;
		$wrongParameter['kodebank'] = "204";

		$jsonData = json_encode(
			$wrongParameter
		);
		$url = "http://localhost/ssbdev/UjiDini";
		$result = $this->utility->postBodyRawCurl($jsonData, $url);
		$result = json_decode($result, true);

		$expectedResult = array(
			"response_code" => "02"
		);
		$testName = "API - UjiDini Wrong Kode Bank";

		$notes = $this->ModelUnitTest->formatNotes($result, $expectedResult);
		$this->unit->run($result["responsecode"], $expectedResult["response_code"], $testName, $notes);
	}

	public function unitTestUjiDiniAPIWrongLoginId(){
		$wrongParameter = $this->parameterDataALot;
		$wrongParameter['loginid'] = "x";

		$jsonData = json_encode(
			$wrongParameter
		);
		$url = "http://localhost/ssbdev/UjiDini";
		$result = $this->utility->postBodyRawCurl($jsonData, $url);
		$result = json_decode($result, true);

		$expectedResult = array(
			"response_code" => "02"
		);
		$testName = "API - UjiDini Wrong Login ID";

		$notes = $this->ModelUnitTest->formatNotes($result, $expectedResult);
		$this->unit->run($result["responsecode"], $expectedResult["response_code"], $testName, $notes);
	}

	public function unitTestUjiDiniAPIHargaRumahDiatasSeratusLimaPuluhJuta(){
		$wrongParameter = $this->parameterDataALot;
		$wrongParameter['harga_rumah'] = "175000000";

		$jsonData = json_encode(
			$wrongParameter
		);
		$url = "http://localhost/ssbdev/UjiDini";
		$result = $this->utility->postBodyRawCurl($jsonData, $url);
		$result = json_decode($result, true);

		$expectedResult = array(
			"response_code" => "04",
			"fieldcek04" => "N"
		);
		$fieldCekError = "N";

		$finalresult = ($result["responsecode"] == $expectedResult["response_code"]) && ($result["responsedetails"][3]["valid"] == $fieldCekError);
		$finalExpectedResult = true;

		$testName = "API - UjiDini Harga Rumah Diatas 150 Juta";

		$notes = $this->ModelUnitTest->formatNotes($result, $expectedResult);
		$this->unit->run($finalresult, $finalExpectedResult, $testName, $notes);
	}

	public function unitTestUjiDiniAPISukuBungaDiatasNolKomaLima(){
		$wrongParameter = $this->parameterDataALot;
		$wrongParameter['suku_bunga'] = "0.06";

		$jsonData = json_encode(
			$wrongParameter
		);
		$url = "http://localhost/ssbdev/UjiDini";
		$result = $this->utility->postBodyRawCurl($jsonData, $url);
		$result = json_decode($result, true);

		$expectedResult = array(
			"response_code" => "04",
			"fieldcek03" => "N"
		);
		$fieldCekError = "N";

		$finalresult = ($result["responsecode"] == $expectedResult["response_code"]) && ($result["responsedetails"][2]["valid"] == $fieldCekError);
		$finalExpectedResult = true;

		$testName = "API - UjiDini Suku Bunga Diatas 0.05";

		$notes = $this->ModelUnitTest->formatNotes($result, $expectedResult);
		$this->unit->run($finalresult, $finalExpectedResult, $testName, $notes);
	}

	public function unitTestUjiDiniAPIPenghasilanDiatasDelapanJuta(){
		$wrongParameter = $this->parameterDataALot;
		$wrongParameter['penghasilan'] = "8100000";

		$jsonData = json_encode(
			$wrongParameter
		);
		$url = "http://localhost/ssbdev/UjiDini";
		$result = $this->utility->postBodyRawCurl($jsonData, $url);
		$result = json_decode($result, true);

		$expectedResult = array(
			"response_code" => "04",
			"fieldcek01" => "N"
		);
		$fieldCekError = "N";

		$finalresult = ($result["responsecode"] == $expectedResult["response_code"]) && ($result["responsedetails"][0]["valid"] == $fieldCekError);
		$finalExpectedResult = true;

		$testName = "API - UjiDini Penghasilan Diatas 8 Juta";

		$notes = $this->ModelUnitTest->formatNotes($result, $expectedResult);
		$this->unit->run($finalresult, $finalExpectedResult, $testName, $notes);
	}

	public function unitTestUjiDiniAPINilaiKPRDiatasStandard(){
		$wrongParameter = $this->parameterDataALot;
		$wrongParameter['nilai_kpr'] = "172760000";

		$jsonData = json_encode(
			$wrongParameter
		);
		$url = "http://localhost/ssbdev/UjiDini";
		$result = $this->utility->postBodyRawCurl($jsonData, $url);
		$result = json_decode($result, true);

		$expectedResult = array(
			"response_code" => "04",
			"fieldcek02" => "N"
		);
		$fieldCekError = "N";

		$finalresult = ($result["responsecode"] == $expectedResult["response_code"]) && ($result["responsedetails"][1]["valid"] == $fieldCekError);
		$finalExpectedResult = true;

		$testName = "API - UjiDini Nilai KPR Diatas Ketentuan";

		$notes = $this->ModelUnitTest->formatNotes($result, $expectedResult);
		$this->unit->run($finalresult, $finalExpectedResult, $testName, $notes);
	}

	public function unitTestUjiDiniAPISudahSubsidi(){		
		$wrongParameter = $this->parameterDataALot;

		// inserting row to subsidi data for make row exist scenario
		$data = array(
			'nik' => $this->parameterDataALot['nik'],
			'is_subsidi' => 1,
			'subsidi_type' => "subsidi",
			'subsidi_date' => date('Y-m-d 00:00:00')
		);
		$this->db->insert('subsidi', $data);

		$jsonData = json_encode(
			$wrongParameter
		);
		$url = "http://localhost/ssbdev/UjiDini";
		$result = $this->utility->postBodyRawCurl($jsonData, $url);
		$result = json_decode($result, true);

		$expectedResult = array(
			"response_code" => "06",
			"fieldcek06" => "N"
		);
		$fieldCekError = "N";

		$finalresult = ($result["responsecode"] == $expectedResult["response_code"]) && ($result["responsedetails"][5]["valid"] == $fieldCekError);
		$finalExpectedResult = true;

		$testName = "API - UjiDini Sudah subsidi";

		$notes = $this->ModelUnitTest->formatNotes($result, $expectedResult);
		$this->unit->run($finalresult, $finalExpectedResult, $testName, $notes);

		// apus lagi datanya yak
		$this->db->delete('subsidi', $data);
	}

	public function unitTestUjiDiniAPISudahSubsidiHitAPISSB(){		
		$wrongParameter = $this->parameterDataALot;

		$wrongParameter['nik'] = "6171023010780006";
		$wrongParameter['nama_pemohon'] = "BENTO ANDREAS MANAFE";

		$jsonData = json_encode(
			$wrongParameter
		);
		$url = "http://localhost/ssbdev/UjiDini";
		$result = $this->utility->postBodyRawCurl($jsonData, $url);
		$result = json_decode($result, true);

		$this->cleanUpTableForTesting($wrongParameter['nik']);

		$expectedResult = array(
			"response_code" => "06",
			"fieldcek06" => "N"
		);
		$fieldCekError = "N";

		$finalresult = ($result["responsecode"] == $expectedResult["response_code"]) && ($result["responsedetails"][5]["valid"] == $fieldCekError);
		$finalExpectedResult = true;

		$testName = "API - UjiDini Sudah subsidi Hit API";

		$notes = $this->ModelUnitTest->formatNotes($result, $expectedResult);
		$this->unit->run($finalresult, $finalExpectedResult, $testName, $notes);
	}

	public function unitTestUjiDiniAPINPWPPengembangTidakTerdaftar(){
		$wrongParameter = $this->parameterDataALot;
		$wrongParameter['npwp_pengembang'] = "123456789098888";

		$jsonData = json_encode(
			$wrongParameter
		);
		$url = "http://localhost/ssbdev/UjiDini";
		$result = $this->utility->postBodyRawCurl($jsonData, $url);
		$result = json_decode($result, true);

		$expectedResult = array(
			"response_code" => "07",
			"fieldcek07" => "N"
		);
		$fieldCekError = "N";

		$finalresult = ($result["responsecode"] == $expectedResult["response_code"]) && ($result["responsedetails"][6]["valid"] == $fieldCekError);
		$finalExpectedResult = true;

		$testName = "API - UjiDini NPWP pengembang tidak terdaftar";

		$notes = $this->ModelUnitTest->formatNotes($result, $expectedResult);
		$this->unit->run($finalresult, $finalExpectedResult, $testName, $notes);
	}

	public function unitTestUjiDiniAPIWrongKTPPemohon(){
		$wrongParameter = $this->parameterDataALot;
		$wrongParameter['nik'] = "3276012509910020";
		$wrongParameter['nama_pemohon'] = "Rendy Reivaldys";

		$jsonData = json_encode(
			$wrongParameter
		);
		$url = "http://localhost/ssbdev/UjiDini";
		$result = $this->utility->postBodyRawCurl($jsonData, $url);
		$result = json_decode($result, true);

		$this->cleanUpTableForTesting($wrongParameter['nik']);

		$expectedResult = array(
			"response_code" => "05",
			"fieldcek05" => "N"
		);
		$fieldCekError = "N";

		$finalresult = ($result["responsecode"] == $expectedResult["response_code"]) && ($result["responsedetails"][4]["valid"] == $fieldCekError);
		$finalExpectedResult = true;

		$testName = "API - UjiDini KTP Pemohon tidak terdaftar";

		$notes = $this->ModelUnitTest->formatNotes($result, $expectedResult);
		$this->unit->run($finalresult, $finalExpectedResult, $testName, $notes);
	}

	public function unitTestUjiDiniAPIWrongLKTPPasangan(){
		$wrongParameter = $this->parameterDataALot;
		$wrongParameter['nik_pasangan'] = "3173055605910050";
		$wrongParameter['nama_pasangan'] = "Fauziyahs";

		$jsonData = json_encode(
			$wrongParameter
		);
		$url = "http://localhost/ssbdev/UjiDini";
		$result = $this->utility->postBodyRawCurl($jsonData, $url);

		$result = json_decode($result, true);

		$expectedResult = array(
			"response_code" => "05",
			"fieldcek05" => "N"
		);
		$fieldCekError = "N";

		$finalresult = ($result["responsecode"] == $expectedResult["response_code"]) && ($result["responsedetails"][4]["valid"] == $fieldCekError);
		$finalExpectedResult = true;

		$testName = "API - UjiDini KTP Pasangan tidak terdaftar";

		$notes = $this->ModelUnitTest->formatNotes($result, $expectedResult);
		$this->unit->run($finalresult, $finalExpectedResult, $testName, $notes);
	}

	public function unitTestUjiDiniAPIKodeWilayahTidakTerdaftar(){
		$wrongParameter = $this->parameterDataALot;
		$wrongParameter['kode_adm_wilayah'] = "123456";

		$jsonData = json_encode(
			$wrongParameter
		);
		$url = "http://localhost/ssbdev/UjiDini";
		$result = $this->utility->postBodyRawCurl($jsonData, $url);
		$result = json_decode($result, true);

		$expectedResult = array(
			"response_code" => "10",
			"fieldcek04" => "N"
		);
		$fieldCekError = "N";

		$finalresult = ($result["responsecode"] == $expectedResult["response_code"]) && ($result["responsedetails"][3]["valid"] == $fieldCekError);
		$finalExpectedResult = true;

		$testName = "API - UjiDini Kode Wilayah tidak terdaftar";

		$notes = $this->ModelUnitTest->formatNotes($result, $expectedResult);
		$this->unit->run($finalresult, $finalExpectedResult, $testName, $notes);
	}

	public function unitTestUjiDiniAPINIKSudahDiujiPadaBankYangSamaDenganStatusLolos(){
		$jsonData = json_encode(
			$this->parameterDataALot
		);
		$url = "http://localhost/ssbdev/UjiDini";
		$result = $this->utility->postBodyRawCurl($jsonData, $url);
		$result = json_decode($result, true);

		$expectedResult = array(
			"response_code" => "13"
		);
		$testName = "API - UjiDini dengan nik yang sudah tersimpan dan lolos di sistem";

		$notes = $this->ModelUnitTest->formatNotes($result, $expectedResult);
		$this->unit->run($result["responsecode"], $expectedResult["response_code"], $testName, $notes);
	}

	public function unitTestUjiDiniAPIHitCountMax(){
		$kodeBank = $this->parameterDataALot['kodebank'];
		$hitData = $this->db->query("SELECT hit, max_hit FROM rflogin WHERE kodebank = '$kodeBank'", FALSE)->result()[0];

		// update to max hit
		$this->db->query("UPDATE rflogin SET hit = max_hit WHERE kodebank = '$kodeBank'", FALSE);

		$jsonData = json_encode(
			$this->parameterDataALot
		);
		$url = "http://localhost/ssbdev/UjiDini";
		$result = $this->utility->postBodyRawCurl($jsonData, $url);
		$result = json_decode($result, true);

		// restore current hit
		$lastCurrentHit = $hitData->hit;
		$this->db->query("UPDATE rflogin SET hit = $lastCurrentHit WHERE kodebank = '$kodeBank'", FALSE);

		$expectedResult = array(
			"response_code" => "12"
		);

		$testName = "API - Ujidini max hit count reached";

		$notes = $this->ModelUnitTest->formatNotes($result, $expectedResult);
		$this->unit->run($result['responsecode'], $expectedResult["response_code"], $testName, $notes);
	}

	public function unitTestUjiDiniAPIDeleteDataLebihDari3Bulan(){
		$kodeBank = $this->parameterDataALot['kodebank'];
		$nik = $this->parameterDataALot['nik'];
		$dateData = $this->db->query("SELECT a.created_at, b.tanggaluji FROM data_uji a LEFT JOIN resultuji b on a.id_uji = b.id_uji WHERE a.kodebank = '$kodeBank' AND a.nik = '$nik'", FALSE)->result()[0];

		// update to more than 3 months ago
		$this->db->query("UPDATE data_uji SET created_at = '2019-12-01 00:00:00' WHERE kodebank = '$kodeBank' AND nik = '$nik'", FALSE);
		$this->db->query("UPDATE resultuji SET tanggaluji = '2019-12-01 00:00:00' WHERE kodebank = '$kodeBank' AND nik = '$nik'", FALSE);


		$jsonData = json_encode(
			array(
				"kodebank" => "200",
				"pin" => "123"
			)
		);
		$url = "http://localhost/ssbdev/GetLoginId";
		$result = $this->utility->postBodyRawCurl($jsonData, $url);
		$result = json_decode($result, true);

		// check if data exist
		$dateDataThen = $this->db->query("SELECT a.created_at, b.tanggaluji FROM data_uji a LEFT JOIN resultuji b on a.id_uji = b.id_uji WHERE a.kodebank = '$kodeBank' AND a.nik = '$nik'", FALSE);

		// return the data
		$jsonData = json_encode(
			$this->parameterDataALot
		);
		$url = "http://localhost/ssbdev/UjiDini";
		$result = $this->utility->postBodyRawCurl($jsonData, $url);

		$result = $dateData == $dateDataThen;
		$expectedResult = false;
		$testName = "API - Ujidini Delete data more than 3 months old";
		$notes = $this->ModelUnitTest->formatNotes($result, $expectedResult);
		$this->unit->run($result, $expectedResult, $testName, $notes);
	}

	public function unitTestUjiDiniAPIPemohonSudahMenikahNIKPasanganKosong(){

		$wrongParameter = $this->parameterDataALot;
		$wrongParameter['nik'] = "6171023010780006";
		$wrongParameter['nama_pemohon'] = "BENTO ANDREAS MANAFE";
		$wrongParameter['npwp_pemohon'] = "920607538902000";
		$wrongParameter['nik_pasangan'] = "";
		$wrongParameter['nama_pasangan'] = "";

		$jsonData = json_encode(
			$wrongParameter
		);
		$url = "http://localhost/ssbdev/UjiDini";
		$result = $this->utility->postBodyRawCurl($jsonData, $url);
		$result = json_decode($result, true);

		$expectedResult = array(
			"response_code" => "14",
			"fieldcek05" => "N"
		);
		$fieldCekError = "N";

		$finalresult = ($result["responsecode"] == $expectedResult["response_code"]) && ($result["responsedetails"][4]["valid"] == $fieldCekError);
		$finalExpectedResult = true;

		$testName = "API - UjiDini KTP Sudah Menikah NIK Pasangan Kosong ";

		$this->cleanUpTableForTesting($wrongParameter['nik']);

		$notes = $this->ModelUnitTest->formatNotes($result, $expectedResult);
		$this->unit->run($finalresult, $finalExpectedResult, $testName, $notes);
	}

	public function unitTestUjiDiniAPIPemohonBelumMenikahNIKPasanganKosong(){

		$wrongParameter = $this->parameterDataALot;
		$wrongParameter['nik'] = "3276012509910003";
		$wrongParameter['nama_pemohon'] = "Rendy Reivaldy";
		$wrongParameter['npwp_pemohon'] = "3276012509910003";
		$wrongParameter['nik_pasangan'] = "";
		$wrongParameter['nama_pasangan'] = "";

		$jsonData = json_encode(
			$wrongParameter
		);
		$url = "http://localhost/ssbdev/UjiDini";
		$result = $this->utility->postBodyRawCurl($jsonData, $url);

		$result = json_decode($result, true);

		$expectedResult = array(
			"response_code" => "00"
		);

		$testName = "API - UjiDini KTP Belum Menikah NIK Pasangan Kosong ";

		$this->cleanUpTableForTesting($wrongParameter['nik']);

		$notes = $this->ModelUnitTest->formatNotes($result, $expectedResult);
		$this->unit->run($result['responsecode'], $expectedResult['response_code'], $testName, $notes);
	}

	public function unitTestUjiDiniAPISuamiIstriTidakBolehDuaKali(){

		$wrongParameter = $this->parameterDataALot;
		$wrongParameter['nama_pemohon'] = "KETUT WIDIANI";
		$wrongParameter['jenis_kelamin'] = "P";
		$wrongParameter['nik'] = "5108064505780008";
		$wrongParameter['npwp_pemohon'] = "9206075389020001";
		$wrongParameter['nama_pasangan'] = "KADEK WIDIADA";
		$wrongParameter['nik_pasangan'] = "5108060507800005";

		// data biasa dulu
		$jsonData = json_encode(
			$this->parameterDataALot
		);
		$url = "http://localhost/ssbdev/UjiDini";
		$result = $this->utility->postBodyRawCurl($jsonData, $url);

		// data salah
		$jsonData = json_encode(
			$wrongParameter
		);
		$url = "http://localhost/ssbdev/UjiDini";
		$result = $this->utility->postBodyRawCurl($jsonData, $url);

		$result = json_decode($result, true);

		$expectedResult = array(
			"response_code" => "15"
		);

		$testName = "API - UjiDini NIK Istri sudah pernah mengajukan";

		$this->cleanUpTableForTesting($wrongParameter['nik']);
		$this->cleanUpTableForTesting($this->parameterDataALot['nik']);

		$notes = $this->ModelUnitTest->formatNotes($result, $expectedResult);
		$this->unit->run($result['responsecode'], $expectedResult['response_code'], $testName, $notes);

	}

	public function unitTestUjiDiniAPICekDukcapilJenisKelaminBeda(){

		$wrongParameter = $this->parameterDataALot;
		$wrongParameter['jenis_kelamin'] = "P";

		$jsonData = json_encode(
			$wrongParameter
		);
		$url = "http://localhost/ssbdev/UjiDini";
		$result = $this->utility->postBodyRawCurl($jsonData, $url);
		$result = json_decode($result, true);

		$expectedResult = array(
			"response_code" => "16",
			"fieldcek05" => "N"
		);
		$fieldCekError = "N";

		$finalresult = ($result["responsecode"] == $expectedResult["response_code"]) && ($result["responsedetails"][4]["valid"] == $fieldCekError);
		$finalExpectedResult = true;

		$testName = "API - UjiDini Jenis Kelamin tidak sesuai dengan dukcapil";

		$notes = $this->ModelUnitTest->formatNotes($result, $expectedResult);
		$this->unit->run($finalresult, $finalExpectedResult, $testName, $notes);
	}

	public function unitTestUjiDiniAPICekNPWPExist(){
		$jsonData = json_encode(
			$this->parameterDataALot
		);
		$url = "http://localhost/ssbdev/UjiDini";

		// hit dua kali buat simulasiin udah pernah
		$resultPre = $this->utility->postBodyRawCurl($jsonData, $url);
		$result = $this->utility->postBodyRawCurl($jsonData, $url);
		$result = json_decode($result, true);

		$expectedResult = array(
			"response_code" => "13"
		);
		$testName = "API - UjiDini dengan NPWP yang sudah tersimpan dan lolos di sistem";

		$this->cleanUpTableForTesting($this->parameterDataALot['nik']);

		$notes = $this->ModelUnitTest->formatNotes($result, $expectedResult);
		$this->unit->run($result["responsecode"], $expectedResult["response_code"], $testName, $notes);
	}

	public function unitTestUjiDiniAPICekDukcapilNoKKSuamiIstriHarusSama(){

		$wrongParameter = $this->parameterDataALot;
		$wrongParameter['nama_pemohon'] = "BENTO ANDREAS MANAFE";
		$wrongParameter['nik'] = "6171023010780006";
		$wrongParameter['npwp_pemohon'] = "6171023010780006";

		$jsonData = json_encode(
			$wrongParameter
		);
		$url = "http://localhost/ssbdev/UjiDini";
		$result = $this->utility->postBodyRawCurl($jsonData, $url);
		$result = json_decode($result, true);

		$expectedResult = array(
			"response_code" => "17",
			"fieldcek06" => "N"
		);
		$fieldCekError = "N";

		$finalresult = ($result["responsecode"] == $expectedResult["response_code"]) && ($result["responsedetails"][5]["valid"] == $fieldCekError);
		$finalExpectedResult = true;

		$testName = "API - UjiDini no KK pemohon dan pasangan tidak sesuai";

		$this->cleanUpTableForTesting($wrongParameter['nik']);

		$notes = $this->ModelUnitTest->formatNotes($result, $expectedResult);
		$this->unit->run($finalresult, $finalExpectedResult, $testName, $notes);
	}

	public function unitTestUjiDiniAPITanggalUjiManual(){
		$newParameter = $this->parameterDataALot;
		$newParameter['tanggal_uji'] = "2020-01-01";
		$jsonData = json_encode(
			$newParameter
		);

		// change parameter manual by unit test
		$query = $this->db->query(
            "
            SELECT value FROM param WHERE name = 'manual_tgl_uji'
            "
            , FALSE
        );
        $resultDefault = $query->result_array()[0];
        $defaultValue = $resultDefault['value'];

		$this->db->query(
			"UPDATE param SET value = '1' WHERE name = 'manual_tgl_uji'", 
			FALSE
		);

		$url = "http://localhost/ssbdev/UjiDini";
		$result = $this->utility->postBodyRawCurl($jsonData, $url);

		$result = json_decode($result, true);

		// change parameter back to default
		$this->db->query(
			"UPDATE param SET value = '$defaultValue' WHERE name = 'manual_tgl_uji'", 
			FALSE
		);

		$nik = $newParameter['nik'];
		$cekTanggalDataUji = $this->db->query(
			"SELECT id_uji, created_at FROM data_uji WHERE nik = '$nik'",
			FALSE
		)->result_array()[0];
		$idUji = $cekTanggalDataUji['id_uji'];
		$cekTanggalDataUjiHist = $this->db->query(
			"SELECT created_at FROM data_uji_hist WHERE id_uji = '$idUji'",
			FALSE
		)->result_array()[0];
		$cekTanggalResultUji = $this->db->query(
			"SELECT tanggaluji FROM resultuji WHERE id_uji = '$idUji'",
			FALSE
		)->result_array()[0];
		$cekTanggalResultUjiHist = $this->db->query(
			"SELECT tanggaluji FROM resultuji_hist WHERE id_uji = '$idUji'",
			FALSE
		)->result_array()[0];

		$expected = true;
		$test = (
					$cekTanggalDataUji['created_at'] == ($newParameter['tanggal_uji'] . " 00:00:00") &&
					$cekTanggalDataUjiHist['created_at'] ==  ($newParameter['tanggal_uji'] . " 00:00:00") &&
					$cekTanggalResultUji['tanggaluji'] ==  ($newParameter['tanggal_uji'] . " 00:00:00") &&
					$cekTanggalResultUjiHist['tanggaluji'] ==  ($newParameter['tanggal_uji'] . " 00:00:00")
				);

		$testName = "API - UjiDini manual tanggal uji";

		$notes = $this->ModelUnitTest->formatNotes($test, $expected);
		$this->unit->run($test, $expected, $testName, $notes);
	}

	public function unitTestBatalUjiAPIDataNIKTidakAda(){
		$parameter = array(
			"kodebank" => $this->parameterDataALot['kodebank'],
			"loginid" =>  $this->parameterDataALot['loginid'],
			"nik" =>  "1234567890"
		);

		$jsonData = json_encode(
			$parameter
		);
		$url = "http://localhost/ssbdev/BatalUji";
		$result = $this->utility->postBodyRawCurl($jsonData, $url);
		$result = json_decode($result, true);

		$expectedResult = array(
			"response_code" => "18"
		);

		$testName = "API - Batal Uji NIK Tidak Ada";

		$notes = $this->ModelUnitTest->formatNotes($result, $expectedResult);
		$this->unit->run($result['responsecode'], $expectedResult["response_code"], $testName, $notes);
	}














}
