<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
		$this->load->model('ModelUser');
		$this->load->model('ModelParam');
		$this->load->model('ModelMenu');
		$this->utility->everyControllerConstruct($this);
    }

	public function index()
	{
		// LOGGING DATA
		$this->loggings->logDatabase($this->session->userdata('id'), 'View', 'Menu Index page', $this);

		$subparameter['dataMenuAll'] = $this->ModelMenu->getMenuData();
		$subparameter['rightname'] = $this->utility->getNameAndRole($this);
		$subparameter['leftmenu'] = $this->utility->getMenuByRole($this);
		$subparameter['appname'] = $this->utility->getAppsName();

		$parameter['appname'] = $subparameter['appname'];
		$parameter['mainappname'] = $this->utility->getAppsNameParam($this);
		$parameter['menuleft'] = $this->load->view('menu', $subparameter, TRUE);
		$parameter['menuright'] = $this->load->view('menuright', $subparameter, TRUE);
		$parameter['menucontent'] = $this->load->view('content_menu', $subparameter, TRUE);
		$this->load->view('mainmenu', $parameter);
	}

	public function createMenu(){
		// LOGGING DATA
		$this->loggings->logDatabase($this->session->userdata('id'), 'View', 'Menu Create page', $this);

		$subparameter = array();
		$subparameter['notificationDisplay'] = "none";
		$subparameter['notificationMessage'] = "";
		$subparameter['id'] = "";
		$subparameter['nama'] = "";
		$subparameter['icon'] = "";
		$subparameter['idparent'] = "";
		$subparameter['url'] = "";
		$subparameter['role'] = "";
		$subparameter['active'] = "";
		$subparameter['idparentddl'] = $this->ModelMenu->getIdParentForDropDown();
		$subparameter['roleddl'] = $this->ModelMenu->getRoleForDropDown();
		$subparameter['rightname'] = $this->utility->getNameAndRole($this);
		$subparameter['leftmenu'] = $this->utility->getMenuByRole($this);

		$parameter['appname'] = $this->utility->getAppsName();
		$parameter['mainappname'] = $this->utility->getAppsNameParam($this);
		$parameter['menuleft'] = $this->load->view('menu', $subparameter, TRUE);
		$parameter['menuright'] = $this->load->view('menuright', $subparameter, TRUE);
		$parameter['menucontent'] = $this->load->view('create_menu', $subparameter, TRUE);

		$this->load->view('mainmenu', $parameter);
	}

	public function editMenu($id){
		// LOGGING DATA
		$this->loggings->logDatabase($this->session->userdata('id'), 'View', 'Menu Edit page', $this);

		$dataGet = $this->ModelMenu->getDataByIdForEdit($id);

		$subparameter = array();
		$subparameter['notificationDisplay'] = "none";
		$subparameter['notificationMessage'] = "";
		$subparameter['id'] = $dataGet[0]['id'];
		$subparameter['nama'] = $dataGet[0]['name'];
		$subparameter['icon'] = $dataGet[0]['icon'];
		$subparameter['idparent'] = $dataGet[0]['id_parent'];
		$subparameter['url'] = $dataGet[0]['url'];
		$subparameter['role'] = $dataGet[0]['user_role'];
		$subparameter['active'] = $dataGet[0]['active'];
		$subparameter['roleddl'] = $this->ModelMenu->getRoleForDropDown();
		$subparameter['idparentddl'] = $this->ModelMenu->getIdParentForDropDown();
		$subparameter['rightname'] = $this->utility->getNameAndRole($this);
		$subparameter['leftmenu'] = $this->utility->getMenuByRole($this);

		$parameter['appname'] = $this->utility->getAppsName();
		$parameter['mainappname'] = $this->utility->getAppsNameParam($this);
		$parameter['menuleft'] = $this->load->view('menu', $subparameter, TRUE);
		$parameter['menuright'] = $this->load->view('menuright', $subparameter, TRUE);
		$parameter['menucontent'] = $this->load->view('edit_menu', $subparameter, TRUE);

		$this->load->view('mainmenu', $parameter);
	}

	public function submitCreateMenu(){
		$subparameter = array();

		$subparameter['nama'] = $this->security->xss_clean($this->input->post('nama'));
		$subparameter['icon'] = $this->security->xss_clean($this->input->post('icon'));
		$subparameter['idparent'] = $this->security->xss_clean($this->input->post('idparent'));
		$subparameter['url'] = $this->security->xss_clean($this->input->post('url'));
		$subparameter['role'] = $this->security->xss_clean($this->input->post('role'));
		$subparameter['active'] = $this->security->xss_clean($this->input->post('active'));
		$subparameter['notificationDisplay'] = "none";
		$subparameter['notificationMessage'] = "";

		if($subparameter['nama'] == ''){
			$subparameter['notificationDisplay'] = "block";
			$subparameter['notificationMessage'] = "Nama Tidak Boleh Kosong";
		} else if ($subparameter['idparent'] != '' && $subparameter['url'] == '') {
			$subparameter['notificationDisplay'] = "block";
			$subparameter['notificationMessage'] = "URL Tidak Boleh Kosong";
		} else {
			if($subparameter['active'] == 'on'){
				$subparameter['active'] = 1;
			} else {
				$subparameter['active'] = 0;
			}
			if($subparameter['idparent'] == ''){
				$subparameter['idparent'] = 0;
			} 

			// LOGGING DATA
			$this->loggings->logDatabase($this->session->userdata('id'), 'Submit', 'Submit create menu success : ' . json_encode($subparameter), $this);

			$this->ModelMenu->createMenu($subparameter);
			redirect('Menu/', 'refresh');
		}

		// LOGGING DATA
		$this->loggings->logDatabase($this->session->userdata('id'), 'Submit', 'Submit create menu error : ' . json_encode($subparameter), $this);

		$subparameter['roleddl'] = $this->ModelMenu->getRoleForDropDown();
		$subparameter['idparentddl'] = $this->ModelMenu->getIdParentForDropDown();
		$subparameter['rightname'] = $this->utility->getNameAndRole($this);
		$subparameter['leftmenu'] = $this->utility->getMenuByRole($this);

		$parameter['appname'] = $this->utility->getAppsName();
		$parameter['mainappname'] = $this->utility->getAppsNameParam($this);
		$parameter['menuleft'] = $this->load->view('menu', $subparameter, TRUE);
		$parameter['menuright'] = $this->load->view('menuright', $subparameter, TRUE);
		$parameter['menucontent'] = $this->load->view('create_menu', $subparameter, TRUE);

		$this->load->view('mainmenu', $parameter);
	}

	public function submitEditMenu($id){
		$subparameter = array();

		$subparameter['nama'] = $this->security->xss_clean($this->input->post('nama'));
		$subparameter['icon'] = $this->security->xss_clean($this->input->post('icon'));
		$subparameter['idparent'] = $this->security->xss_clean($this->input->post('idparent'));
		$subparameter['url'] = $this->security->xss_clean($this->input->post('url'));
		$subparameter['role'] = $this->security->xss_clean($this->input->post('role'));
		$subparameter['active'] = $this->security->xss_clean($this->input->post('active'));
		$subparameter['id'] = $id;
		$subparameter['notificationDisplay'] = "none";
		$subparameter['notificationMessage'] = "";

		if($subparameter['nama'] == ''){
			$subparameter['notificationDisplay'] = "block";
			$subparameter['notificationMessage'] = "Nama Tidak Boleh Kosong";
		} else if ($subparameter['idparent'] != '' && $subparameter['url'] == '') {
			$subparameter['notificationDisplay'] = "block";
			$subparameter['notificationMessage'] = "URL Tidak Boleh Kosong";
		} else {
			if($subparameter['active'] == 'on'){
				$subparameter['active'] = 1;
			} else {
				$subparameter['active'] = 0;
			}
			if($subparameter['idparent'] == ''){
				$subparameter['idparent'] = 0;
			} 

			// LOGGING DATA
			$this->loggings->logDatabase($this->session->userdata('id'), 'Submit', 'Submit edit menu success : ' . json_encode($subparameter), $this);

			$this->ModelMenu->updateDataMenu($subparameter);
			redirect('Menu/', 'refresh');
		}

		// LOGGING DATA
		$this->loggings->logDatabase($this->session->userdata('id'), 'Submit', 'Submit edit menu error : ' . json_encode($subparameter), $this);

		$subparameter['roleddl'] = $this->ModelMenu->getRoleForDropDown();
		$subparameter['idparentddl'] = $this->ModelMenu->getIdParentForDropDown();
		$subparameter['rightname'] = $this->utility->getNameAndRole($this);
		$subparameter['leftmenu'] = $this->utility->getMenuByRole($this);

		$parameter['appname'] = $this->utility->getAppsName();
		$parameter['mainappname'] = $this->utility->getAppsNameParam($this);
		$parameter['menuleft'] = $this->load->view('menu', $subparameter, TRUE);
		$parameter['menuright'] = $this->load->view('menuright', $subparameter, TRUE);
		$parameter['menucontent'] = $this->load->view('edit_menu', $subparameter, TRUE);

		$this->load->view('mainmenu', $parameter);
	}

	public function deleteMenu($id){
		// LOGGING DATA
		$deletedData = $this->ModelMenu->getDataByIdForEdit($id);
		$this->loggings->logDatabase($this->session->userdata('id'), 'Delete', 'Deleted Menu  : ' . json_encode($deletedData), $this);

		$this->ModelMenu->deleteMenu($id);
		redirect('Menu/', 'refresh');
	}
}
