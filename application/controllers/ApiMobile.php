<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class ApiMobile extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['index_post']['limit'] = 100; // 100 requests per hour per user/key
    }

    /*
        index :
            TYPE 1 : login 
                    params : 
                        # who -> username
                        # salt -> password
                        # type -> api type

            TYPE 2 : get data upil
                    params : 
                        # who -> id user
                        # salt -> password
                        # type -> type api
                        # lastid -> last get id from database

            TYPE 3 : get data upil by search
                    params :
                        # who -> id user
                        # salt -> password
                        # type -> type api
                        # word -> searched word, it will be searched in name, and description

            TYPE 4 : logout
                    params :
                        # who -> id user
                        # type -> type api

            TYPE 5 : insert upil
                    params :
                        # who -> id user
                        # salt -> password
                        # type -> type api
                        # name -> name pass nya
                        # description -> description pass nya
                        # username -> username pass nya
                        # hashed -> hashed pass nya

            TYPE 6 : edit upil
                    params :
                        # who -> id user
                        # salt -> password
                        # type -> type api
                        # id -> id pass yang mau diubah
                        # name -> name pass nya
                        # description -> description pass nya
                        # username -> username pass nya
                        # hashed -> hashed pass nya

            TYPE 7 : delete upil
                    params :
                        # who -> id user
                        # salt -> password
                        # type -> type api
                        # id -> id pass yang mau diapus
    */
    public function index_post()
    {
        $type = $this->post('type');
        if($type == 1){
            $this->login();
        } else if ($type == 2 || $type == 3){
            $this->getDataUpil($type);
        } else if ($type == 4){
            $this->logout();
        } else if ($type == 5 || $type == 6){
            $this->insertEditDataUpil($type);
        } else if ($type == 7){
            $this->deletetDataUpil();
        }
    }

    public function login(){
        $this->load->model('ModelUser');
        $this->load->model('ModelParam');
        $dataArray['username'] = $this->post('who');
        $dataArray['password'] = $this->post('salt');
        $result = $this->ModelUser->checkLogin($dataArray);
        if($result['status']){
            $id = $result['id'];
            $this->ModelUser->successLoginApiMobile($id);
            $message = [
                'status' => $result['status'],
                'id' => $id
            ];
            // LOGGING DATA
            $this->loggings->logDatabase($id, 'login mobile', '', $this);
        } else {
            $message = [
                'status' => $result['status'],
                'message' => $result['message'] 
            ];
            // LOGGING DATA
            $this->loggings->logDatabase('', 'login mobile false', '', $this);
        }
        $this->set_response($message, REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code
    }

    public function logout(){
        $this->load->model('ModelUser');
        $id = $this->post('who');
        $this->ModelUser->logOutApiMobile($id);
        $message = [
                'status' => true
            ];
        // LOGGING DATA
        $this->loggings->logDatabase($id, 'logout mobile', '', $this);
        $this->set_response($message, REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code
    }

    public function getDataUpil($type){
        $this->load->model('ModelUpil');
        $this->load->model('ModelUser');
        $id = $this->post('who');
        $pass = $this->post('salt');
        $resultCheckUser = $this->ModelUser->getUserApiMobile($id, $pass);
        if(count($resultCheckUser) > 0){
            if($type == 2){
                $lastId = $this->post('lastid');
                $message = [
                    'status' => true,
                    'data' => $this->ModelUpil->getUpilDataApiMobile($lastId)
                ];
                $this->loggings->logDatabase($id, 'get data upil Mobile', json_encode($message), $this);
            } else {
                $word = $this->post('word');
                $message = [
                    'status' => true,
                    'data' => $this->ModelUpil->getUpilDataSearchApiMobile($word)
                ];
                $this->loggings->logDatabase($id, 'get data upil Mobile searched', json_encode($message), $this);
            }
        } else {
            $message = [
                'status' => false,
                'message' => "User not found, or not yet login. Not authorized"
            ];
            $this->loggings->logDatabase($id, 'get data upil Mobile error', json_encode($message), $this);
        }
        $this->set_response($message, REST_Controller::HTTP_OK);
    }

    public function insertEditDataUpil($type){
        $this->load->model('ModelUpil');
        $this->load->model('ModelUser');
        $id = $this->post('who');
        $pass = $this->post('salt');
        $resultCheckUser = $this->ModelUser->getUserApiMobile($id, $pass);
        $dataArray = array();
        $dataArray['name'] = $this->post('name');
        $dataArray['desc'] = $this->post('description');
        $dataArray['username'] = $this->post('username');
        $dataArray['hashed'] = $this->utility->encryptString($this, $this->post('hashed'));
        if(count($resultCheckUser) > 0){
            if($type == 5){
                $this->ModelUpil->createUpilData($dataArray);
                $message = [
                    'status' => true
                ];
                $this->loggings->logDatabase($id, 'insert upil Mobile', json_encode($dataArray) . " | " . json_encode($message), $this);
            } else {
                $dataArray['id'] = $this->post('id');
                $this->ModelUpil->updateDataUpil($dataArray);
                $message = [
                    'status' => true
                ];
                $this->loggings->logDatabase($id, 'update upil Mobile', json_encode($dataArray) . " | " . json_encode($message), $this);
            }
        } else {
            $message = [
                'status' => false,
                'message' => "User not found, or not yet login. Not authorized"
            ];
            $this->loggings->logDatabase($id, 'update upil Mobile error', json_encode($dataArray) . " | " . json_encode($message), $this);

        }
        $this->set_response($message, REST_Controller::HTTP_OK);
    }

    public function deletetDataUpil($type){
        $this->load->model('ModelUpil');
        $this->load->model('ModelUser');
        $id = $this->post('who');
        $pass = $this->post('salt');
        $idPass = $this->post('id');
        $resultCheckUser = $this->ModelUser->getUserApiMobile($id, $pass);
        if(count($resultCheckUser) > 0){
            $this->ModelUpil->deleteUpil($idPass);
            $message = [
                'status' => true
            ];
            $this->loggings->logDatabase($id, 'delete upil Mobile', json_encode($message), $this);
        } else {
            $message = [
                'status' => false,
                'message' => "User not found, or not yet login. Not authorized"
            ];
            $this->loggings->logDatabase($id, 'delete upil Mobile error', json_encode($message), $this);
        }
        $this->set_response($message, REST_Controller::HTTP_OK);
    }
}   

