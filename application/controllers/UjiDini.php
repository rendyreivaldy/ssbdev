<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class UjiDini extends REST_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('ModelApi');
        $this->load->model('ModelUjiDini');
    }

    public function index_post()
    {
        $parameter = $this->ModelApi->getParameterRawBody(); 
        $arrayToCheck = array(
            "nama_pemohon",
            "pekerjaan",
            "jenis_kelamin",
            "nik",
            "npwp_pemohon",

            "penghasilan",
            "nama_pasangan",
            "nik_pasangan",
            "no_sp3k",
            "harga_rumah",

            "uang_muka",
            "sbum",
            "nilai_kpr",
            "suku_bunga",
            "tenor_bulan",

            "angsuran",
            "nilai_subsidi",
            "nama_pengembang",
            "npwp_pengembang",
            "nama_perumahan",

            "alamat_agunan",
            "kota_agunan",
            "provinsi",
            "kode_adm_wilayah",
            "kode_pos_agunan",

            "luas_tanah",
            "luas_bangunan",
            "kodebank",
            "loginid"
        );
        $cekParameter = $this->ModelApi->cekParameter($parameter, $arrayToCheck);

        // pastikan parameter yang dikirim sesuai dengan yang di dokumentasi
        if($cekParameter['responseCode'] != "00") {
            $message = $this->ModelApi->getResponseMessage('08');
            $message['response_message'] .= " (" . $cekParameter['responseMessage'] . ")";
        } else {
            $kodeBank = $parameter['kodebank'];
            $message = $this->ModelApi->cekLoginIdApi($parameter);
            if($message['responsecode'] == "00")
                $message = $this->ModelUjiDini->ujiDiniStartFlow($parameter);  
        }
        $message = $this->ModelApi->changeResponseMessageFormat($message);
        $kodeBank = isset($parameter['kodebank']) ? $parameter['kodebank'] : '';
        
        // LOGGING
        $logData = array(
            "parameter" => $parameter,
            "response" => $message
        );
        $this->loggings->logDatabase($kodeBank, 'API Uji Dini', json_encode($logData), $this);
        $this->set_response($message, REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code
    }
}   

