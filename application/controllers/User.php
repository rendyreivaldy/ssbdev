<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
		$this->load->model('ModelUser');
		$this->load->model('ModelParam');
		$this->utility->everyControllerConstruct($this);
    }

	public function index()
	{
		// LOGGING DATA
		$this->loggings->logDatabase($this->session->userdata('id'), 'View', 'User Index page', $this);

		$subparameter['dataUserAll'] = $this->ModelUser->getUserData();
		$subparameter['rightname'] = $this->utility->getNameAndRole($this);
		$subparameter['leftmenu'] = $this->utility->getMenuByRole($this);
		$subparameter['appname'] = $this->utility->getAppsName();

		$parameter['appname'] = $subparameter['appname'];
		$parameter['mainappname'] = $this->utility->getAppsNameParam($this);
		$parameter['menuleft'] = $this->load->view('menu', $subparameter, TRUE);
		$parameter['menuright'] = $this->load->view('menuright', $subparameter, TRUE);
		$parameter['menucontent'] = $this->load->view('content_user', $subparameter, TRUE);
		$this->load->view('mainmenu', $parameter);
	}

	public function createUser(){
		// LOGGING DATA
		$this->loggings->logDatabase($this->session->userdata('id'), 'View', 'User Create page', $this);

		$subparameter = array();
		$subparameter['notificationDisplay'] = "none";
		$subparameter['notificationMessage'] = "";
		$subparameter['id'] = "";
		$subparameter['nama'] = "";
		$subparameter['username'] = "";
		$subparameter['password'] = "";
		$subparameter['passwordkonfirm'] = "";
		$subparameter['role'] = "";
		$subparameter['isblocked'] = "";
		$subparameter['roleddl'] = $this->ModelUser->getRoleForDropDown();
		$subparameter['rightname'] = $this->utility->getNameAndRole($this);
		$subparameter['leftmenu'] = $this->utility->getMenuByRole($this);

		$parameter['appname'] = $this->utility->getAppsName();
		$parameter['mainappname'] = $this->utility->getAppsNameParam($this);
		$parameter['menuleft'] = $this->load->view('menu', $subparameter, TRUE);
		$parameter['menuright'] = $this->load->view('menuright', $subparameter, TRUE);
		$parameter['menucontent'] = $this->load->view('create_user', $subparameter, TRUE);

		$this->load->view('mainmenu', $parameter);
	}

	public function editUser($id){
		// LOGGING DATA
		$this->loggings->logDatabase($this->session->userdata('id'), 'View', 'User Edit page', $this);

		$dataGet = $this->ModelUser->getDataByIdForEdit($id);

		$subparameter = array();
		$subparameter['notificationDisplay'] = "none";
		$subparameter['notificationMessage'] = "";
		$subparameter['id'] = $dataGet[0]['id'];
		$subparameter['nama'] = $dataGet[0]['nama'];
		$subparameter['username'] = $dataGet[0]['username'];
		$subparameter['password'] = "";
		$subparameter['passwordkonfirm'] = "";
		$subparameter['role'] = $dataGet[0]['id_role'];
		$subparameter['isblocked'] = $dataGet[0]['is_blocked'];
		$subparameter['roleddl'] = $this->ModelUser->getRoleForDropDown();
		$subparameter['rightname'] = $this->utility->getNameAndRole($this);
		$subparameter['leftmenu'] = $this->utility->getMenuByRole($this);

		$parameter['appname'] = $this->utility->getAppsName();
		$parameter['mainappname'] = $this->utility->getAppsNameParam($this);
		$parameter['menuleft'] = $this->load->view('menu', $subparameter, TRUE);
		$parameter['menuright'] = $this->load->view('menuright', $subparameter, TRUE);
		$parameter['menucontent'] = $this->load->view('edit_user', $subparameter, TRUE);

		$this->load->view('mainmenu', $parameter);
	}

	public function viewUserDetail($id){
		$dataGet = $this->ModelUser->getDataByIdForEdit($id);

		$subparameter = array();
		$subparameter['id'] = $dataGet[0]['id'];
		$subparameter['nama'] = $dataGet[0]['nama'];
		$subparameter['username'] = $dataGet[0]['username'];
		$subparameter['role'] = $dataGet[0]['id_role'];
		$subparameter['isblocked'] = $dataGet[0]['is_blocked'];
		$subparameter['lastlogindate'] = $dataGet[0]['last_login_date'];
		$subparameter['createdby'] = $dataGet[0]['created_by'];
		$subparameter['updatedby'] = $dataGet[0]['updated_by'];
		$subparameter['rightname'] = $this->utility->getNameAndRole($this);
		$subparameter['leftmenu'] = $this->utility->getMenuByRole($this);

		$parameter['appname'] = $this->utility->getAppsName();
		$parameter['mainappname'] = $this->utility->getAppsNameParam($this);
		$parameter['menuleft'] = $this->load->view('menu', $subparameter, TRUE);
		$parameter['menuright'] = $this->load->view('menuright', $subparameter, TRUE);
		$parameter['menucontent'] = $this->load->view('view_user', $subparameter, TRUE);

		$this->load->view('mainmenu', $parameter);
	}

	public function changePassword($id){
		// LOGGING DATA
		$this->loggings->logDatabase($this->session->userdata('id'), 'View', 'User change password page', $this);

		$subparameter = array();
		$subparameter['notificationDisplay'] = "none";
		$subparameter['notificationMessage'] = "";
		$subparameter['id'] = $id;
		$subparameter['oldpassword'] = "";
		$subparameter['password'] = "";
		$subparameter['passwordkonfirm'] = "";
		$subparameter['rightname'] = $this->utility->getNameAndRole($this);
		$subparameter['leftmenu'] = $this->utility->getMenuByRole($this);

		$parameter['appname'] = $this->utility->getAppsName();
		$parameter['mainappname'] = $this->utility->getAppsNameParam($this);
		$parameter['menuleft'] = $this->load->view('menu', $subparameter, TRUE);
		$parameter['menuright'] = $this->load->view('menuright', $subparameter, TRUE);
		$parameter['menucontent'] = $this->load->view('ubah_password', $subparameter, TRUE);

		$this->load->view('mainmenu', $parameter);
	}

	public function submitCreateUser(){
		$subparameter = array();

		$subparameter['nama'] = $this->security->xss_clean($this->input->post('nama'));
		$subparameter['username'] = $this->security->xss_clean($this->input->post('username'));
		$subparameter['password'] = $this->security->xss_clean($this->input->post('password'));
		$subparameter['passwordkonfirm'] = $this->security->xss_clean($this->input->post('passwordkonfirm'));
		$subparameter['role'] = $this->security->xss_clean($this->input->post('role'));
		$subparameter['isblocked'] = $this->security->xss_clean($this->input->post('isblocked'));
		$this->ModelUser->manualConstructObject($subparameter);
		$subparameter['notificationDisplay'] = "none";
		$subparameter['notificationMessage'] = "";

		if($subparameter['nama'] == ''){
			$subparameter['notificationDisplay'] = "block";
			$subparameter['notificationMessage'] = "Nama Tidak Boleh Kosong";
		} else if ($subparameter['username'] == '') {
			$subparameter['notificationDisplay'] = "block";
			$subparameter['notificationMessage'] = "Username Tidak Boleh Kosong";
		} else if ($subparameter['password'] == '') {
			$subparameter['notificationDisplay'] = "block";
			$subparameter['notificationMessage'] = "Password Tidak Boleh Kosong";
		} else if ($subparameter['passwordkonfirm'] == '') {
			$subparameter['notificationDisplay'] = "block";
			$subparameter['notificationMessage'] = "Konfirmasi Password Tidak Boleh Kosong";
		} else if ($this->ModelUser->checkUserIsExist()['status']) {
			$subparameter['notificationDisplay'] = "block";
			$subparameter['notificationMessage'] = "Username ini sudah terdaftar di dalam sistem";
		} else {
			$subparameter['createdby'] = $this->session->userdata('id');
			if($subparameter['isblocked'] == 'on'){
				$subparameter['isblocked'] = 1;
			} else {
				$subparameter['isblocked'] = 0;
			}

			// LOGGING DATA
			$this->loggings->logDatabase($this->session->userdata('id'), 'Submit', 'Submit create user success : ' . json_encode($subparameter), $this);

			$this->ModelUser->registrationInputAdmin($subparameter);
			redirect('User/', 'refresh');
		}

		// LOGGING DATA
		$this->loggings->logDatabase($this->session->userdata('id'), 'Submit', 'Submit create user error : ' . json_encode($subparameter), $this);

		$subparameter['roleddl'] = $this->ModelUser->getRoleForDropDown();
		$subparameter['rightname'] = $this->utility->getNameAndRole($this);
		$subparameter['leftmenu'] = $this->utility->getMenuByRole($this);

		$parameter['appname'] = $this->utility->getAppsName();
		$parameter['mainappname'] = $this->utility->getAppsNameParam($this);
		$parameter['menuleft'] = $this->load->view('menu', $subparameter, TRUE);
		$parameter['menuright'] = $this->load->view('menuright', $subparameter, TRUE);
		$parameter['menucontent'] = $this->load->view('create_user', $subparameter, TRUE);
		$this->load->view('mainmenu', $parameter);
	}

	public function submitEditUser($id){
		$subparameter = array();

		$subparameter['nama'] = $this->security->xss_clean($this->input->post('nama'));
		$subparameter['username'] = $this->security->xss_clean($this->input->post('username'));
		$subparameter['role'] = $this->security->xss_clean($this->input->post('role'));
		$subparameter['isblocked'] = $this->security->xss_clean($this->input->post('isblocked'));
		$subparameter['notificationDisplay'] = "none";
		$subparameter['notificationMessage'] = "";
		$subparameter['id'] = $id;

		if($subparameter['nama'] == ''){
			$subparameter['notificationDisplay'] = "block";
			$subparameter['notificationMessage'] = "Nama Tidak Boleh Kosong";
		} else if ($subparameter['username'] == '') {
			$subparameter['notificationDisplay'] = "block";
			$subparameter['notificationMessage'] = "Username Tidak Boleh Kosong";
		} else {
			$subparameter['createdby'] = $this->session->userdata('id');
			if($subparameter['isblocked'] == 'on'){
				$subparameter['isblocked'] = 1;
			} else {
				$subparameter['isblocked'] = 0;
			}

			// LOGGING DATA
			$this->loggings->logDatabase($this->session->userdata('id'), 'Submit', 'Submit edit user success : ' . json_encode($subparameter), $this);

			$this->ModelUser->updateDataUser($subparameter, $this->session);
			redirect('User/', 'refresh');
		}

		// LOGGING DATA
		$this->loggings->logDatabase($this->session->userdata('id'), 'Submit', 'Submit edit user error : ' . json_encode($subparameter), $this);

		$subparameter['roleddl'] = $this->ModelUser->getRoleForDropDown();
		$subparameter['rightname'] = $this->utility->getNameAndRole($this);
		$subparameter['leftmenu'] = $this->utility->getMenuByRole($this);

		$parameter['appname'] = $this->utility->getAppsName();
		$parameter['mainappname'] = $this->utility->getAppsNameParam($this);
		$parameter['menuleft'] = $this->load->view('menu', $subparameter, TRUE);
		$parameter['menuright'] = $this->load->view('menuright', $subparameter, TRUE);
		$parameter['menucontent'] = $this->load->view('edit_user', $subparameter, TRUE);

		$this->load->view('mainmenu', $parameter);
	}

	public function submitChangePassword($id){
		$subparameter = array();

		$subparameter['password'] = $this->security->xss_clean($this->input->post('password'));
		$subparameter['passwordkonfirm'] = $this->security->xss_clean($this->input->post('passwordkonfirm'));
		$subparameter['oldpassword'] = $this->security->xss_clean($this->input->post('oldpassword'));
		$subparameter['notificationDisplay'] = "none";
		$subparameter['notificationMessage'] = "";
		$subparameter['id'] = $id;

		if ($subparameter['oldpassword'] == '') {
			$subparameter['notificationDisplay'] = "block";
			$subparameter['notificationMessage'] = "Password Lama Tidak Boleh Kosong";
		} else if ($subparameter['password'] == '') {
			$subparameter['notificationDisplay'] = "block";
			$subparameter['notificationMessage'] = "Password Tidak Boleh Kosong";
		} else if ($subparameter['passwordkonfirm'] == '') {
			$subparameter['notificationDisplay'] = "block";
			$subparameter['notificationMessage'] = "Konfirmasi Password Tidak Boleh Kosong";
		} else if (!$this->ModelUser->checkOldPassword($subparameter)) {
			$subparameter['notificationDisplay'] = "block";
			$subparameter['notificationMessage'] = "Password Lama Salah";
		} else {

			// LOGGING DATA
			$this->loggings->logDatabase($this->session->userdata('id'), 'Submit', 'Submit change password user success : ' . json_encode($subparameter), $this);

			$this->ModelUser->changePassword($subparameter);
			redirect('User/editUser/' . $id, 'refresh');
		}

		// LOGGING DATA
		$this->loggings->logDatabase($this->session->userdata('id'), 'Submit', 'Submit change password user error : ' . json_encode($subparameter), $this);

		$subparameter['leftmenu'] = $this->utility->getMenuByRole($this);
		$subparameter['rightname'] = $this->utility->getNameAndRole($this);

		$parameter['appname'] = $this->utility->getAppsName();
		$parameter['mainappname'] = $this->utility->getAppsNameParam($this);
		$parameter['menuleft'] = $this->load->view('menu', $subparameter, TRUE);
		$parameter['menuright'] = $this->load->view('menuright', $subparameter, TRUE);
		$parameter['menucontent'] = $this->load->view('ubah_password', $subparameter, TRUE);

		$this->load->view('mainmenu', $parameter);
	}

	public function deleteUser($id){
		// LOGGING DATA
		$deletedData = $this->ModelUser->getDataByIdForEdit($id);
		$this->loggings->logDatabase($this->session->userdata('id'), 'Delete', 'Deleted User  : ' . json_encode($deletedData), $this);

		$this->ModelUser->deleteUser($id);
		redirect('User/', 'refresh');
	}
}
