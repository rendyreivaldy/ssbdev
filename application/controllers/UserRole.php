<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserRole extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
		$this->load->model('ModelUser');
		$this->load->model('ModelParam');
		$this->load->model('ModelUserRole');
		$this->utility->everyControllerConstruct($this);
    }

	public function index()
	{
		// LOGGING DATA
		$this->loggings->logDatabase($this->session->userdata('id'), 'View', 'User Role Index page', $this);

		$subparameter['dataUserRoleAll'] = $this->ModelUserRole->getUserRoleData();
		$subparameter['rightname'] = $this->utility->getNameAndRole($this);
		$subparameter['leftmenu'] = $this->utility->getMenuByRole($this);
		$subparameter['appname'] = $this->utility->getAppsName();

		$parameter['appname'] = $subparameter['appname'];
		$parameter['mainappname'] = $this->utility->getAppsNameParam($this);
		$parameter['menuleft'] = $this->load->view('menu', $subparameter, TRUE);
		$parameter['menuright'] = $this->load->view('menuright', $subparameter, TRUE);
		$parameter['menucontent'] = $this->load->view('content_userrole', $subparameter, TRUE);

		$this->load->view('mainmenu', $parameter);
	}

	public function createUserRole(){
		// LOGGING DATA
		$this->loggings->logDatabase($this->session->userdata('id'), 'View', 'User Role Create page', $this);

		$subparameter = array();
		$subparameter['notificationDisplay'] = "none";
		$subparameter['notificationMessage'] = "";
		$subparameter['id'] = "";
		$subparameter['role'] = "";
		$subparameter['keterangan'] = "";
		$subparameter['landingpage'] = "";
		$subparameter['rightname'] = $this->utility->getNameAndRole($this);
		$subparameter['leftmenu'] = $this->utility->getMenuByRole($this);

		$parameter['appname'] = $this->utility->getAppsName();
		$parameter['mainappname'] = $this->utility->getAppsNameParam($this);
		$parameter['menuleft'] = $this->load->view('menu', $subparameter, TRUE);
		$parameter['menuright'] = $this->load->view('menuright', $subparameter, TRUE);
		$parameter['menucontent'] = $this->load->view('create_userrole', $subparameter, TRUE);

		$this->load->view('mainmenu', $parameter);
	}

	public function editUserRole($id){
		// LOGGING DATA
		$this->loggings->logDatabase($this->session->userdata('id'), 'View', 'User Role Edit page', $this);

		$dataGet = $this->ModelUserRole->getDataByIdForEdit($id);

		$subparameter = array();
		$subparameter['notificationDisplay'] = "none";
		$subparameter['notificationMessage'] = "";
		$subparameter['id'] = $dataGet[0]['id'];
		$subparameter['role'] = $dataGet[0]['role'];
		$subparameter['keterangan'] = $dataGet[0]['keterangan'];
		$subparameter['landingpage'] = $dataGet[0]['landing_page'];
		$subparameter['rightname'] = $this->utility->getNameAndRole($this);
		$subparameter['leftmenu'] = $this->utility->getMenuByRole($this);

		$parameter['appname'] = $this->utility->getAppsName();
		$parameter['mainappname'] = $this->utility->getAppsNameParam($this);
		$parameter['menuleft'] = $this->load->view('menu', $subparameter, TRUE);
		$parameter['menuright'] = $this->load->view('menuright', $subparameter, TRUE);
		$parameter['menucontent'] = $this->load->view('edit_userrole', $subparameter, TRUE);

		$this->load->view('mainmenu', $parameter);
	}

	public function submitCreateUserrole(){
		$subparameter = array();

		$subparameter['role'] = $this->security->xss_clean($this->input->post('role'));
		$subparameter['keterangan'] = $this->security->xss_clean($this->input->post('keterangan'));
		$subparameter['landingpage'] = $this->security->xss_clean($this->input->post('landingpage'));
		$subparameter['notificationDisplay'] = "none";
		$subparameter['notificationMessage'] = "";

		if($subparameter['role'] == ''){
			$subparameter['notificationDisplay'] = "block";
			$subparameter['notificationMessage'] = "Role Tidak Boleh Kosong";
		} else if($subparameter['landingpage'] == ''){
			$subparameter['notificationDisplay'] = "block";
			$subparameter['notificationMessage'] = "Landing Page Tidak Boleh Kosong";
		} else {

			// LOGGING DATA
			$this->loggings->logDatabase($this->session->userdata('id'), 'Submit', 'Submit create user role success : ' . json_encode($subparameter), $this);

			$this->ModelUserRole->createUserRole($subparameter);
			redirect('UserRole/', 'refresh');
		}

		// LOGGING DATA
		$this->loggings->logDatabase($this->session->userdata('id'), 'Submit', 'Submit create user role error : ' . json_encode($subparameter), $this);

		$subparameter['rightname'] = $this->utility->getNameAndRole($this);
		$subparameter['leftmenu'] = $this->utility->getMenuByRole($this);

		$parameter['appname'] = $this->utility->getAppsName();
		$parameter['mainappname'] = $this->utility->getAppsNameParam($this);
		$parameter['menuleft'] = $this->load->view('menu', $subparameter, TRUE);
		$parameter['menuright'] = $this->load->view('menuright', $subparameter, TRUE);
		$parameter['menucontent'] = $this->load->view('create_userrole', $subparameter, TRUE);

		$this->load->view('mainmenu', $parameter);
	}

	public function submitEditUserrole($id){

		$subparameter = array();

		$subparameter['role'] = $this->security->xss_clean($this->input->post('role'));
		$subparameter['keterangan'] = $this->security->xss_clean($this->input->post('keterangan'));
		$subparameter['landingpage'] = $this->security->xss_clean($this->input->post('landingpage'));
		$subparameter['id'] = $id;
		$subparameter['notificationDisplay'] = "none";
		$subparameter['notificationMessage'] = "";

		if($subparameter['role'] == ''){
			$subparameter['notificationDisplay'] = "block";
			$subparameter['notificationMessage'] = "Role Tidak Boleh Kosong";
		} else if($subparameter['landingpage'] == ''){
			$subparameter['notificationDisplay'] = "block";
			$subparameter['notificationMessage'] = "Landing Page Tidak Boleh Kosong";
		} else {

			// LOGGING DATA
			$this->loggings->logDatabase($this->session->userdata('id'), 'Submit', 'Submit edit user role success : ' . json_encode($subparameter), $this);

			$this->ModelUserRole->updateDataUserRole($subparameter);
			redirect('UserRole/', 'refresh');
		}

		// LOGGING DATA
		$this->loggings->logDatabase($this->session->userdata('id'), 'Submit', 'Submit edit user role error : ' . json_encode($subparameter), $this);

		$subparameter['rightname'] = $this->utility->getNameAndRole($this);
		$subparameter['leftmenu'] = $this->utility->getMenuByRole($this);

		$parameter['appname'] = $this->utility->getAppsName();
		$parameter['mainappname'] = $this->utility->getAppsNameParam($this);
		$parameter['menuleft'] = $this->load->view('menu', $subparameter, TRUE);
		$parameter['menuright'] = $this->load->view('menuright', $subparameter, TRUE);
		$parameter['menucontent'] = $this->load->view('edit_userrole', $subparameter, TRUE);

		$this->load->view('mainmenu', $parameter);
	}

	public function deleteUserRole($id){
		// LOGGING DATA
		$deletedData = $this->ModelUserRole->getDataByIdForEdit($id);
		$this->loggings->logDatabase($this->session->userdata('id'), 'Delete', 'Deleted user role  : ' . json_encode($deletedData), $this);

		$this->ModelUserRole->deleteUserRole($id);
		redirect('UserRole/', 'refresh');
	}
}
