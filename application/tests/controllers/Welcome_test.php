<?php
/**
 * Part of ci-phpunit-test
 *
 * @author     Kenji Suzuki <https://github.com/kenjis>
 * @license    MIT License
 * @copyright  2015 Kenji Suzuki
 * @link       https://github.com/kenjis/ci-phpunit-test
 */

class Welcome_test extends TestCase
{
	public function setUp()
    {
        $this->resetInstance();
        $this->CI->load->model('ModelUser');
        $this->obj = $this->CI->ModelUser;
    }

    public function test_formNotCompleted(){
    	$dataArray = array();
		$dataArray['username'] = '';
		$dataArray['password'] = '';
		$this->obj->manualConstructObject($dataArray);

		$expected = array(
			"status" => false,
			"message" => 'Username dan password tidak boleh kosong'
		);
		$test = $this->obj->checkIsFormLoginValid();
		$this->assertEquals($expected, $test);
    }
}
