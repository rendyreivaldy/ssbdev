<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ModelParam extends CI_Model {

	// fields in param table
	public $id;
	public $name;
	public $value;
	public $keterangan;
	
	public function __construct()
    {
    	
    }

	public function manualConstructObject($dataArray){
	    $this->id = isset($dataArray['id']) ? $dataArray['id'] : null;
	    $this->name = isset($dataArray['name']) ? $dataArray['name'] : null;
	    $this->value = isset($dataArray['value']) ? $dataArray['value'] : null;
	    $this->keterangan = isset($dataArray['keterangan']) ? $dataArray['keterangan'] : null;
	}

	public function objectToString(){
		return
			"id : " . $this->id .
			" | name : " . $this->name .
			" | value : " . $this->value .
			" | keterangan : " . $this->keterangan;

	}

	// get function

	public function getId(){
		return $this->id;
	}

	public function getName(){
		return $this->name;
	}

	public function getValue(){
		return $this->value;
	}

	public function getKeterangan(){
		return $this->keterangan;
	}

	// set function

	public function setId($data){
		$this->id = $data;
	}

	public function setName($data){
		$this->name = $data;
	}

	public function setValue($data){
		$this->value = $data;
	}

	public function setKeterangan($data){
		$this->keterangan = $data;
	}

	public function getParameter($key){
		$this->db->where('name', $key);
        $query = $this->db->get('param');
        return $query->row()->value;
	}

	////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////// FUNCTION /////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////

	public function getParamData(){
		$query = $this->db->query(
			"SELECT 
			*
			FROM param"
			, FALSE
		);
		return $query->result_array();
	}

	public function createParam($dataArray){
		$dataArray['name'] = $dataArray['name'];
		$dataArray['value'] = $dataArray['value'];
		$dataArray['keterangan'] = $dataArray['keterangan'];
		$this->manualConstructObject($dataArray);
		$this->saveObjectToDatabase();
	}

	public function saveObjectToDatabase(){
		$data = array(
			'name' => $this->name,
			'value' => $this->value,
			'keterangan' => $this->keterangan
		);
		return $this->db->insert('param', $data);
	}

	public function getDataByIdForEdit($id){
		$query = $this->db->get_where(
        	'param', 
        	array(
        		'id' => $id
        	)
        );
		return $query->result_array();
	}

	public function getParameterByKey($key){
		$query = $this->db->get_where(
        	'param', 
        	array(
        		'name' => $key
        	)
        );
		return $query->result_array()[0]["value"];
	}

	public function updateDataParam($dataArray){
		$this->db->set('name', "'" . $dataArray['name'] . "'", FALSE);
		$this->db->set('value', "'" . $dataArray['value'] . "'", FALSE);
		$this->db->set('keterangan', "'" . $dataArray['keterangan'] . "'", FALSE);
		$this->db->where('id', $dataArray['id']);
		$this->db->update('param');
	}

	public function deleteParam($id){
		$this->db->delete(
			'param', 
			array(
				'id' => $id
			)
		);
	}

}
