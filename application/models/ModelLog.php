<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ModelLog extends CI_Model {

	public function __construct()
    {

    }

	////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////// FUNCTION /////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////

	public function getLogData(){
		$query = $this->db->query(
			"SELECT 
			b.username as user,
			a.datetimelog,
			a.action,
			a.keterangan
			FROM log a 
			LEFT JOIN user b ON b.id = a.user
			ORDER BY a.datetimelog DESC"
			, FALSE
		);
		return $query->result_array();
	}

	public function getDataByIdForEdit($id){
		$query = $this->db->get_where(
        	'log', 
        	array(
        		'id' => $id
        	)
        );
		return $query->result_array();
	}

	public function deleteLog($id){
		$this->db->delete(
			'log', 
			array(
				'id' => $id
			)
		);
	}

}
