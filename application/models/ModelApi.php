<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ModelApi extends CI_Model {

	public function __construct()
    {

    }

	public function getParameterRawBody(){
		return json_decode($this->input->raw_input_stream, true);
	}

    /*
		Apa kode bank dan pin yang mau login udah kesimpen di database apa belom
    */
	public function cekLogin($kodeBank, $pin){
		$query = $this->db->query(
			"
			CALL CekLogin('$kodeBank', '$pin')
			"
			, FALSE
		);
		$result = $query->result_array();

		//add this two line after procedure calls
		$query->next_result(); 
		$query->free_result(); 
		//end of new code

		return $result[0];
	}

    /*
		Apa kode bank dan pin yang mau login udah kesimpen di database apa belom
    */
	public function cekLoginId($kodeBank, $loginId){
		$query = $this->db->query(
			"
			CALL CekLoginId('$kodeBank', '$loginId')
			"
			, FALSE
		);
		$result = $query->result_array();

		//add this two line after procedure calls
		$query->next_result(); 
		$query->free_result(); 
		//end of new code

		return $result[0];
	}

    /*
		Tampilkan error parameter kurang atau tidak sesuai
    */
	public function getResponseMessage($errorCode){
		$query = $this->db->query(
			"
			SELECT 
			    response_code, response_message
			FROM
			    rfresponsecode
			WHERE
			    response_code = '$errorCode'
			"
			, FALSE
		);
		$result = $query->result_array();
		return $result[0];
	}

	// cek parameter key apakah sudah sesuai dengan yang dokumentasi apa gak.
    public function cekParameter($parameter, $arrayToCek){
    	$check = array();
		$finalResult = array();
    	foreach ($arrayToCek as $checkParams) {
    		foreach ($parameter as $key => $value) {
	    		if($checkParams == $key) {
	    			$check = true;
	    			break;
	    		} else {
	    			$check = false;
	    		}
	    	}
	    	if(!$check){
	    		$finalResult['responseCode'] = "08";
	    		$finalResult['responseMessage'] = $checkParams;
	    		return $finalResult;
	    	}
    	}
		$finalResult['responseCode'] = "00";
		return $finalResult;
    }

    // cek apakah kode bank dan login ID sesuai
    public function cekLoginIdApi($parameter){
    	$kodeBank = $parameter['kodebank'];
        $loginId = $parameter['loginid'];
        $cekLoginId = $this->cekLoginId($kodeBank, $loginId);
        return array(
            'responsecode' => $cekLoginId['response_code'],
            'responsemessage' => isset($cekLoginId['response_message']) ? $cekLoginId['response_message'] : ""
        );
    }

    public function changeResponseMessageFormat($message){
       	if(isset($message['response_code']))
       		$message['responsecode'] = $message['response_code'];
       	else if(isset($message['responsecode']))
       		$message['responsecode'] = $message['responsecode'];
       	else
       		$message['responsecode'] = "";

       	if(isset($message['response_message']))
       		$message['responsemessage'] = $message['response_message'];
       	else if(isset($message['responsemessage']))
       		$message['responsemessage'] = $message['responsemessage'];
       	else
       		$message['responsemessage'] = "";

        unset($message['response_code']);
        unset($message['response_message']);
        return $message;
    }


}
