<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ModelUjiDini extends CI_Model {

    public $dukcapilResponseTest;

	public function __construct()
    {
        $this->dukcapilResponseTest = '<?xml version="1.0" encoding="utf-8"?>
            <string xmlns="http://tempuri.org/">{"lastPage":"True","numberOfElements":"1","sort":"","totalElements":"1","firstPage":"True","number":"0","size":"1","content":[{"EKTP_STATUS":"","NO_KK":"5108062810090135","DUSUN":"-","NIK":"3276012509910003","NAMA_LGKP":"RENDY REIVALDY","KAB_NAME":"BULELENG","AGAMA":"HINDU","NAMA_LGKP_AYAH":"PUTU CERANA","NO_RW":"0","KEC_NAME":"BULELENG","JENIS_PKRJN":"KARYAWAN SWASTA","NO_RT":"1","NO_KEL":"1019","ALAMAT":"JLN. GEMPOL NO 114 BANYUNING TENGAH","NO_KEC":"6","TMPT_LHR":"BANYUNING","PDDK_AKH":"SLTP\/SEDERAJAT","NO_PROP":"51","STATUS_KAWIN":"KAWIN","NAMA_LGKP_IBU":"LUH NADI","PNYDNG_CCT":"","PROP_NAME":"BALI","NO_KAB":"8","KEL_NAME":"BANYUNING","JENIS_KLMIN":"Laki-Laki","TGL_LHR":"1980-07-05","RESPON":""}]}</string>';
        $this->load->model('ModelParam');
    }

    public function cekParameter($parameter){
    	if(
    		isset($parameter['nama_pemohon']) &&
            isset($parameter['pekerjaan']) &&
            isset($parameter['jenis_kelamin']) &&
            isset($parameter['nik']) &&
            isset($parameter['npwp_pemohon']) &&

            isset($parameter['penghasilan']) &&
            isset($parameter['nama_pasangan']) &&
            isset($parameter['nik_pasangan']) &&
            isset($parameter['no_sp3k']) &&
            isset($parameter['harga_rumah']) &&
            
            isset($parameter['uang_muka']) &&
            isset($parameter['sbum']) &&
            isset($parameter['nilai_kpr']) &&
            isset($parameter['suku_bunga']) &&
            isset($parameter['tenor_bulan']) &&
            
            isset($parameter['angsuran']) &&
            isset($parameter['nilai_subsidi']) &&
            isset($parameter['nama_pengembang']) &&
            isset($parameter['npwp_pengembang']) &&
            isset($parameter['nama_perumahan']) &&
            
            isset($parameter['alamat_agunan']) &&
            isset($parameter['kota_agunan']) &&
            isset($parameter['provinsi']) &&
            isset($parameter['kode_adm_wilayah']) &&
            isset($parameter['kode_pos_agunan']) &&
            
            isset($parameter['luas_tanah']) &&
            isset($parameter['luas_bangunan']) &&
            isset($parameter['kodebank']) &&
            isset($parameter['loginid'])
    	){
    		return true;
    	} else {
    		return false;
    	}
    }

    public function ujiDiniStartFlow($parameter){
        $resultSukses = $this->ModelApi->getResponseMessage('00');
        $parameter['responseMessage'] = $resultSukses['response_message'];
        $parameter['responseCode'] = $resultSukses['response_code'];
        /*
            start insert uji, insert ke table data_uji dan data_uji_history
            return response_code, response_message, id_uji -> jika berhasil
            jika gagal? gatau, soalnya ga dijagain. default responsenya response berhasil
        */
        $resultInsertUji = $this->executeInsertUji($parameter);
        if(isset($resultInsertUji['response_code']) && $resultInsertUji['response_code'] != "00"){
            return $resultInsertUji;
        } 
        $parameter['id_uji'] = $resultInsertUji['id_uji'];

        /*
            Result uji berhasil, next step, start cek ketentuan.
            return response_code, response_message
        */
        $resultCekKetentuan = $this->executeCekKetentuan($parameter);
        if(isset($resultCekKetentuan['response_code']) && $resultCekKetentuan['response_code'] != "00") {
            $parameter = $this->getResponseCodeAndMessage($parameter, $resultCekKetentuan);
        } 

        /*
            Result CekKetentuan berhasil, next step, start cek dukcapil.
            return response_code, response_message
        */
        $dukcapilTargetHitApi = $this->ModelParam->getParameter('dukcapil_direct_hit');

        // pemohon
        $resultCekDukcapil = $this->executeCekDukcapil($parameter['nik'], $parameter['nama_pemohon'], $parameter['kodebank'], $dukcapilTargetHitApi, $parameter['id_uji'], $parameter['jenis_kelamin']);
        
        if(isset($resultCekDukcapil['response_code']) && $resultCekDukcapil['response_code'] != "00") {
            $parameter = $this->getResponseCodeAndMessage($parameter, $resultCekDukcapil);
        } 

        // cek hasil balikan dukcapil, pemohon udah nikah apa belom
        // kalo belom gausah cek data pasangan
        $isKawin = false;
        $resultDukcapilPemohon = $resultCekDukcapil['data'];
        if(strtoupper($resultDukcapilPemohon['kawin']) == strtoupper("KAWIN")) {
            $isKawin = true;
        }

        if($isKawin) {
            // pasangan
            $resultCekDukcapilPasangan = $this->executeCekDukcapil($parameter['nik_pasangan'], $parameter['nama_pasangan'], $parameter['kodebank'], $dukcapilTargetHitApi, $parameter['id_uji'], $this->getJenisKelaminPasangan($parameter['jenis_kelamin']));
            
            if(isset($resultCekDukcapilPasangan['response_code']) && $resultCekDukcapilPasangan['response_code'] != "00") {
                $parameter = $this->getResponseCodeAndMessage($parameter, $resultCekDukcapilPasangan);
            } 
        } else {
            // kenapa 14 yak ? pemohon sudah menikah tapi pasangan kosong ? lupa coy
            $resultCekDukcapilPasangan = $result = $this->ModelApi->getResponseMessage('14');
        }

        $parameter = $this->noKKCheck($resultCekDukcapil, $resultCekDukcapilPasangan, $parameter, $isKawin);

        // update result_dukcapil
        // nik pemohon dan pasangan harus dua duanya true / valid. kalo ada salah satu invalid, cek dukcapil dianggap gagal
        $this->updateResultAfterCall($parameter, $resultCekDukcapil, $resultCekDukcapilPasangan, 'result_dukcapil', $isKawin);

        /*
            Result dukcapil berhasil, next step, start cek subsidi.
            return response_code, response_message
        */

        // Pemohon
        $resultCekSubsidi = $this->executeCekSubsidiHitAPI($parameter);

        if(isset($resultCekSubsidi['response_code']) && $resultCekSubsidi['response_code'] != "00") {
            $parameter = $this->getResponseCodeAndMessage($parameter, $resultCekSubsidi);
        } 

        if($isKawin) {
            // Pasangan
            $resultCekSubsidiPasangan = $this->executeCekSubsidiHitAPI($parameter, true);   

            if(isset($resultCekSubsidiPasangan['response_code']) && $resultCekSubsidiPasangan['response_code'] != "00") {
                $parameter = $this->getResponseCodeAndMessage($parameter, $resultCekSubsidiPasangan);
            } 
        } else {
            $resultCekSubsidiPasangan = $result = $this->ModelApi->getResponseMessage('14');
        }

        // update result_subsidi dengan cek pemohon dan pasangan
        $this->updateResultAfterCall($parameter, $resultCekSubsidi, $resultCekSubsidiPasangan, 'result_subsidi', $isKawin);

        /*
            Result subsidi berhasil, next step, start cek sireng / pengembang.
            return response_code, response_message
        */

        $resultCekPengembang = $this->executeCekPengembangHitAPI($parameter);  

        if(isset($resultCekPengembang['response_code']) && $resultCekPengembang['response_code'] != "00") {
            $parameter = $this->getResponseCodeAndMessage($parameter, $resultCekPengembang);
        } 

        // requested by ridwan, bisa set tanggal uji manual by API parameter passed
        // key tanggal_uji
        // biar gampang nge trace nya mana yang data uji dan result uji nya yang pake manual set tanggal uji
        // liat aja jamnya, kalo manual dia 00:00:00
        $manualTanggalUji = $this->ModelParam->getParameter('manual_tgl_uji');
        if($manualTanggalUji == "1") {
            $this->updateTanggalUji($parameter);
        }

        return $this->getFinalResult($parameter);
    }

    public function executeInsertUji($parameter){
        $nama_pemohon = $this->db->escape($parameter['nama_pemohon']);
        $pekerjaan = $this->db->escape($parameter['pekerjaan']);
        $jenis_kelamin = $this->db->escape($parameter['jenis_kelamin']);
        $nik = $this->db->escape($parameter['nik']);
        $npwp_pemohon = $this->db->escape($parameter['npwp_pemohon']);

        $penghasilan = $this->db->escape($parameter['penghasilan']);
        $nama_pasangan = $this->db->escape($parameter['nama_pasangan']);
        $nik_pasangan = $this->db->escape($parameter['nik_pasangan']);
        $no_sp3k = $this->db->escape($parameter['no_sp3k']);
        $harga_rumah = $this->db->escape($parameter['harga_rumah']);
        
        $uang_muka = $this->db->escape($parameter['uang_muka']);
        $sbum = $this->db->escape($parameter['sbum']);
        $nilai_kpr = $this->db->escape($parameter['nilai_kpr']);
        $suku_bunga = $this->db->escape($parameter['suku_bunga']);
        $tenor_bulan = $this->db->escape($parameter['tenor_bulan']);
        
        $angsuran = $this->db->escape($parameter['angsuran']);
        $nilai_subsidi = $this->db->escape($parameter['nilai_subsidi']);
        $nama_pengembang = $this->db->escape($parameter['nama_pengembang']);
        $npwp_pengembang = $this->db->escape($parameter['npwp_pengembang']);
        $nama_perumahan = $this->db->escape($parameter['nama_perumahan']);
        
        $alamat_agunan = $this->db->escape($parameter['alamat_agunan']);
        $kota_agunan = $this->db->escape($parameter['kota_agunan']);
        $provinsi = $this->db->escape($parameter['provinsi']);
        $kode_adm_wilayah = $this->db->escape($parameter['kode_adm_wilayah']);
        $kode_pos_agunan = $this->db->escape($parameter['kode_pos_agunan']);
        
        $luas_tanah = $this->db->escape($parameter['luas_tanah']);
        $luas_bangunan = $this->db->escape($parameter['luas_bangunan']);
        $kodebank = $this->db->escape($parameter['kodebank']);

        $query = $this->db->query(
            "
            CALL InsertUji(
                $nama_pemohon,
                $pekerjaan,
                $jenis_kelamin,
                $nik,
                $npwp_pemohon,

                $penghasilan,
                $nama_pasangan,
                $nik_pasangan,
                $no_sp3k,
                $harga_rumah,

                $uang_muka,
                $sbum,
                $nilai_kpr,
                $suku_bunga,
                $tenor_bulan,

                $angsuran,
                $nilai_subsidi,
                $nama_pengembang,
                $npwp_pengembang,
                $nama_perumahan,

                $alamat_agunan,
                $kota_agunan,
                $provinsi,
                $kode_adm_wilayah,
                $kode_pos_agunan,

                $luas_tanah,
                $luas_bangunan,
                $kodebank
            )
            "
            , FALSE
        );
        $result = $query->result_array();

        //add this two line after procedure calls
        $query->next_result(); 
        $query->free_result(); 
        //end of new code

        return $result[0];
    }

    public function executeCekKetentuan($parameter){

        $id_uji = $parameter['id_uji'];
        $kodebank = $parameter['kodebank'];
        $nik = $parameter['nik'];
        $penghasilan = $parameter['penghasilan'];
        $kode_adm_wilayah = $parameter['kode_adm_wilayah'];

        $harga_rumah = $parameter['harga_rumah'];
        $uang_muka = $parameter['uang_muka'];
        $sbum = $parameter['sbum'];
        $nilai_kpr = $parameter['nilai_kpr'];
        $suku_bunga = $parameter['suku_bunga'];

        $nik_pasangan = $parameter['nik_pasangan'];

        $query = $this->db->query(
            "
            CALL CekKetentuan(
                '$id_uji',
                '$kodebank',
                '$nik',
                '$penghasilan',
                '$kode_adm_wilayah',

                '$harga_rumah',
                '$uang_muka',
                '$sbum',
                '$nilai_kpr',
                '$suku_bunga'
            )
            "
            , FALSE
        );
        $result = $query->result_array();

        //add this two line after procedure calls
        $query->next_result(); 
        $query->free_result(); 
        //end of new code

        return $result[0];
    }

    // $type : 0 -> api mas fitrah, 1 -> direct
    public function executeCekDukcapil($nik, $nama, $kodeBank, $type, $idUji, $jenisKelamin){
        $mockResponseDukcapil = $this->ModelParam->getParameter('dukcapil_mock_response');

        // buat kalo misalnya ada yang passing nik nya kosong,
        // contoh pemohon yang belom menikah, nik pasangannya kosong
        if($nik == ""){
            $dukcapilResult = array(
                "kosong" => true
            );
            $paramArray = array(
                "nik" => $nik,
                "nama" => $nama,
                "kodeBank" => $kodeBank,
                "idUji" => $idUji
            );
        } else if($type == 0) {
            $url = $this->ModelParam->getParameter('dukcapil_cek_url');
            $progname = $this->ModelParam->getParameter('dukcapil_progname');
            $hashcode = $this->ModelParam->getParameter('dukcapil_hashcode');
            $paramArray = array(
                "nik" => $nik,
                "progname" => $progname,
                "hashcode" => $hashcode,
            );
            if($mockResponseDukcapil == "0")
                $resultDukcapil = $this->utility->initCurlPost($paramArray, $url);
            else
                $resultDukcapil = $this->dukcapilResponseTest;

            $arrayParsedXml = $this->utility->parseXML($resultDukcapil);
            $dukcapilResult = json_decode($arrayParsedXml[0], true)['content'][0];

        } else {

            $url = $this->ModelParam->getParameter('dukcapil_url_direct');
            $user = $this->ModelParam->getParameter('dukcapil_user');
            $password = $this->ModelParam->getParameter('dukcapil_password');
            $ipAddress =  $this->ModelParam->getParameter('dukcapil_ip_address');
            
            $paramArray = json_encode(
                array(
                    "nik" => $nik,
                    "user_id" => $user,
                    "password" => $password,
                    "ip_address" => $ipAddress
                )
            );

            $header = array(
                "Content-Type: application/json",
                "Accept: application/json"
            );
            
            if($mockResponseDukcapil == "0") {
                $resultDukcapil = $this->utility->postBodyRawCurlCustomHeader($jsonData, $url, $header);
                $dukcapilResult = json_decode($resultDukcapil, true)['content'][0];
            } else {
                $resultDukcapil = $this->dukcapilResponseTest;
                $arrayParsedXml = $this->utility->parseXML($resultDukcapil);
                $dukcapilResult = json_decode($arrayParsedXml[0], true)['content'][0];
            }

        }

        /*
            <?xml version="1.0" encoding="utf-8"?>
            <string xmlns="http://tempuri.org/">{"lastPage":"True","numberOfElements":"1","sort":"","totalElements":"1","firstPage":"True","number":"0","size":"1","content":[{"EKTP_STATUS":"","NO_KK":"","DUSUN":"","NIK":"","NAMA_LGKP":"","KAB_NAME":"","AGAMA":"","NAMA_LGKP_AYAH":"","NO_RW":"","KEC_NAME":"","JENIS_PKRJN":"","NO_RT":"","NO_KEL":"","ALAMAT":"","NO_KEC":"","TMPT_LHR":"","PDDK_AKH":"","NO_PROP":"","STATUS_KAWIN":"","NAMA_LGKP_IBU":"","PNYDNG_CCT":"","PROP_NAME":"","NO_KAB":"","KEL_NAME":"","JENIS_KLMIN":"","TGL_LHR":"","RESPON":"Data Tidak Ditemukan"}]}</string>
        */

        // LOGGING khusus dukcapil, biar ketauan
        $logData = array(
            "parameter" => $paramArray,
            "response" => $dukcapilResult
        );
        $this->loggings->logDatabase($kodeBank, 'API Cek Dukcapil ' . $nama, json_encode($logData), $this);

        if(!is_array($dukcapilResult)){
            $result = $this->ModelApi->getResponseMessage('11');
            $result['response_message'] .= " (Cek DUKCAPIL)";
        } else if(isset($dukcapilResult['kosong'])){
            $result = $this->ModelApi->getResponseMessage('14');
        } else if(strtoupper($dukcapilResult['RESPON']) == strtoupper('Data Tidak Ditemukan')){
            $result = $this->ModelApi->getResponseMessage('05');
        } else if(!$this->jenisKelaminCheck($dukcapilResult['JENIS_KLMIN'], $jenisKelamin)){
            $result = $this->ModelApi->getResponseMessage('16');
        } else if (strtoupper($dukcapilResult['NAMA_LGKP']) != strtoupper($nama)){
            $result = $this->ModelApi->getResponseMessage('09');
        } else {
            $result = $this->ModelApi->getResponseMessage('00');
        }
        if(isset($result['response_code']) && $result['response_code'] != "00" && $result['response_code'] != "14") {
            $result['response_message'] .= ". (Error : " . $nama . ")";
        } 

        // masukin data result hasil dukcapil di return data nya
        // soalnya buat ngcek udah nikah apa belom
        $result['data'] = array(
            "kawin" => is_array($dukcapilResult) && isset($dukcapilResult['STATUS_KAWIN']) ? $dukcapilResult['STATUS_KAWIN'] : "",
            "noKk" => is_array($dukcapilResult) && isset($dukcapilResult['NO_KK']) ? $dukcapilResult['NO_KK'] : "",
            "jenisKelamin" => is_array($dukcapilResult) && isset($dukcapilResult['JENIS_KLMIN']) ? $dukcapilResult['JENIS_KLMIN'] : "",
        );
        return $result;
    }

    private function updateResultAfterCall($parameter, $resultPemohon, $resultpasangan, $columnName, $isKawin = true){
        $nik = $parameter['nik'];
        $kodeBank = $parameter['kodebank'];
        $idUji = $parameter['id_uji'];

        // print_r(array($resultPemohon, $resultpasangan));exit;

        if(!$isKawin &&
            (
                isset($resultPemohon['response_code']) && 
                $resultPemohon['response_code'] == "00"
            )
        ){
            $result = "Y";
        } else if(
                (
                    isset($resultPemohon['response_code']) && 
                    $resultPemohon['response_code'] != "00"
                )
                    ||
                (
                    isset($resultpasangan['response_code']) && 
                    $resultpasangan['response_code'] != "00"
                )
                    ||
                (
                    isset($parameter['responseCode']) && 
                    $parameter['responseCode'] != "00"
                )
            ) {
            $result = "N";
        } else {
            $result = "Y";
        }

        // update table result uji
        $query = $this->db->query(
            "
            UPDATE resultuji SET tanggaluji = NOW(), $columnName = '$result' where resultuji.nik = '$nik' AND resultuji.kodebank = '$kodeBank' AND resultuji.id_uji = '$idUji';
            "
            , FALSE
        );
    }

    public function executeCekSubsidi($parameter){
        $id_uji = $parameter['id_uji'];
        $kodebank = $parameter['kodebank'];
        $nik = $parameter['nik'];

        $query = $this->db->query(
            "
            CALL CekSubsidi(
                '$id_uji',
                '$kodebank',
                '$nik'
            )
            "
            , FALSE
        );
        $result = $query->result_array();

        //add this two line after procedure calls
        $query->next_result(); 
        $query->free_result(); 
        //end of new code

        return $result[0];
    }

    /*
        karena pasangan mau di cek juga, dan males modif2 aneh2 lagi di pecah2 kayak dukcapil, alhasil di gabung aja biar gampang
        tapi pake flag, buat bedain.
        kenapa dukcapil ga bgini aja yak ? 
        ah sama aja
    */
    public function executeCekSubsidiHitAPI($parameter, $isPasangan = false){

        $kodeBank = $parameter['kodebank'];
        $this->load->model('ModelApi');
        $nik = $isPasangan ? $parameter['nik_pasangan'] : $parameter['nik'];
        $urlCekSubsidi = $this->ModelParam->getParameter('ssb_ceksubsidi');
        $url = $urlCekSubsidi . $nik;
        $resultSubsidi = $this->utility->initCurl($url);
        $nama = $isPasangan ? $parameter['nama_pasangan'] : $parameter['nama_pemohon'];

        // LOGGING , biar ketauan
        $logData = array(
            "parameter" => $parameter,
            "response" => $resultSubsidi,
            "isPasangan" => $isPasangan,
            "url" => $url
        );
        $this->loggings->logDatabase($kodeBank, 'API Cek API SSB getdebitur_flpp ' . $nama, json_encode($logData), $this);

        $resultSubsidi = json_decode($resultSubsidi, true);

        $urlCekPupr = $this->ModelParam->getParameter('ssb_cekpupr');
        $url = $urlCekPupr . $nik;
        $resultPupr = $this->utility->initCurl($url);

        // LOGGING , biar ketauan
        $logData = array(
            "parameter" => $parameter,
            "response" => $resultPupr,
            "isPasangan" => $isPasangan,
            "url" => $url
        );
        $this->loggings->logDatabase($kodeBank, 'API Cek API SSB getdata_pupr ' . $nama, json_encode($logData), $this);

        $resultPupr = json_decode($resultPupr, true);

        // kalo misalnya a true, b true returnya harus false
        // kalo misalnya a true, b false returnnya harus false
        // kalo misalnya a false, b true returnnya harus false
        // kalo misalnya a false, b false returnnya harus true

        if(!$resultSubsidi['valid'] && is_array($resultSubsidi) && !$resultPupr['valid'] && is_array($resultPupr))  {
            $finalResult = $this->ModelApi->getResponseMessage('00');
        } else {
            if(!is_array($resultSubsidi) && !is_array($resultPupr)){
                $message = "Cek subsidi FLPP & PUPR";
            } else if(!is_array($resultSubsidi)){
                $message = "Cek subsidi FLPP";
            } else if (!is_array($resultPupr)){
                $message = "Cek subsidi PUPR";
            } else if ($resultSubsidi['valid'] && $resultPupr['valid']){
                $message = "Data terdaftar di Sistem FLPP dan PUPR";
            } else if ($resultSubsidi['valid']){
                $message = "Data terdaftar di Sistem FLPP";
            } else if ($resultPupr['valid']){
                $message = "Data terdaftar di Sistem PUPR";
            } else {
                $message = "-";
            }
            if(!is_array($resultSubsidi) || !is_array($resultPupr)){
                $responseCode = $this->ModelApi->getResponseMessage('11');
            } else {
                $responseCode = $this->ModelApi->getResponseMessage('06');
            }
            $responseCode['response_message'] .= " (" . $message . " - " . $nama . ")";
            $finalResult = $responseCode;
        }
        return $finalResult;

        /*
            cek subsidi
            {
                valid: false,
                message: "Data dengan nik 1271060901940002 tidak ada di data realisasi flpp"
            }

            {
                valid: true,
                message: ""
            }

            cek PUPR
            {
                valid: true,
                message: "Data tersedia",
                data: {
                    no_ktp: "6171023010780006",
                    nama: "BENTO ANDREAS MANAFE",
                    no_ktp_pasangan: "6171026702810006",
                    nama_pasangan: "RUT KOEHUAN",
                    no_npwp: "824416028701000",
                    tanggal_akad: "2018-01-29",
                    tenor: "240"
                }
            }

            {
                valid: false,
                message: "Data tidak tersedia",
                data: ""
            }

        */
    }

    // return true jika data sudah ada di data subsidi
    public function executeCekPengembangHitAPI($parameter){

        $this->load->model('ModelApi');
        $kodeBank = $parameter['kodebank'];
        $urlCekPengembang = $this->ModelParam->getParameter('ssb_cekpengembang');
        $resultPengembang = $this->utility->initCurl($urlCekPengembang . $parameter['npwp_pengembang']);

        // LOGGING , biar ketauan
        $logData = array(
            "parameter" => $parameter,
            "response" => $resultPengembang
        );
        $this->loggings->logDatabase($kodeBank, 'API Cek API SSB getdata_pengembang', json_encode($logData), $this);

        $resultPengembang = json_decode($resultPengembang, true);

        if(!is_array($resultPengembang)) {
            $responseCode = "11";
            $statusCekPengembang = "N";
        } else if(!$resultPengembang['valid']) {
            $responseCode = "07";
            $statusCekPengembang = "N";
        } else {
            $responseCode = "00";
            $statusCekPengembang = "Y";
        }

        // update result cek subsidi
        $nik = $parameter['nik'];
        $idUji = $parameter['id_uji'];
        $query = $this->db->query(
            "
            UPDATE resultuji SET tanggaluji = NOW(), result_pengembang = '$statusCekPengembang' where resultuji.nik = '$nik' AND resultuji.kodebank = '$kodeBank' AND resultuji.id_uji = '$idUji';
            "
            , FALSE
        );
        $response = $this->ModelApi->getResponseMessage($responseCode);
        if(!is_array($resultPengembang)) {
            $response['response_message'] .= " (Cek Pengembang)";
        }
        return $response;

        /*
            cek pengembang
            {
                valid: true,
                message: "",
                data: {
                    nama: "PT. KHAZANAH BABEL LESTARI",
                    npwp: "840699714304000",
                    noanggota: "02.01.2018.039",
                    asosiasi: "PI",
                    stcode: "1",
                    asosiasicode: "8"
                }
            }

            {
                valid: false,
                message: "NPWP Pengembang, Data Tidak Ada",
                data: null
            }

        */
    }

    public function executeCekPengembang($parameter){
        $id_uji = $parameter['id_uji'];
        $kodebank = $parameter['kodebank'];
        $nik = $parameter['nik'];
        $npwpPengembang = $parameter['npwp_pengembang'];

        $query = $this->db->query(
            "
            CALL CekPengembang(
                '$id_uji',
                '$kodebank',
                '$nik',
                '$npwpPengembang'

            )
            "
            , FALSE
        );
        $result = $query->result_array();

        //add this two line after procedure calls
        $query->next_result(); 
        $query->free_result(); 
        //end of new code

        return $result[0];
    }

    public function getFinalResult($parameter){
        $id_uji = $parameter['id_uji'];

        $query = $this->db->query(
            "
            SELECT * FROM resultuji WHERE id_uji = '$id_uji'
            "
            , FALSE
        );
        $result = $query->result_array()[0];

        /*

        01 = Cek Ketentuan Penghasilan
        02 = Cek Nilai KPR
        03 = Cek Suku Bunga
        04 = Cek Harga Wilayah
        05 = Cek Dukcapil
        06 = Cek DataSubsidi (belum menerima subsidi)
        07 = Cek Pengembang di SIRENG


            Response Jika Sukses
            {
                "nik": "3287090909800011",
                "iduji": "2020042832870909098000111",
                "responsecode": "00",
                "responsemessage": "success",
                "responsedetails": [{
                    "fieldcek": "01",
                    "valid": "Y"
                }, {
                    "fieldcek": "02",
                    "valid": "Y"
                }, {
                    "fieldcek": "03",
                    "valid": "Y"
                }, {
                    "fieldcek": "04",
                    "valid": "Y"
                }, {
                    "fieldcek": "05",
                    "valid": "Y"
                }, {
                    "fieldcek": "06",
                    "valid": "Y"
                }, {
                    "fieldcek": "07",
                    "valid": "Y"
                }]
            }
        */

        return array(
            "nik" => $result['nik'],
            "iduji" => $result['id_uji'],
            "responsecode" => $parameter['responseCode'],
            "responsemessage" => $parameter['responseMessage'],
            "responsedetails" => array(
                array(
                    "fieldcek" => "01",
                    "valid" => $result['result_penghasilan']
                ),
                array(
                    "fieldcek" => "02",
                    "valid" => $result['result_nilaikpr']
                ),
                array(
                    "fieldcek" => "03",
                    "valid" => $result['result_sukubunga']
                ),
                array(
                    "fieldcek" => "04",
                    "valid" => $result['result_hargarumah']
                ),
                array(
                    "fieldcek" => "05",
                    "valid" => $result['result_dukcapil']
                ),
                array(
                    "fieldcek" => "06",
                    "valid" => $result['result_subsidi']
                ),
                array(
                    "fieldcek" => "07",
                    "valid" => $result['result_pengembang']
                )
            )   
        );
    }

    public function getResponseCodeAndMessage($parameter, $result){
        if($parameter['responseCode'] == "00") {
            $parameter['responseMessage'] = $result['response_message'];
            $parameter['responseCode'] = $result['response_code'];
        }
        return $parameter;
    }

    // return true jika sesuai dan sama
    public function jenisKelaminCheck($dataResultDukcapilJenisKelamin, $passedJenisKelamin){
        if($dataResultDukcapilJenisKelamin == "") return true;
        $resultConvert = $dataResultDukcapilJenisKelamin == "Laki-Laki" ? "L" : "P";
        if($passedJenisKelamin == $resultConvert)
            return true;
        return false;
    }

    // cuman ngereverse aja, kalo pemohon L, berarti pasangannya P. kalo pemohon P, pasangannya L
    // indonesia belom boleh pernikahan sesama jenis kan ?
    public function getJenisKelaminPasangan($jenisKelaminPemohon){
        return $jenisKelaminPemohon != "" ? ($jenisKelaminPemohon == "L" ? "P" : "L") : "";
    }

    // cek no KK pemohon sama pasangannya sama gak
    public function noKKCheck($resultDukcapilPemohon, $resultDukcpilPasangan, $parameter, $isKawin){

        if($isKawin && 
            isset($resultDukcapilPemohon['data']) &&
            isset($resultDukcpilPasangan['data']) &&
            ($resultDukcapilPemohon['data']['noKk'] != $resultDukcpilPasangan['data']['noKk']) 
        ) {
            $result = $this->ModelApi->getResponseMessage('17');
        } else {
            $result = $this->ModelApi->getResponseMessage('00');
        }
        $parameter = $this->getResponseCodeAndMessage($parameter, $result);
        return $parameter;
    }

    public function updateTanggalUji($parameter){
        if(!isset($parameter['tanggal_uji'])) return;

        $tglUjiManual = $parameter['tanggal_uji'];
        $idUji = $parameter['id_uji'];
        $kodeBank = $parameter['kodebank'];

        $this->db->query(
            "
            UPDATE data_uji SET created_at = '$tglUjiManual' where id_uji = '$idUji';
            "
            , FALSE
        );

        $this->db->query(
            "
            UPDATE data_uji_hist SET created_at = '$tglUjiManual' where id_uji = '$idUji';
            "
            , FALSE
        );

        $this->db->query(
            "
            UPDATE resultuji SET tanggaluji = '$tglUjiManual' where id_uji = '$idUji';
            "
            , FALSE
        );

        $this->db->query(
            "
            UPDATE resultuji_hist SET tanggaluji = '$tglUjiManual' where id_uji = '$idUji';
            "
            , FALSE
        );

        // LOGGING khusus manual tglUji, biar ketauan
        $logData = array(
            "parameter" => $parameter,
            "idUji" => $idUji
        );
        $this->loggings->logDatabase($kodeBank, 'Manual Tanggal Uji', json_encode($logData), $this);
    }

}
