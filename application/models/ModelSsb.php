<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ModelSsb extends CI_Model {
	
	public function __construct()
    {

    }

	public function getMenuData(){
		$query = $this->db->query(
			"SELECT 
			a.id,
			a.name,
			a.icon,
			CONCAT(b.name, ' - ', c.role) AS id_parent,
			a.url,
			c.role AS user_role,
			CASE WHEN a.active = 1 THEN 'Yes' ELSE 'No' END as active
			FROM menu a 
			LEFT JOIN menu b ON a.id_parent = b.id
			JOIN user_role c on a.user_role = c.id"
			, FALSE
		);
		return $query->result_array();
	}

	public function getIdParentForDropDown(){
		$query = $this->db->query(
			"SELECT 
			a.id,
			CONCAT(a.name , ' - ' , b.role) AS name,
			a.icon,
			a.id_parent,
			a.url,
			a.user_role,
			a.active
			FROM menu a
			LEFT JOIN user_role b on a.user_role = b.id
			WHERE a.id_parent = 0"
			, FALSE
		);
		$arrayData = $query->result_array();
		array_unshift($arrayData, "");
		return $arrayData;
	}

	public function createMenu($dataArray){
		$dataArray['name'] = $dataArray['nama'];
		$dataArray['id_parent'] = $dataArray['idparent'];
		$dataArray['user_role'] = $dataArray['role'];
		$this->manualConstructObject($dataArray);
		$this->saveObjectToDatabase();
	}

	public function saveObjectToDatabase(){
		$data = array(
			'name' => $this->name,
			'icon' => $this->icon,
			'id_parent' => $this->id_parent,
			'url' => $this->url,
			'user_role' => $this->user_role,
			'active' => $this->active
		);
		return $this->db->insert('menu', $data);
	}

	public function getDataByIdForEdit($idUji, $tableName){
		$query = $this->db->get_where(
        	$tableName, 
        	array(
        		'id_uji' => $idUji
        	)
        );
		return $query->result_array();
	}

	public function updateDataMenu($dataArray){
		$this->db->set('name', "'" . $dataArray['nama'] . "'", FALSE);
		$this->db->set('icon', "'" . $dataArray['icon'] . "'", FALSE);
		$this->db->set('id_parent', $dataArray['idparent'], FALSE);
		$this->db->set('url', "'" . $dataArray['url'] . "'", FALSE);
		$this->db->set('user_role', $dataArray['role'], FALSE);
		$this->db->set('active', $dataArray['active'], FALSE);
		$this->db->where('id', $dataArray['id']);
		$this->db->update('menu');
	}

	public function deleteMenu($id){
		$this->db->delete(
			'menu', 
			array(
				'id' => $id
			)
		);
	}

	public function getRoleForDropDown(){
		// $query = $this->db->query(
		// 	"SELECT 
		// 	*
		// 	FROM user_role 
		// 	WHERE id != 1"
		// 	, FALSE
		// );
		$query = $this->db->get('user_role');
		return $query->result_array();
	}

}
