<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ModelUser extends CI_Model {

	// fields in user table
	public $id;
	public $username;
	public $password;
	public $id_role;
	public $nama;

	public $is_blocked;
	public $login_trial;
	public $last_login_date;
	public $created_by;
	public $updated_by;

	public $islogin;
	public $lastactiontime;
	public $session;
	
	public function __construct()
    {
    	
    }

	public function manualConstructObject($dataArray){
	    $this->id = isset($dataArray['id']) ? $dataArray['id'] : null;
	    $this->username = isset($dataArray['username']) ? $dataArray['username'] : null;
	    $this->password = isset($dataArray['password']) ? $dataArray['password'] : null;
	    $this->id_role = isset($dataArray['id_role']) ? $dataArray['id_role'] : null;
	    $this->nama = isset($dataArray['nama']) ? $dataArray['nama'] : null;

	    $this->is_blocked = isset($dataArray['is_blocked']) ? $dataArray['is_blocked'] : null;
	    $this->login_trial = isset($dataArray['login_trial']) ? $dataArray['login_trial'] : null;
	    $this->last_login_date = isset($dataArray['last_login_date']) ? $dataArray['last_login_date'] : null;
	    $this->created_by = isset($dataArray['created_by']) ? $dataArray['created_by'] : null;
	    $this->updated_by = isset($dataArray['updated_by']) ? $dataArray['updated_by'] : null;

	    $this->islogin = isset($dataArray['islogin']) ? $dataArray['islogin'] : null;
	    $this->lastactiontime = isset($dataArray['lastactiontime']) ? $dataArray['lastactiontime'] : null;
	    $this->session = isset($dataArray['session']) ? $dataArray['session'] : null;
	}

	public function objectToString(){
		return
			"id : " . $this->id .
			" | username : " . $this->username .
			" | password : " . $this->password .
			" | id_role : " . $this->id_role .
			" | nama : " . $this->nama .
			" | is_blocked : " . $this->is_blocked .
			" | login_trial : " . $this->login_trial .
			" | last_login_date : " . $this->last_login_date .
			" | created_by : " . $this->created_by .
			" | updated_by : " . $this->updated_by .
			" | islogin : " . $this->islogin .
			" | lastactiontime : " . $this->lastactiontime .
			" | session : " . $this->session;

	}

	// get function

	public function getId(){
		return $this->id;
	}

	public function getUsername(){
		return $this->username;
	}

	public function getPassword(){
		return $this->password;
	}

	public function getIdRole(){
		return $this->id_role;
	}

	public function getNama(){
		return $this->nama;
	}

	public function getIsBlocked(){
		return $this->is_blocked;
	}

	public function getLoginTrial(){
		return $this->login_trial;
	}

	public function getLastLoginDate(){
		return $this->last_login_date;
	}

	public function getCreatedBy(){
		return $this->created_by;
	}

	public function getUpdatedBy(){
		return $this->updated_by;
	}

	public function getIsLogin(){
		return $this->islogin;
	}

	public function getLastTransaction(){
		return $this->lastactiontime;
	}

	public function getSession(){
		return $this->session;
	}

	// set function

	public function setId($data){
		$this->id = $data;
	}

	public function setUsername($data){
		$this->username = $data;
	}

	public function setPassword($data){
		$this->password = $data;
	}

	public function setIdRole($data){
		$this->id_role = $data;
	}

	public function setNama($data){
		$this->nama = $data;
	}

	public function setIsBlocked($data){
		$this->is_blocked = $data;
	}

	public function setLoginTrial($data){
		$this->login_trial = $data;
	}

	public function setLastLoginDate($data){
		$this->last_login_date = $data;
	}

	public function setCreatedBy($data){
		$this->created_by = $data;
	}

	public function setUpdatedBy($data){
		$this->updated_by = $data;
	}

	public function setIsLogin($data){
		$this->islogin = $data;
	}

	public function setLastActionTime($data){
		$this->lastactiontime = $data;
	}

	public function setSession($data){
		$this->session = $data;
	}

	////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////// UNIT TESTING /////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////

	public function startUnitTestingLogin($object){
		$this->unitTestingFormNotComplete($object);
		$this->unitTestingLoginUserNotFound($object);
		$this->unitTestingGetDataUser($object);
		$this->unitTestingLogin($object);
		$this->unitTestingLoginUserPendingOrBlocked($object);
		$this->unitTestingSuccessLogin($object);
		echo $this->unit->report();
		exit;
	}

	public function unitTestingFormNotComplete($object){
		$dataArray = array();
		$dataArray['username'] = '';
		$dataArray['password'] = '';
		$this->manualConstructObject($dataArray);

		$test = $this->checkIsFormLoginValid($dataArray);
		$expected_result = array();

		$expected_result['status'] = false;
		$expected_result['message'] = 'Username dan password tidak boleh kosong';

		// $expected_result['status'] = false;
		// $expected_result['message'] = 'username tidak boleh kosong';

		// $expected_result['status'] = false;
		// $expected_result['message'] = 'password tidak boleh kosong';

		$object->run($test, $expected_result, 'Login Test form not complete', json_encode($test));
	}

	public function unitTestingLoginUserNotFound($object){
		$dataArray = array();
		$dataArray['username'] = 'rdy1';
		$dataArray['password'] = '1';
		$this->manualConstructObject($dataArray);

		$test = $this->checkUserIsExist()['status'];

		$expected_result = false;

		// $expected_result = true;

		$object->run($test, $expected_result, 'Login Test user not found', json_encode($test));
	}

	public function unitTestingGetDataUser($object){
		$dataArray = array();
		$dataArray['username'] = 'rdy';
		$dataArray['password'] = '1';
		$dataArray['password'] = hash('sha512', $dataArray['password']);
		$this->manualConstructObject($dataArray);
		$data = $this->getDataUser();

        $checkIdRole = $this->id_role == '1' ? true : false;
        $checkNama = $this->nama == 'Rendy Reivaldy' ? true : false;
        $checkId = $this->id == '1' ? true : false;
		$test = $checkIdRole && $checkNama && $checkId;

		// $expected_result = false;
		$expected_result = true;

		$object->run($test, $expected_result, 'Login Test Get data user', json_encode($data));
	}

	public function unitTestingLoginUserPendingOrBlocked($object){
		$dataArray = array();
		$dataArray['username'] = 'test';
		$dataArray['password'] = 'test';
		$dataArray['password'] =  hash('sha512', $dataArray['password']);
		$this->manualConstructObject($dataArray);

		$dataUser = $this->getDataUser();
		$test = $this->checkUserIsPendingOrBlocked($dataUser);
		$expected_result = array();

		$expected_result['status'] = true;
		$expected_result['message'] = 'User masih dalam status pending dan belum di approve';

		// $expected_result['status'] = true;
		// $expected_result['message'] = 'User terkunci. Lapor kepada sistem administrator untuk melanjutkan';

		// $expected_result['status'] = false;		

		$object->run($test, $expected_result, 'Login Test User Pending or Blocked', json_encode($test));
	}

	public function unitTestingLogin($object){
		$dataArray = array();
		$dataArray['username'] = 'rdy';
		$dataArray['password'] = '1';
		$this->manualConstructObject($dataArray);

		$test = $this->checkLogin($dataArray);
		$expected_result = array();

		$expected_result['status'] = true;

		// $expected_result['status'] = false;
		// $expected_result['message'] = 'Username atau Password salah';

		// $expected_result['status'] = false;
		// $expected_result['message'] = 'User tidak terdaftar';

		// $expected_result['status'] = false;
		// $expected_result['message'] = 'Username dan password tidak boleh kosong';

		// $expected_result['status'] = false;
		// $expected_result['message'] = 'username tidak boleh kosong';

		// $expected_result['status'] = false;
		// $expected_result['message'] = 'password tidak boleh kosong';

		// $expected_result['status'] = true;
		// $expected_result['message'] = 'User masih dalam status pending dan belum di approve';

		// $expected_result['status'] = true;
		// $expected_result['message'] = 'User terkunci. Lapor kepada sistem administrator untuk melanjutkan';

		// $expected_result['status'] = false;		

		$object->run($test, $expected_result, 'Login Test', json_encode($test));
	}

	public function unitTestingSuccessLogin($object){
		$dataArray = array();
		$dataArray['username'] = 'rdy';
		$dataArray['password'] = '1';
		$this->manualConstructObject($dataArray);

		$test = $this->checkLogin($dataArray);
		if($test)
			$this->successLogin();

        $checkIden = $_SESSION['iden'] == 'rdy' ? true : false;
        $checkRole = $_SESSION['role'] == '1' ? true : false;
        $checkNama = $_SESSION['nama'] == 'Rendy Reivaldy' ? true : false;
        $checkId = $_SESSION['id'] == '1' ? true : false;
		$test = $checkIden && $checkRole && $checkNama && $checkId;

		// $expected_result = false;

		$expected_result = true;

		$object->run($test, $expected_result, 'Login Test success login', json_encode($test));
	}

	public function checkMenuRole($menu, $object){

		// INI GAK KEPAKE KARENA QUERYNYA ERROR DI MENU USER ROLE. NAMA FILE SAMA NAMA MENU NYA
		// GA SAMA.
		/*$query = $this->db->get_where(
        	'menu', 
        	array(
        		'user_role' => $object->session->userdata('role'),
        		'name' => $menu
        	)
        );*/

		$this->db->where('user_role', $object->session->userdata('role'));
        $this->db->like('url', $menu);
        $query = $this->db->get('menu');

		$result = $query->result_array();
		if(count($result) > 0)
			return true;
		else
			return false;
	}

	////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////// LOGIN  /////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////

	public function registration($dataArray){

		// $dataArray['username'] = $dataArray['username'];
		$dataArray['password'] = hash('sha512',$dataArray['password']);
		// $dataArray['nama'] = $dataArray['nama'];

		$dataArray['is_blocked'] = 0;
		$dataArray['login_trial'] = 0;
		$dataArray['created_by'] = 0;
		$dataArray['islogin'] = 0;
		$dataArray['id_role'] = "P";
		$dataArray['lastactiontime'] = Utility::getCurrentDateTimeWithFormat("Y-m-d H:i:s");

		$this->manualConstructObject($dataArray);
		$this->saveObjectToDatabase();
	}

	public function saveObjectToDatabase(){
		$data = array(
			'username' => $this->username,
			'password' => $this->password,
			'nama' => $this->nama,
			'is_blocked' => $this->is_blocked,
			'login_trial' => $this->login_trial,
			'created_by' => $this->created_by,
			'islogin' => $this->islogin,
			'id_role' => $this->id_role,
			'lastactiontime' => $this->lastactiontime
		);
		return $this->db->insert('user', $data);
	}

	public function checkLogin($dataArray){
		$this->manualConstructObject($dataArray);
		
		// LOGGING DATA
		$this->loggings->logDatabase('', 'trial login', 'username : ' . $dataArray['username'], $this);

		$this->password = hash('sha512', $this->password);
		$response = array();

		$checkForm = $this->checkIsFormLoginValid();
		if($checkForm['status']){
			$dataUserOnly = $this->checkUserIsExist();
			if($dataUserOnly['status']){
				if($this->ModelParam->getParameter('login_ldap') == "1"){
	        		if($this->utility->loginLDAP($this->username, $dataArray['password'])){
	        			$this->setDataUserIfLDAP($dataUserOnly['data']);
						$response['status'] = true;
						$response['landingpage'] = $this->getLandingPage($this->id_role);
	        		} else {
						$response['status'] = false;
						$response['message'] = 'Username atau Password salah';
	        		}
	        	} else {
					$dataUser = $this->getDataUser();
					$checkPendingOrBlocked = $this->checkUserIsPendingOrBlocked($dataUser, $dataUserOnly);
					if($checkPendingOrBlocked['status']){
						$response = $checkPendingOrBlocked;
						$response['status'] = false;
					} else {
				        if(count($dataUser) > 0){
							$response['status'] = true;
							$response['id'] = $this->id;
							$response['landingpage'] = $this->getLandingPage($this->id_role);
				        } else {
							$response['status'] = false;
							$response['message'] = 'Username atau Password salah';
							$this->incrementFalseLogin();
						}
					}
				}
			} else {
				$response['status'] = false;
				$response['message'] = 'User tidak terdaftar';
			}
		} else {
			$response = $checkForm;
		}
        return $response;
	}

	public function checkUserIsExist(){
		$query = $this->db->get_where(
        	'user', 
        	array(
        		'username' => $this->username
        	)
        );
		$result = $query->result_array();
        if(count($result) > 0)
        	return array('status' => true, 'data' => $result);
        else
        	return array('status' => false);
	}

	public function checkUserIsPendingOrBlocked($dataUser, $dataUserOnly){
		$response = array();
		$response['status'] = false;
	    if(count($dataUserOnly) > 0 && $dataUserOnly['data'][0]['is_blocked'] == 1){
    		$response['status'] = true;
			$response['message'] = 'User terkunci. Lapor kepada sistem administrator untuk melanjutkan';
	    }
		if(count($dataUser) > 0 && $dataUser[0]['id_role'] == 0) {
			$response['status'] = true;
			$response['message'] = 'User masih dalam status pending dan belum di approve';
	    }
	    return $response;
	}

	public function getDataUser(){
		$query = $this->db->get_where(
        	'user', 
        	array(
        		'username' => $this->username, 
        		'password' => $this->password
        	)
        );
        $finalResult = $query->result_array();
        if(count($finalResult) > 0){
	        $this->id_role = $finalResult[0]['id_role'];
	        $this->nama = $finalResult[0]['nama'];
	        $this->id = $finalResult[0]['id'];
        } else {
        	$this->id_role = 0;
	        $this->nama = '';
	        $this->id = 0;
    	}
		return $finalResult;
	}

	public function setDataUserIfLDAP($dataArray){
        if(count($dataArray) > 0){
	        $this->id_role = $dataArray[0]['id_role'];
	        $this->nama = $dataArray[0]['nama'];
	        $this->id = $dataArray[0]['id'];
        } else {
        	$this->id_role = 0;
	        $this->nama = '';
	        $this->id = 0;
    	}
	}

	public function checkIsFormLoginValid(){
		$response = array();
		$emptyStringHashed = hash('sha512', '');
		if ($this->password == $emptyStringHashed && $this->username == ''){
			$response['status'] = false;
			$response['message'] = 'Username dan password tidak boleh kosong';
		} else if($this->username == ''){
			$response['status'] = false;
			$response['message'] = 'username tidak boleh kosong';
		} else if ($this->password == $emptyStringHashed){
			$response['status'] = false;
			$response['message'] = 'password tidak boleh kosong';
		} else {
			$response['status'] = true;
		}
		return $response;
	}

	public function checkUserLoginiLoan($dataArray, $type){

		$arrayResult = $this->checkiLoanCoreAccess($dataArray, $type);
		if(!$arrayResult['status'] && $arrayResult['message'] == 'cekiloan') {
			return $this->checkUserLoginiLoanAfterCore($dataArray, $type);
		} else {
			return $arrayResult;
		} 

	}

	public function checkUserLoginiLoanAfterCore($dataArray, $type){
		$username = $dataArray['username'];
		$password = $dataArray['password'];
		$finalArrayResult = array();
		$conn = $this->mssqlutility->connectOdbc("login");
		if($type == "login"){
			$query = "
				SELECT 
				USERID, 
				GROUPID, 
				SU_FULLNAME, 
				SU_NIP, 
				SU_UPLINER, 
				SU_BRANCH_CODE, 
				SU_JABATAN, 
				SU_BRANCH_PROC_CODE 
				FROM SCALLUSER 
				WHERE USERID = '$username' 
				AND SU_PWD = CONVERT(VARCHAR(50), HashBytes('SHA1','$password'), 2)";
		} else if($type == "api"){
			$query = "
				SELECT 
				USERID, 
				GROUPID, 
				SU_FULLNAME, 
				SU_NIP, 
				SU_UPLINER, 
				SU_BRANCH_CODE, 
				SU_JABATAN, 
				SU_BRANCH_PROC_CODE 
				FROM SCALLUSER 
				WHERE USERID = '$username'";
		}
		
		$resultUser = $this->mssqlutility->odbcExec($query, $conn);

		if($type == "login"){

			if(!count($resultUser) > 0){
				// $finalArrayResult['status'] = false;
				// $finalArrayResult['message'] = 'Periksa kembali user atau password iLoan Anda';

				$finalArrayResult = $this->checkUserLoginiCollAfteriLoan($dataArray, $type);

			} else if(!$this->getLoginCheckAllowed($resultUser[0]['GROUPID'])){
				$finalArrayResult['status'] = false;
				$finalArrayResult['message'] = 'User tidak memiliki wewenang menggunakan aplikasi ini';
			} else {
				$finalArrayResult['status'] = true;
				$this->nama = $resultUser[0]['SU_FULLNAME'];
				$this->branch = $resultUser[0]['SU_BRANCH_CODE'];
				// $finalArrayResult['datauser'] = $resultUser[0];
				$finalArrayResult['landingpage'] = $this->redirectByiLoanGroupId($resultUser[0]['GROUPID']);
			}
		} else if ($type == "api") {
			if(!count($resultUser) > 0){
				$finalArrayResult['status'] = false;
				$finalArrayResult['message'] = 'User tidak dikenal';
			} else {
				$finalArrayResult['status'] = true;
			}
		}

		$this->mssqlutility->closePDO($conn);
		return $finalArrayResult;
	}

	public function checkUserLoginiCollAfteriLoan($dataArray, $type){
		$username = $dataArray['username'];
		$password = $dataArray['password'];
		$finalArrayResult = array();
		$conn = $this->mssqlutility->connectOdbc("loginicoll");
		$query = "
				SELECT 
				USERID, 
				GROUPID, 
				SU_FULLNAME, 
				SU_NIP, 
				SU_UPLINER, 
				SU_BRANCH_CODE, 
				SU_JABATAN, 
				NULL as SU_BRANCH_PROC_CODE 
				FROM SCALLUSER 
				WHERE USERID = '$username' 
				AND SU_PWD = CONVERT(VARCHAR(50), HashBytes('SHA1','$password'), 2)";
		
		$resultUser = $this->mssqlutility->odbcExec($query, $conn);

		if($type == "loginicoll"){

			if(!count($resultUser) > 0){
				$finalArrayResult['status'] = false;
				$finalArrayResult['message'] = 'Periksa kembali user atau password Anda';
			} else if(!$this->getLoginCheckAllowed($resultUser[0]['GROUPID'])){
				$finalArrayResult['status'] = false;
				$finalArrayResult['message'] = 'User tidak memiliki wewenang menggunakan aplikasi ini';
			} else {
				$finalArrayResult['status'] = true;
				$this->nama = $resultUser[0]['SU_FULLNAME'];
				$this->branch = $resultUser[0]['SU_BRANCH_CODE'];
				// $finalArrayResult['datauser'] = $resultUser[0];
				$finalArrayResult['landingpage'] = $this->redirectByiLoanGroupId($resultUser[0]['GROUPID']);
			}
		} else if ($type == "api") {
			if(!count($resultUser) > 0){
				$finalArrayResult['status'] = false;
				$finalArrayResult['message'] = 'User tidak dikenal';
			} else {
				$finalArrayResult['status'] = true;
			}
		}

		$this->mssqlutility->closePDO($conn);
		return $finalArrayResult;
	}

	public function checkiLoanCoreAccess($dataArray, $type){
		$username = $dataArray['username'];

		// cek dulu ini kalo misalnya core access true, login pake ini, kalo false, jalanin login iLoan biasa
		$finalArrayResult = array();

		$stringquery = "SELECT a.USERID, a.GROUPID, a.SU_FULLNAME, a.SU_NIP, a.SU_UPLINER, a.SU_BRANCH_CODE, a.SU_JABATAN, a.SU_BRANCH_PROC_CODE, b.CORE_ACCESS
			FROM SCALLUSER a
			LEFT JOIN SCALLGROUP b ON a.GROUPID = b.GROUPID
			WHERE USERID = '" . $username . "'";

		$conn = $this->mssqlutility->connectOdbc("login");
		$resultUser = $this->mssqlutility->odbcExec($stringquery, $conn);

		if(!count($resultUser) > 0){
			// $finalArrayResult['status'] = false;
			// $finalArrayResult['message'] = 'User tidak ditemukan di sistem iLoan';

			$finalArrayResult = $this->checkiCollCoreAccess($dataArray, $type);

		} else if(!$this->getLoginCheckAllowed($resultUser[0]['GROUPID'])){
			$finalArrayResult['status'] = false;
			$finalArrayResult['message'] = 'User tidak memiliki wewenang menggunakan aplikasi ini';
		} else if ($resultUser[0]['CORE_ACCESS'] == "1"){	

			if($type == "login"){
				// hit ws login core
				$statusCoreCheck = array();
				$statusCoreCheck = $this->hitSoapLoginCore($dataArray);

				if($statusCoreCheck['status']) {

					$finalArrayResult['status'] = true;
					$this->nama = $resultUser[0]['SU_FULLNAME'];
					$this->branch = $resultUser[0]['SU_BRANCH_CODE'];
					// $finalArrayResult['datauser'] = $resultUser[0];
					$finalArrayResult['landingpage'] = $this->redirectByiLoanGroupId($resultUser[0]['GROUPID']);

				} else {
					$finalArrayResult = $statusCoreCheck;
				}
			} else if ($type == "api") {
				$finalArrayResult['status'] = true;
			}

			

		} else if ($resultUser[0]['CORE_ACCESS'] == "0") {

			$finalArrayResult['status'] = false;
			$finalArrayResult['message'] = 'cekiloan';

		}
		return $finalArrayResult;

	}

	public function checkiCollCoreAccess($dataArray, $type){
		$username = $dataArray['username'];

		// cek dulu ini kalo misalnya core access true, login pake ini, kalo false, jalanin login iLoan biasa
		$finalArrayResult = array();

		$stringquery = "SELECT a.USERID, a.GROUPID, a.SU_FULLNAME, a.SU_NIP, a.SU_UPLINER, a.SU_BRANCH_CODE, a.SU_JABATAN, NULL as SU_BRANCH_PROC_CODE, b.CORE_ACCESS
			FROM SCALLUSER a
			LEFT JOIN SCALLGROUP b ON a.GROUPID = b.GROUPID
			WHERE USERID = '" . $username . "'";

		$conn = $this->mssqlutility->connectOdbc("loginicoll");
		$resultUser = $this->mssqlutility->odbcExec($stringquery, $conn);

		if(!count($resultUser) > 0){
			$finalArrayResult['status'] = false;
			$finalArrayResult['message'] = 'User tidak ditemukan di sistem iLoan';
		} else if(!$this->getLoginCheckAllowed($resultUser[0]['GROUPID'])){
			$finalArrayResult['status'] = false;
			$finalArrayResult['message'] = 'User tidak memiliki wewenang menggunakan aplikasi ini';
		} else if ($resultUser[0]['CORE_ACCESS'] == "1"){	

			if($type == "login" || $type == "loginicoll"){
				// hit ws login core
				$statusCoreCheck = array();
				$statusCoreCheck = $this->hitSoapLoginCore($dataArray);

				if($statusCoreCheck['status']) {

					$finalArrayResult['status'] = true;
					$this->nama = $resultUser[0]['SU_FULLNAME'];
					$this->branch = $resultUser[0]['SU_BRANCH_CODE'];
					// $finalArrayResult['datauser'] = $resultUser[0];
					$finalArrayResult['landingpage'] = $this->redirectByiLoanGroupId($resultUser[0]['GROUPID']);

				} else {
					$finalArrayResult = $statusCoreCheck;
				}
			} else if ($type == "api") {
				$finalArrayResult['status'] = true;
			}

		} else if ($resultUser[0]['CORE_ACCESS'] == "0") {

			$finalArrayResult['status'] = false;
			$finalArrayResult['message'] = 'cekiloan';

		}
		return $finalArrayResult;

	}

	public function hitSoapLoginCore($dataArray){
		$username = $dataArray['username'];
		$password = $dataArray['password'];

		// Initialize WS with the WSDL
		$client = new SoapClient(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . 'konvenLoscrms.wsdl');
		// Set request params
		$params = array(
		  "userId" => $username,
		  "password" => $password,
		);

		// Invoke WS method (Function1) with the request params 
		// $response = $client->__soapCall("checkUserValidation", array($params));

		// print_r($response);exit;

		// anggep aja bisa deh tinggal ini doang nih
		return array(
			"status" => true
		);
	}

	public function successLogin($objectSession){
		$date = $this->utility->getCurrentDateTimeWithFormat("Y-m-d H:i:s");

		$this->db->set('last_login_date', "'" . $date . "'", FALSE);
		$this->db->set('login_trial', 0, FALSE);
		$this->db->set('islogin', 1, FALSE);
		$this->db->where('id', $this->id);
		$this->db->update('user'); // gives UPDATE mytable SET field = field+1 WHERE id = 2

		$data_session = array(
			'iden' => $this->username,
			'role' => $this->id_role,
			'nama' => $this->nama,
			'id' => $this->id,
			'status' => "login"
			);
 
		$objectSession->set_userdata($data_session);
		
		// LOGGING DATA
		$this->loggings->logDatabase($this->id, 'login', '', $this);
	}

	public function logOut($objectSession){

		// LOGGING DATA
		$this->loggings->logDatabase($objectSession->userdata('id'), 'logout', '', $this);

		$this->db->set('islogin', 0, FALSE);
		$this->db->where('id', $objectSession->userdata('id'));
		$this->db->update('user'); 

		$user_data = $objectSession->all_userdata();
        foreach ($user_data as $key => $value) {
            if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {
                $objectSession->unset_userdata($key);
            }
        }

		$objectSession->sess_destroy();
	}

	public function incrementFalseLogin(){
		$this->load->model('ModelParam');

		$query = $this->db->get_where(
        	'user', 
        	array(
        		'username' => $this->username
        	)
        );
        $finalResult = $query->result_array();
        $counter = $finalResult[0]['login_trial'];
        $isBlocked = $finalResult[0]['is_blocked'];

		if($this->ModelParam->getParameter('block_multiple_false_login') == "1"){
			if($this->ModelParam->getParameter('login_trial_max') == $counter && 
				$isBlocked != 1){
				$this->db->set('is_blocked', 1, FALSE);
				$this->db->set('login_trial', 0, FALSE);
				$this->db->where('username', $this->username);
				$this->db->update('user'); // gives UPDATE mytable SET field = field+1 WHERE id = 2	
				
				// LOGGING DATA
				$this->loggings->logDatabase($this->id, 'login blocked', '', $this);
			} else {
				$this->db->set('login_trial', "login_trial+1", FALSE);
				$this->db->where('username', $this->username);
				$this->db->update('user'); // gives UPDATE mytable SET field = field+1 WHERE id = 2	
			}
			
		}
	}

	public function getLandingPage($idRole){
		$query = $this->db->get_where(
        	'user_role', 
        	array(
        		'id' => $idRole
        	)
        );
		return $query->result_array();
	}

	////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////// USER ///////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////

	public function getUserData(){
		$query = $this->db->query(
			"SELECT 
			a.id,
			a.username,
			a.password,
			b.role AS id_role,
			a.nama,
			CASE WHEN a.is_blocked = 1 THEN 'Yes' ELSE 'No' END AS is_blocked,
			a.login_trial,
			a.last_login_date,
			c.username AS created_by,
			d.username AS updated_by,
			a.islogin,
			a.lastactiontime,
			a.session 
			FROM user a
			JOIN user_role b ON b.id = a.id_role
			LEFT JOIN user c ON c.id = a.created_by
			LEFT JOIN user d ON d.id = a.updated_by
			WHERE a.id_role != 1"
			, FALSE
		);
		return $query->result_array();
	}

	public function registrationInputAdmin($dataArray){
		$dataArray['password'] = hash('sha512',$dataArray['password']);
		$dataArray['is_blocked'] = $dataArray['isblocked'];
		$dataArray['login_trial'] = 0;
		$dataArray['created_by'] = $dataArray['createdby'];
		$dataArray['islogin'] = 0;
		$dataArray['id_role'] = $dataArray['role'];
		$dataArray['lastactiontime'] = null;

		$this->manualConstructObject($dataArray);
		$this->saveObjectToDatabase();
	}

	public function getDataByIdForEdit($id){
		$query = $this->db->get_where(
        	'user', 
        	array(
        		'id' => $id
        	)
        );
		return $query->result_array();
	}

	public function getRoleForDropDown(){
		$query = $this->db->query(
			"SELECT 
			*
			FROM user_role 
			WHERE id != 1"
			, FALSE
		);
		// $query = $this->db->get('user_role');
		return $query->result_array();
	}

	public function updateDataUser($dataArray, $objectSession){
		$this->db->set('username', "'" . $dataArray['username'] . "'", FALSE);
		$this->db->set('nama', "'" . $dataArray['nama'] . "'", FALSE);
		$this->db->set('id_role', $dataArray['role'], FALSE);
		$this->db->set('is_blocked', $dataArray['isblocked'], FALSE);
		$this->db->set('updated_by', $objectSession->userdata('id'), FALSE);
		$this->db->where('id', $dataArray['id']);
		$this->db->update('user');
	}

	public function deleteUser($id){
		$this->db->delete(
			'user', 
			array(
				'id' => $id
			)
		);
	}

	public function checkOldPassword($dataArray){
		$query = $this->db->get_where(
        	'user', 
        	array(
        		'id' => $dataArray['id'],
        		'password' => hash('sha512',$dataArray['oldpassword'])
        	)
        );
		$dataResult = $query->result_array();
		if(count($dataResult) > 0){
			return true;
		} else {
			return false;
		}
	}

	public function changePassword($dataArray){
		$this->db->set('password', "'" . hash('sha512',$dataArray['password']) . "'", FALSE);
		$this->db->where('id', $dataArray['id']);
		$this->db->update('user');
	}



	////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////// MOBILE API /////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////

	public function getUserApiMobile($id, $pass){
		$query = $this->db->get_where(
        	'user', 
        	array(
        		'id' => $id, 
        		'password' => hash('sha512',$pass),
        		'islogin' => 1
        	)
        );
        $finalResult = $query->result_array();
		return $finalResult;
	}

	public function successLoginApiMobile($id){
		$date = $this->utility->getCurrentDateTimeWithFormat("Y-m-d H:i:s");

		$this->db->set('last_login_date', "'" . $date . "'", FALSE);
		$this->db->set('login_trial', 0, FALSE);
		$this->db->set('islogin', 1, FALSE);
		$this->db->where('id', $id);
		$this->db->update('user'); // gives UPDATE mytable SET field = field+1 WHERE id = 2
		
	}

	public function logOutApiMobile($id){

		$this->db->set('islogin', 0, FALSE);
		$this->db->where('id', $id);
		$this->db->update('user'); 

	}

}
