<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ModelBatalUji extends CI_Model {

	public function __construct()
    {
        $this->load->model('ModelParam');
        $this->load->model('ModelParam');
    }

    public function executeBatalUji($parameter){
        $kodeBank = $this->db->escape($parameter['kodebank']);
        $nik = $this->db->escape($parameter['nik']);
        if($this->checkIsNikExist($parameter)){
            $this->db->query(
                "
                    DELETE FROM data_uji WHERE kodebank = $kodeBank AND nik = $nik
                "
                , FALSE
            );
            $this->db->query(
                "
                    DELETE FROM resultuji WHERE kodebank = $kodeBank AND nik = $nik
                "
                , FALSE
            );
            return true;
        } else {
            return false;
        }
    }

    public function checkIsNikExist($parameter){
        $kodeBank = $this->db->escape($parameter['kodebank']);
        $nik = $this->db->escape($parameter['nik']);
        $query = $this->db->query(
            "
            SELECT * FROM data_uji WHERE kodebank = $kodeBank AND nik = $nik
            "
            , FALSE
        );
        $resultDataUji = $query->result_array();
        if(count($resultDataUji) > 0){
            return true;
        }
        return false;
    }

}

?>