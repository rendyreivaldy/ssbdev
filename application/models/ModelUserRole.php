<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ModelUserRole extends CI_Model {

	// fields in menu table
	public $id;
	public $role;
	public $keterangan;
	public $landing_page;
	
	public function __construct()
    {

    }

	public function manualConstructObject($dataArray){
	    $this->id = isset($dataArray['id']) ? $dataArray['id'] : null;
	    $this->role = isset($dataArray['role']) ? $dataArray['role'] : null;
	    $this->keterangan = isset($dataArray['keterangan']) ? $dataArray['keterangan'] : null;
	    $this->landing_page = isset($dataArray['landing_page']) ? $dataArray['landing_page'] : null;
	}

	public function objectToString(){
		return
			"id : " . $this->id .
			" | role : " . $this->role .
			" | keterangan : " . $this->keterangan .
			" | landing_page : " . $this->landing_page;

	}

	// get function

	public function getId(){
		return $this->id;
	}

	public function getRole(){
		return $this->role;
	}

	public function getKeterangan(){
		return $this->keterangan;
	}

	public function getLandingPage(){
		return $this->landing_page;
	}

	// set function

	public function setId($data){
		$this->id = $data;
	}

	public function setRole(){
		$this->role = $data;
	}

	public function setKeterangan(){
		$this->keterangan = $data;
	}

	public function setLandingPage(){
		$this->landing_page = $data;
	}

	////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////// FUNCTION /////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////

	public function getUserRoleData(){
		$query = $this->db->query(
			"SELECT 
			*
			FROM user_role
			WHERE id != 1"
			, FALSE
		);
		return $query->result_array();
	}

	public function createUserRole($dataArray){
		$this->manualConstructObject($dataArray);
		$this->saveObjectToDatabase();
	}

	public function saveObjectToDatabase(){
		$data = array(
			'role' => $this->role,
			'keterangan' => $this->keterangan
		);
		return $this->db->insert('user_role', $data);
	}

	public function getDataByIdForEdit($id){
		$query = $this->db->get_where(
        	'user_role', 
        	array(
        		'id' => $id
        	)
        );
		return $query->result_array();
	}

	public function updateDataUserRole($dataArray){
		$this->db->set('role', "'" . $dataArray['role'] . "'", FALSE);
		$this->db->set('keterangan', "'" . $dataArray['keterangan'] . "'", FALSE);
		$this->db->set('landing_page', "'" . $dataArray['landingpage'] . "'", FALSE);
		$this->db->where('id', $dataArray['id']);
		$this->db->update('user_role');
	}

	public function deleteUserRole($id){
		$this->db->delete(
			'user_role', 
			array(
				'id' => $id
			)
		);
	}

}
