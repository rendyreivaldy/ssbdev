<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ModelUnitTest extends CI_Model {

	public function __construct()
    {
    }

    public function cleanUpDataForLog($data){
    	if(is_array($data))
    		return json_encode($data);
    	else
    		return $data;
    }

    public function formatNotes($result, $expectedResult){
    	return "Data compared : <br>=============================================<br>[" . 
			$this->cleanUpDataForLog($result) . 
			"]<br>=============================================<br>[" . 
			$this->cleanUpDataForLog($expectedResult) . "]";
    }
	 
}
