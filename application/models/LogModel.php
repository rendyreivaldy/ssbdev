<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LogModel extends CI_Model {


	public function logData($username, $action, $keterangan)
	{		
    	date_default_timezone_set("Asia/Jakarta");
		$date = $this->utility->getCurrentDateTimeWithFormat("Y-m-d H:i:s");
    	
		$this->dbutility->createNewRow(
			'log', 
			array(
				'user' => $username,
				'datetimelog' => $date,
				'action' => $action,
				'keterangan' => $keterangan
			), 
			$this->db
		);
	}
	 
}
