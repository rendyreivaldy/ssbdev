<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ModelDataUjiList extends CI_Model {
	
	public function __construct()
    {

    }

    public function getAjaxListDataUji($postData){
		$list = $this->get_datatables();
        $data = array();
        $no = $postData['start'];
        foreach ($list as $dataUji) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $dataUji->id_uji;
            $row[] = $dataUji->nama_pemohon;
            $row[] = $dataUji->nik;
            $row[] = $dataUji->kodebank;
 
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $postData['draw'],
                        "recordsTotal" => $this->count_all(),
                        "recordsFiltered" => $this->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        return $output;
	}	

	// AJAX PAGINATION YASS
	// ganti sesuai kebutuhan, join2nya gimana, trus pastiin jangan sampe muncul ambiguous
	// Datatable ajax pagination
	var $table = 'data_uji';

    private function _get_datatables_query()
    {
         
        //add custom filter here

	    $column_order = array(null, 'data_uji.nama_pemohon','data_uji.nik','data_uji.kodebank'); //set column field database for datatable orderable
	    $column_search = array('data_uji.nama_pemohon','data_uji.nik','data_uji.kodebank'); //set column field database for datatable searchable 
	    $order = array('created_at' => 'asc'); // default order 

    	$this->db->select(
    		'data_uji.id_uji,
    		data_uji.nama_pemohon,
			data_uji.nik,
			data_uji.kodebank'
		);
        $this->db->from('data_uji');

		if($this->input->post('nik'))
        {
            $this->db->where('data_uji.nik', $this->input->post('nik'));
        }
        if($this->input->post('nama_pemohon'))
        {
            $this->db->like('data_uji.nama_pemohon', $this->input->post('nama_pemohon'));
        }
        if($this->input->post('kodebank'))
        {
            $this->db->like('data_uji.kodebank', $this->input->post('kodebank'));
        }
    
        $i = 0;
     
        foreach ($column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($order))
        {
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    public function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->_get_datatables_query();
        return $this->db->count_all_results();
    }

}
