/*
SQLyog Community v8.71 
MySQL - 5.5.5-10.4.11-MariaDB : Database - ssb
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
USE `ssb`;

/*Table structure for table `data_uji` */

DROP TABLE IF EXISTS `data_uji`;

CREATE TABLE `data_uji` (
  `nama_pemohon` varchar(999) DEFAULT NULL,
  `pekerjaan` varchar(200) DEFAULT NULL,
  `jenis_kelamin` varchar(200) DEFAULT NULL,
  `nik` varchar(20) DEFAULT NULL,
  `npwp_pemohon` varchar(20) DEFAULT NULL,
  `penghasilan` float DEFAULT NULL,
  `nama_pasangan` varchar(200) DEFAULT NULL,
  `nik_pasangan` varchar(20) DEFAULT NULL,
  `no_sp3k` varchar(200) DEFAULT NULL,
  `harga_rumah` float DEFAULT NULL,
  `uang_muka` float DEFAULT NULL,
  `sbum` float DEFAULT NULL,
  `nilai_kpr` float DEFAULT NULL,
  `suku_bunga` varchar(10) DEFAULT NULL,
  `tenor_bulan` varchar(10) DEFAULT NULL,
  `angsuran` float DEFAULT NULL,
  `nilai_subsidi` float DEFAULT NULL,
  `nama_pengembang` varchar(200) DEFAULT NULL,
  `npwp_pengembang` varchar(20) DEFAULT NULL,
  `nama_perumahan` varchar(200) DEFAULT NULL,
  `alamat_agunan` varchar(200) DEFAULT NULL,
  `kota_agunan` varchar(20) DEFAULT NULL,
  `provinsi` varchar(20) DEFAULT NULL,
  `kode_adm_wilayah` varchar(20) DEFAULT NULL,
  `kode_pos_agunan` varchar(200) DEFAULT NULL,
  `luas_tanah` varchar(10) DEFAULT NULL,
  `luas_bangunan` varchar(10) DEFAULT NULL,
  `id_uji` varchar(10) NOT NULL,
  PRIMARY KEY (`id_uji`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `data_uji` */

/*Table structure for table `data_uji_hist` */

DROP TABLE IF EXISTS `data_uji_hist`;

CREATE TABLE `data_uji_hist` (
  `nama_pemohon` varchar(999) DEFAULT NULL,
  `pekerjaan` varchar(200) DEFAULT NULL,
  `jenis_kelamin` varchar(200) DEFAULT NULL,
  `nik` varchar(20) DEFAULT NULL,
  `npwp_pemohon` varchar(20) DEFAULT NULL,
  `penghasilan` float DEFAULT NULL,
  `nama_pasangan` varchar(200) DEFAULT NULL,
  `nik_pasangan` varchar(20) DEFAULT NULL,
  `no_sp3k` varchar(200) DEFAULT NULL,
  `harga_rumah` float DEFAULT NULL,
  `uang_muka` float DEFAULT NULL,
  `sbum` float DEFAULT NULL,
  `nilai_kpr` float DEFAULT NULL,
  `suku_bunga` varchar(10) DEFAULT NULL,
  `tenor_bulan` varchar(10) DEFAULT NULL,
  `angsuran` float DEFAULT NULL,
  `nilai_subsidi` float DEFAULT NULL,
  `nama_pengembang` varchar(200) DEFAULT NULL,
  `npwp_pengembang` varchar(20) DEFAULT NULL,
  `nama_perumahan` varchar(200) DEFAULT NULL,
  `alamat_agunan` varchar(200) DEFAULT NULL,
  `kota_agunan` varchar(20) DEFAULT NULL,
  `provinsi` varchar(20) DEFAULT NULL,
  `kode_adm_wilayah` varchar(20) DEFAULT NULL,
  `kode_pos_agunan` varchar(200) DEFAULT NULL,
  `luas_tanah` varchar(10) DEFAULT NULL,
  `luas_bangunan` varchar(10) DEFAULT NULL,
  `id_uji` varchar(10) NOT NULL,
  PRIMARY KEY (`id_uji`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `data_uji_hist` */

/*Table structure for table `resultuji` */

DROP TABLE IF EXISTS `resultuji`;

CREATE TABLE `resultuji` (
  `id_uji` varchar(10) NOT NULL,
  `kodebank` varchar(5) DEFAULT NULL,
  `nik` varchar(20) DEFAULT NULL,
  `tanggaluji` datetime DEFAULT NULL,
  `result_dukcapil` varchar(10) DEFAULT NULL,
  `result_hargarumah` varchar(10) DEFAULT NULL,
  `result_penghasilan` varchar(10) DEFAULT NULL,
  `result_pengembang` varchar(10) DEFAULT NULL,
  `result_nilaikpr` varchar(10) DEFAULT NULL,
  `result_sukubunga` varchar(10) DEFAULT NULL,
  `result_subsidi` varchar(10) DEFAULT NULL,
  `id_hasil_uji` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_uji`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `resultuji` */

/*Table structure for table `rflogin` */

DROP TABLE IF EXISTS `rflogin`;

CREATE TABLE `rflogin` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `kodebank` varchar(5) DEFAULT NULL,
  `pin` varchar(100) DEFAULT NULL,
  `loginid` varchar(999) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `rflogin` */

insert  into `rflogin`(`id`,`kodebank`,`pin`,`loginid`,`tanggal`) values (1,'200','123','CSVWqoVeSRCVhcvGaL1D','2020-04-27 00:00:00');

/*Table structure for table `rfpolicy` */

DROP TABLE IF EXISTS `rfpolicy`;

CREATE TABLE `rfpolicy` (
  `idpolicy` varchar(3) NOT NULL,
  `policy` varchar(200) DEFAULT NULL,
  `value` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`idpolicy`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `rfpolicy` */

insert  into `rfpolicy`(`idpolicy`,`policy`,`value`) values ('1','penghasilan','8000000'),('2','sukubunga','0.05');

/*Table structure for table `rfpriceindex` */

DROP TABLE IF EXISTS `rfpriceindex`;

CREATE TABLE `rfpriceindex` (
  `kodewilayah` varchar(200) NOT NULL,
  `harga` float DEFAULT NULL,
  PRIMARY KEY (`kodewilayah`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `rfpriceindex` */

/* Function  structure for function  `randomlogin` */

/*!50003 DROP FUNCTION IF EXISTS `randomlogin` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `randomlogin`() RETURNS varchar(128) CHARSET utf8mb4
BEGIN
SET @chars = 'aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPqQrRsStTuUvVwWxXyYzZ1234567890';
SET @charLen = LENGTH(@chars);
SET @randomPassword = '';
WHILE LENGTH(@randomPassword) < 20
    DO
    SET @randomPassword = CONCAT(@randomPassword, SUBSTRING(@chars,CEILING(RAND() * @charLen),1));
END WHILE;
RETURN @randomPassword ;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `CekKetentuan` */

/*!50003 DROP PROCEDURE IF EXISTS  `CekKetentuan` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `CekKetentuan`(iduji varchar(10),kodebank1 varchar(5), nik1 varchar(20),penghasilan1 float, kodeadmwilayah varchar(10), hargarumah float, uangmuka float,sbum1 float, nilaikpr float, sukubunga1 varchar(10))
BEGIN
	
	SET @hargarumah1 = (SELECT rfpriceindex.harga FROM rfpriceindex WHERE rfpriceindex.kodewilayah = kodeadmwilayah);
	SET @sukubunga = (SELECT rfpolicy.value FROM rfpolicy WHERE rfpolicy.idpolicy = '2');
	SET @penghasilan = (SELECT rfpolicy.VALUE FROM rfpolicy WHERE rfpolicy.idpolicy = '1');
	
	/* Cek Harga Rumah */	
	IF (hargarumah <= @hargarumah1) THEN
	SET @result_hargarumah = 'Y';
	ELSE
	SET @result_hargarumah = 'N';
	END IF;
	
	/* Cek Suku Bunga */	
	IF (sukubunga1 = @sukubunga) THEN
	SET @result_sukubunga = 'Y';
	ELSE
	SET @result_sukubunga = 'N';
	END IF;
	
	/* Cek Penghasilan */	
	IF (penghasilan1 <= @penghasilan) THEN
	SET @result_penghasilan = 'Y';
	ELSE
	SET @result_penghasilan = 'N';
	END IF;
	
	
	
	if exists (select 1 from resultuji where resultuji.nik = nik1 and resultuji.kodebank <> kodebank1 and datediff(NOW(),resultuji.tanggaluji) <= 30) then
	/*Sedang diajukan/Sudah diuji oleh Akun Bank Lain, Tunggu lewat 30 hari */		
	SELECT '03' ;
	else 
		if exists (SELECT 1 FROM resultuji WHERE resultuji.nik = nik1 AND resultuji.kodebank = kodebank1 and resultuji.id_uji = iduji) then
		UPDATE resultuji set tanggaluji = NOW(), result_hargarumah = @result_hargarumah, result_sukubunga = @result_sukubunga, result_penghasilan=@result_penghasilan  where resultuji.nik = nik1 AND resultuji.kodebank = kodebank1 AND resultuji.id_uji = iduji;
		select '00';	
		else	
		INSERT INTO resultuji (id_uji,kodebank,nik,tanggaluji,result_hargarumah,result_penghasilan,result_sukubunga) values (iduji,kodebank1,nik1,NOW(),@result_hargarumah,@result_penghasilan,@result_sukubunga);
		select '00';
		end if;
	end if;
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `CekLogin` */

/*!50003 DROP PROCEDURE IF EXISTS  `CekLogin` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `CekLogin`(kodebank1 VARCHAR(5),pin1 VARCHAR(100))
BEGIN
	if exists (select 1 from rflogin where rflogin.kodebank = kodebank1) then
		IF EXISTS (SELECT 1 FROM rflogin WHERE rflogin.pin = pin1) THEN
			SET @tanggal = (SELECT rflogin.tanggal FROM rflogin WHERE rflogin.kodebank = kodebank1 AND rflogin.pin = pin1);
			if (SELECT TO_DAYS(NOW())) = (SELECT TO_DAYS(@tanggal)) then
			SELECT loginid from rflogin where rflogin.kodebank = kodebank1 and rflogin.pin = pin1;
			else
			update rflogin set loginid = randomlogin() where rflogin.kodebank = kodebank1 and rflogin.pin = pin1;
			SELECT rflogin.loginid from rflogin WHERE rflogin.kodebank = kodebank1 AND rflogin.pin = pin1;
			end if;
		ELSE
		SELECT '02' ;
		END IF;
	else
	select '01' ;
	end if;
    END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
