/*
SQLyog Community v8.71 
MySQL - 5.5.5-10.4.11-MariaDB : Database - ssb
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`ssb` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `ssb`;

/*Table structure for table `resultuji` */

DROP TABLE IF EXISTS `resultuji`;

CREATE TABLE `resultuji` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `kodebank` varchar(5) DEFAULT NULL,
  `nik` varchar(20) DEFAULT NULL,
  `tanggaluji` datetime DEFAULT NULL,
  `result_dukcapil` varchar(10) DEFAULT NULL,
  `result_hargarumah` varchar(10) DEFAULT NULL,
  `result_penghasilan` varchar(10) DEFAULT NULL,
  `result_pengembang` varchar(10) DEFAULT NULL,
  `result_nilaikpr` varchar(10) DEFAULT NULL,
  `result_sukubunga` varchar(10) DEFAULT NULL,
  `result_subsidi` varchar(10) DEFAULT NULL,
  `id_hasil_uji` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `resultuji` */

/*Table structure for table `rflogin` */

DROP TABLE IF EXISTS `rflogin`;

CREATE TABLE `rflogin` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `kodebank` varchar(5) DEFAULT NULL,
  `pin` varchar(100) DEFAULT NULL,
  `loginid` varchar(999) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `rflogin` */

insert  into `rflogin`(`id`,`kodebank`,`pin`,`loginid`,`tanggal`) values (1,'200','123','CSVWqoVeSRCVhcvGaL1D','2020-04-27 00:00:00');

/*Table structure for table `rfpolicy` */

DROP TABLE IF EXISTS `rfpolicy`;

CREATE TABLE `rfpolicy` (
  `idpolicy` varchar(3) NOT NULL,
  `policy` varchar(200) DEFAULT NULL,
  `value` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`idpolicy`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `rfpolicy` */

/*Table structure for table `rfpriceindex` */

DROP TABLE IF EXISTS `rfpriceindex`;

CREATE TABLE `rfpriceindex` (
  `kodewilayah` varchar(200) NOT NULL,
  `harga` float DEFAULT NULL,
  PRIMARY KEY (`kodewilayah`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `rfpriceindex` */

/* Function  structure for function  `randomlogin` */

/*!50003 DROP FUNCTION IF EXISTS `randomlogin` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `randomlogin`() RETURNS varchar(128) CHARSET utf8mb4
BEGIN
SET @chars = 'aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPqQrRsStTuUvVwWxXyYzZ1234567890';
SET @charLen = LENGTH(@chars);
SET @randomPassword = '';
WHILE LENGTH(@randomPassword) < 20
    DO
    SET @randomPassword = CONCAT(@randomPassword, SUBSTRING(@chars,CEILING(RAND() * @charLen),1));
END WHILE;
RETURN @randomPassword ;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `CekLogin` */

/*!50003 DROP PROCEDURE IF EXISTS  `CekLogin` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `CekLogin`(kodebank1 VARCHAR(5),pin1 VARCHAR(100))
BEGIN
	if exists (select 1 from rflogin where rflogin.kodebank = kodebank1) then
		IF EXISTS (SELECT 1 FROM rflogin WHERE rflogin.pin = pin1) THEN
			SET @tanggal = (SELECT rflogin.tanggal FROM rflogin WHERE rflogin.kodebank = kodebank1 AND rflogin.pin = pin1);
			if (SELECT TO_DAYS(NOW())) = (SELECT TO_DAYS(@tanggal)) then
			SELECT loginid from rflogin where rflogin.kodebank = kodebank1 and rflogin.pin = pin1;
			else
			update rflogin set loginid = randomlogin() where rflogin.kodebank = kodebank1 and rflogin.pin = pin1;
			SELECT rflogin.loginid from rflogin WHERE rflogin.kodebank = kodebank1 AND rflogin.pin = pin1;
			end if;
		ELSE
		SELECT '02' ;
		END IF;
	else
	select '01' ;
	end if;
    END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
