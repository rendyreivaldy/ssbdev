/*
SQLyog Community v8.71 
MySQL - 5.5.5-10.4.11-MariaDB : Database - ssb
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
USE `ssb`;

/*Table structure for table `sireng` */

DROP TABLE IF EXISTS `sireng`;

CREATE TABLE `sireng` (
  `id` bigint(20) NOT NULL,
  `id_pengembang` varchar(20) NOT NULL,
  `nama_pengembang` varchar(999) DEFAULT NULL,
  `npwp_pengembang` varchar(16) DEFAULT NULL,
  `alamat_pengembang` varchar(999) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `sireng` */

/*Table structure for table `subsidi` */

DROP TABLE IF EXISTS `subsidi`;

CREATE TABLE `subsidi` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nik` varchar(20) NOT NULL,
  `is_subsidi` bit(1) DEFAULT NULL,
  `subsidi_type` varchar(100) DEFAULT NULL,
  `subsidi_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `subsidi` */

/* Function  structure for function  `randomlogin` */

/*!50003 DROP FUNCTION IF EXISTS `randomlogin` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `randomlogin`() RETURNS varchar(128) CHARSET utf8mb4
BEGIN
SET @chars = 'aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPqQrRsStTuUvVwWxXyYzZ1234567890';
SET @charLen = LENGTH(@chars);
SET @randomPassword = '';
WHILE LENGTH(@randomPassword) < 20
    DO
    SET @randomPassword = CONCAT(@randomPassword, SUBSTRING(@chars,CEILING(RAND() * @charLen),1));
END WHILE;
RETURN @randomPassword ;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `CekPengembang` */

/*!50003 DROP PROCEDURE IF EXISTS  `CekPengembang` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `CekPengembang`(iduji VARCHAR(10),kodebank1 VARCHAR(5), nik1 VARCHAR(20),npwp_pengembang1 varchar(16))
BEGIN
	IF EXISTS (SELECT 1 FROM sireng WHERE sireng.npwp_pengembang = npwp_pengembang1) THEN
	UPDATE resultuji SET tanggaluji = NOW(), result_pengembang= 'Y'  WHERE resultuji.nik = nik1 AND resultuji.kodebank = kodebank1 AND resultuji.id_uji = iduji;
	SELECT '00';	
	ELSE	
	UPDATE resultuji SET tanggaluji = NOW(), result_pengembang= 'N'  WHERE resultuji.nik = nik1 AND resultuji.kodebank = kodebank1 AND resultuji.id_uji = iduji;
	SELECT '07';
	END IF;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `CekSubsidi` */

/*!50003 DROP PROCEDURE IF EXISTS  `CekSubsidi` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `CekSubsidi`(iduji VARCHAR(10),kodebank1 VARCHAR(5), nik1 VARCHAR(20))
BEGIN
	IF EXISTS (SELECT 1 FROM subsidi WHERE subsidi.nik = nik1) THEN
	/*Sudah Pernah Mendapat Subsidi*/	
	UPDATE resultuji SET tanggaluji = NOW(), result_subsidi = 'N'  WHERE resultuji.nik = nik1 AND resultuji.kodebank = kodebank1 AND resultuji.id_uji = iduji;
	SELECT '06' ;
	ELSE
	UPDATE resultuji SET tanggaluji = NOW(), result_subsidi = 'Y'  WHERE resultuji.nik = nik1 AND resultuji.kodebank = kodebank1 AND resultuji.id_uji = iduji;	
	SELECT '00';
	end if ;
    END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
