// alert dialog
vex.dialog.alert(stringOrOptions)

// confirmation dialog
vex.dialog.confirm(options)

// prompt dialog
vex.dialog.prompt(options)

// plain dialog
vex.dialog.open(options)




=======================================================



callback: function () {},
afterOpen: function () {},
message: '',
input: '',
buttons: [
  dialog.buttons.YES,
  dialog.buttons.NO
],
showCloseButton: false,
onSubmit: function onDialogSubmit (e) {
  e.preventDefault()
  if (this.options.input) {
    this.value = serialize(this.form, { hash: true })
  }
  return this.close()
},
focusFirstInput: true




=================================================================





label: 'Prompt:',
placeholder: '',
value: ''





======================================================================




// opens a dialog 
vex.open(stringOrOptions);

// closes a dialog
vex.close(vexInstanceOrId);

// closes the top dialog
vex.closeTop();

// closes all dialog
vex.closeAll();

// gets a single vex instance by id.
vex.getById(id);

// gets all instances
vex.getAll();