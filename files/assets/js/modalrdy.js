
// $(document).ready(function() {
    var modalNotif = "<div style=\"display: none;\" id=\"notif\" class=\"modal\"><p><a style=\"font-size: 23px; font-weight: bold\" id=\"header\">Notifikasi</a></p><p style=\"font-size: 17px;\" id=\"content\"></p><a id=\"buttonclose\" style=\"display: none;\" class=\"btn btn-success float-right\" href=\"#\" rel=\"modal:close\">OK</a></div>";
    $(document.body).append(modalNotif);
// });

function showDialogNotif(content){
    $("#content").html(content);
    $("#notif").toggle();
    $("#notif").modal({
        fadeDuration: 100,
        escapeClose: false,
		clickClose: false,
		showClose: false
    });
 }

function showDialogNotifStack(content){
    $("#content").html(content);
    $("#notif").toggle();
    $("#notif").modal({
        fadeDuration: 100,
        escapeClose: false,
        clickClose: false,
        showClose: false,
        closeExisting: false
    });
 }

/*
	Show button close, so when loading there is close button
*/
 function btnCloseShow(){
 	$("#buttonclose").show();
    // var img = document.getElementById('buttonclose');
    // img.style.display = "block";
 }


/*
	Hide button close, so when loading there is close button
*/
 function btnCloseHide(){
 	$("#buttonclose").hide();
    // var img = document.getElementById('buttonclose');
    // img.style.display = "none";
 }

/*
	change header style when laoading, i.e : color, green when success, red when failed
*/
 function changeStyleHeader(type, content){
 	$("#header").css(type, content);
 }

/*
	change content style when loading, i.e : color, green when success, red when failed
*/
 function changeStyleContent(type, content){
 	$("#content").css(type, content);
 }

/*
	change content, i.e : change text
*/
 function changeContent(content) {
    $("#content").html(content);
 }

/*
	change header, i.e : change text
*/
 function changeHeader(content) {
    $("#header").html(content);
 }