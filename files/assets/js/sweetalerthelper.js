/*

Icon list type :
- success
- question
- info
- error
- warning

More Info :
https://sweetalert2.github.io
*/

function showDialogSimpleWithOKButton(message){
	Swal.fire(message);
}

function showDialogWithIconTitleContentAndOKButton(title, message, icon){
    closeDialog();
	Swal.fire({
	  title: title,			// title
	  text: message,		// content message
	  type: icon			// icon i.e question
	});
}

function showDialogWithIconTitleContentIndefiniteLoading(title, message){
    closeDialog();
	Swal.fire({
	  title: title,			// title
	  text: message,		// content message
	  onBeforeOpen: () => {
	    Swal.showLoading();
	  },
	  allowOutsideClick: false,
	  showCancelButton: false,
	  showCloseButton: false,
	  allowEscapeKey: false
	});
}

function closeDialog(){
	Swal.close();
}



